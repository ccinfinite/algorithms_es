
Capítulo 2: Recursión - ejercicios y desafíos
*************************************************


Sección 1. Revisión de la teoría
-------------------------------

Completa y ejecuta los trozos de código de las siguientes secciones (el código también se puede encontrar en el capítulo dos).

**Ejercicio 1.1**

La primera solución para encontrar un valor objetivo en una secuencia no ordenada es la *búsqueda secuencial*.
Se utiliza un bucle para examinar cada elemento, hasta que se encuentre el objetivo o se agote el conjunto de datos.
La *búsqueda binaria* es un algoritmo que encuentra un valor objetivo dentro de una secuencia ordenada e indexable de {[n]} elementos,
utilizando la recursividad de una manera más eficiente. Utilice la función definida en el siguiente ActiveCode para encontrar
un valor objetivo en un array de enteros que ha sido previamente ordenado.

.. activecode:: binary-search
   :language: python
   :caption: Función para la búsqueda binaria

   def binary_search(data, target, low, high):
     # Devuelve True si el objetivo se encuentra en los datos, entre el bajo y el alto
     if low > high:
       return False               # no se ha encontrado ninguna coincidencia
     else:
       mid = (low + high) / 2
       if target == data[mid]:    # Se encontró una coincidencia
         return True
       elif target < data[mid]:
         return binary_search(data, target, low, mid - 1)   # llamada en la parte izquierda
       else:
         return binary_search(data, target, mid + 1, high)  # llamada en la parte derecha

        # INSERTE EL NUEVO CÓDIGO AQUÍ


**Ejercicio 1.2**

Consideremos el problema de invertir los elementos de la secuencia indexada;
el primer elemento se convierte en el último, el segundo en el penúltimo, y así sucesivamente.
A continuación se ofrece una implementación recursiva. Utilízala para invertir un array.

.. activecode:: reverse-a-sequence
   :language: python
   :caption: Función para invertir una secuencia de elementos

   def reverse(A, first, last):
     # Elementos invertidos en A, entre el primero y el último
     if first < last - 1:                           # si hay al menos 2 elementos
       A[first], A[last-1] = A[last-1], A[first]    # swap first and last
       reverse(A, first+1, last-1)

        # INSERTE EL NUEVO CÓDIGO AQUÍ


**Ejercicio 1.3**

Consideremos el problema de la suma de los elementos de una secuencia indexada\\(A\\).
La suma de todos los \\(n\\) enteros de \\(A\\) es la suma de los primeros \\(n-1\\) enteros de \N(A\N),
más su último elemento. Proporcionamos la codificación de Python a continuación; utilícela en una secuencia de elementos.

.. activecode:: linear-sum
   :language: python
   :caption:  Suma lineal de una secuencia de elementos

   def linear_sum(A, n):
     # Devuelve la suma de los primeros n números de A
     if n == 0:
       return 0
     else:
       return A[n-1] + linear_sum(A, n-1)

    # INSERTE EL NUEVO CÓDIGO AQUÍ


En lugar de sumar el último elemento de \\(A\\) con la suma de los elementos restantes,
calculamos la suma de la primera mitad de \\(A\\) y la suma de la segunda mitad de \\(A\\),
recursivamente, y luego sumar estos números.

.. activecode:: binary-sum
   :language: python
   :caption: Suma binaria de una secuencia de elementos

   def binary_sum(A, first, last):
     # Devuelve la suma de los números de A entre el primero y el último
     if first <= last:     # ningún elemento
       return 0
     elif first == last-1:     # un elemento
       return A[first]
     else:
       mid = (first + last) / 2
       return binary_sum(A, first, mid) + binary_sum(A, mid, last)

    # INSERTE EL NUEVO CÓDIGO AQUÍ


**Ejercicio 1.4**

Los números de Fibonacci se pueden definir como \\(F_0 =0\\), o \\(F_1 = 1\\), o \\(F_n = F_{n-2} + F_{n-1}\\).
El código de Python para esta función se encuentra en el siguiente ActiveCode; pruebe y vea si puede ejecutar esta función.

.. activecode:: bad-fibonacci
   :language: python
   :caption: Función Fibonacci difícil de calcular

   def fibonacci(n):
     if n <= 1:
       return n
     else:
       return fibonacci(n-2) + fibonacci(n-1)

    # INSERTE EL NUEVO CÓDIGO AQUÍ


Una forma más eficiente de definir este programa es la siguiente; compárela con el código anterior.

.. activecode:: good-fibonacci
   :language: python
   :caption: Función Fibonacci fácil de calcular

   def good_fibonacci(n):
     if n <= 1:
       return (n,0)
     else:
       (a,b) = good_fibonacci(n-1)
       return (a+b,a)

    # INSERTE EL NUEVO CÓDIGO AQUÍ


**Ejercicio 1.5**

El *problema de unicidad de elementos* consiste en decidir si todos los elementos de una
secuencia \\(A\\) son distintos entre sí.

La primera solución es un algoritmo iterativo. Utiliza esta función sobre un array genérico.

.. activecode:: iterative-uniqueness
   :language: python
   :caption: Solución iterativa al problema de la unicidad de los elementos

   def unique1(A):
     for i in range(len(A)):
       for j in range(i+1, len(A)):
         if A[i] == A[j]:
           return False
     return True

    # INSERTE EL NUEVO CÓDIGO AQUÍ


A continuación se presenta una implementación recursiva ineficiente.

.. activecode:: recursive-uniqueness
   :language: python
   :caption: Solución recursiva al problema de la unicidad de los elementos

   def unique3(A, first, last):
     # Devuelve True si no hay elementos duplicados en A[first:last]
     if last - first <= 1:
       return True
     elif not unique3(A, first, last-1):
       return False
     elif not unique3(a, first+1, last):
       return False
     else:
       return A[first] != A[last-1]

     # INSERTE EL NUEVO CÓDIGO AQUÍ


En el siguiente guión se ofrece una solución mejorada.

.. activecode:: improved-recursive-uniqueness
   :language: python
   :caption: Solución recursiva mejorada al problema de la unicidad de los elementos

   def unique(A, first, last):
     if last-first <= 1:
       return True
     elif A[first] in A[first+1:]:
       return False
     else:
       return unique(A, first+1, last)

     # INSERTE EL NUEVO CÓDIGO AQUÍ




Sección 2. Problemas de recursión
--------------------------------

En esta sección, mostraremos algunos problemas clásicos que se pueden resolver mediante
funciones recursivas; comprueba, completa y ejecuta el ActiveCode en cada subsección.

**Ejercicio 2.1**

El rompecabezas *Torre de Hanoi* fue inventado por el matemático francés Edouard Lucas, en 1883.
Una leyenda habla de un templo en el que los sacerdotes recibieron tres palos y una pila de 64 discos de oro,
Cada disco era más pequeño que el que estaba debajo.
Tenían que trasladar los 64 discos de uno de los tres palos a otro, con dos reglas
En primer lugar, sólo se podía mover un disco a la vez; luego, no se podía colocar un disco más grande encima de otro más pequeño.
Cuando terminaran su trabajo, decía la leyenda, el mundo desaparecería.

El número de movimientos necesarios para mover correctamente una torre de 64 discos es \\(2^{64} -1\\), es decir, 18.446.744.073.709.551.615.
Moviendo un disco por segundo, el sacerdote necesitará 584.942.417.355 años.

Para encontrar una solución recursiva a este problema, consideremos el siguiente ejemplo.
Tenemos tres clavijas, a saber, la clavija uno, la clavija dos y la clavija tres, y supongamos que tenemos una torre de cuatro discos, originalmente en la clavija uno.
Si ya sabemos cómo mover una torre de tres discos a la clavija dos, podemos mover fácilmente el disco inferior a la clavija tres,
y luego mover la torre de tres de la clavija dos a la clavija tres.
No sabemos cómo mover una torre de altura tres, pero si sabemos cómo mover una torre de altura dos
podemos mover el tercer disco de una clavija a la otra, y mover la torre de dos de una clavija encima de ella.
Al final de este procedimiento (recursivo), seremos capaces de mover una sola clavija.

El esquema de alto nivel de cómo mover una torre desde el poste de salida hasta el poste de meta utilizando un poste intermedio es::

 Mover una torre de altura-1 a un poste intermedio, utilizando el poste final
 Mover el disco restante al poste final
 Mover la torre de altura-1 desde el poste intermedio al poste final utilizando el poste original

Siempre que obedezcamos la regla de que los discos más grandes permanecen en la parte inferior de la pila
podemos utilizar los tres pasos anteriores, de forma recursiva. Llegamos al caso base de la recursión cuando tenemos una torre de un disco;
en este caso, necesitamos mover sólo un disco a su destino final.
La función de Python está definida como sigue (nótese el orden de ``fromPole``, ``toPole`` y ``withPole`` en las
llamadas recursivas de ``moveTower``)::

 def moveTower(height, fromPole, toPole, withPole):
    if height >= 1:
        moveTower(height-1, fromPole, withPole, toPole)
        moveDisk(fromPole, toPole)
        moveTower(height-1, withPole, toPole, fromPole)

El código anterior realiza dos llamadas recursivas diferentes: con la primera
todos los discos, excepto el inferior, se mueven de la torre inicial a un poste intermedio;
la siguiente línea mueve el disco inferior a su lugar de descanso final;
la segunda llamada recursiva mueve la torre desde el polo intermedio hasta la parte superior del disco mayor.
El caso base se detecta cuando la altura de la torre es 0, y en este caso no hay nada que hacer.

La función ``moveDisk`` simplemente imprime que está moviendo un disco de un polo a otro::

 def moveDisk(from,to):
   print("Moviendo disco desde ", from, "hasta ", to)

El siguiente ActiveCode proporciona la solución completa.

.. activecode:: tower-of-hanoi
   :language: python
   :caption: Solución recursiva para el problema de la Torre de Hanoi

   def moveTower(height,fromPole, toPole, withPole):
       if height >= 1:
           moveTower(height-1,fromPole,withPole,toPole)
           moveDisk(fromPole,toPole)
           moveTower(height-1,withPole,toPole,fromPole)

   def moveDisk(fp,tp):
       print("Moviendo disco desde ",fp,"hasta ",tp)

   moveTower(3,"A","B","C")



**Ejercicio 2.2**

La criba de Eratóstenes es un algoritmo sencillo para encontrar todos los números primos hasta un número entero especificado \\(n\\);
una función recursiva que implementa este algoritmo tiene el siguiente pseudocódigo::

 a) crear una lista de enteros 2, 3, 4, ..., n;
 b) poner el contador i a 2 (el primer número primo);
 c) a partir de i+i, contar hacia arriba por i y eliminar esos números de la lista (2*i, 3*i, ...)
 d) encontrar el primer número de la lista que sigue a i; éste es el siguiente número primo;
 e) poner el contador i en este número;
 f) repetir los pasos c y d hasta que i sea mayor que n.

El siguiente programa implementa la criba de Eratóstenes de forma iterativa.
Imprimirá los 100 primeros números primos.

.. activecode:: sieve-of-eratosthenes-iterative
   :language: python
   :caption: Solución iterativa para la criba de Eratóstenes

   from math import sqrt
   def sieve(n):
     primes = list(range(2,n+1))
     max = sqrt(n)
     num = 2
     while num < max:
       i = num
       while i <= n:
         i += num
         if i in primes:
           primes.remove(i)
       for j in primes:
         if j > num:
           num = j
           break
     return primes
   print(sieve(100))

A continuación, damos una solución recursiva para el mismo problema.

.. activecode:: sieve-of-eratosthenes-recursive
   :language: python
   :caption: Solución recursiva para la criba de Eratóstenes

   from math import sqrt
   def primes(n):
     if n == 0:
       return []
     elif n == 1:
       return []
     else:
       p = primes(int(sqrt(n)))
       nop = [j for i in p for j in range(i*2, n + 1, i)]
       p = [x for x in range(2, n + 1) if x not in nop]
       return p
     print(primes(100))



Sección 3. Ejercicios y autoevaluación
----------------------------------------

**Ejercicio 3.1**

Escribe un código recursivo de la función \\(f(n)=3*n\\), es decir, los múltiplos de 3.
(Pista: Una definición recursiva de esta función puede escribirse como \\(f(1) = 3\\) y \\( f(n+1)=f(n)+3\\) )

**Ejercicio 3.2**

Escribe un código recursivo de la función \\(power(x,n)=x^n\\), es decir, la función potencia.
(Sugerencia: Una definición recursiva simple se desprende del hecho de que \\(x^n = x * x^{n-1}\\), para \\(n > 0\\) )

**Ejercicio 3.3**

Escribe un código recursivo de la función que devuelve la suma de los primeros \\(n\\) enteros.
(Pista: La solución viene de la observación de que la suma de los primeros \\(n\\)
es igual a la suma de los primeros números enteros \\(n\\) y la suma de los primeros números enteros \\(n-1\\).

**Ejercicio 3.4**

Escribe una función recursiva ``find_index()``, que devuelva el índice de un número en la
secuencia de Fibonacci, si el número es un elemento de esta secuencia, y devuelve -1, en caso contrario.

**Ejercicio 3.5**

Describa un algoritmo recursivo para encontrar los elementos máximos y mínimos de una secuencia de números enteros \\(n\\),
sin utilizar ningún bucle. ¿Cuál es su tiempo de ejecución y el uso de espacio?
(Sugerencia: Considere la posibilidad de devolver una tupla, que contiene tanto el valor mínimo como el máximo)

**Ejercicio 3.6**

Escribe una función recursiva para invertir una lista.

**Ejercicio 3.7**

Escriba una función recursiva para el problema de la unicidad de elementos, que se ejecute en tiempo \\(O(n^2)\\) sin utilizar la ordenación.
(Pista: Decir si los elementos de una secuencia son únicos puede reducirse al problema de decir
si los últimos elementos \\(n−1\\) son todos únicos y diferentes del primer elemento)

**Ejercicio 3.8**

Escriba un algoritmo recursivo para calcular el producto de dos enteros positivos, \\(m\\) y \\(n\\), utilizando sólo la suma.
(Sugerencia: El producto de \\(m\\) y \\(n\\) es la suma de \\(m\\), \\(n\\) veces)

**Ejercicio 3.9**

Escriba un algoritmo recursivo para calcular el exponente de dos enteros positivos, \\(m^n \\), utilizando sólo el producto.
(Sugerencia: Averigua cómo se puede definir la exponenciación mediante el producto)

**Ejercicio 3.10**

Escriba una función recursiva que tome una cadena de caracteres *s* y produzca su inversa.
Por ejemplo, el reverso de *pots* sería *stop*.
(Sugerencia: Imprima un carácter a la vez, sin espacios extra)

**Ejercicio 3.11**

Escribe una función recursiva para ver si una cadena *s* es un palíndromo, es decir, es igual a su inverso.
(Pista: Comprueba la igualdad del primer y último carácter y recursa)

**Ejercicio 3.12**.

Usa la recursión para escribir una función que determine si una cadena *s* tiene más vocales que consonantes.
