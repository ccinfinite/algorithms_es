Capítulo 3: Secuencias basadas en arrays
************************************


Sección 1 Introducción: Tipos de secuencias en Python
---------------------------------------------

Las clases construidas de secuencias de Python son *lista*, *tupla* y *str*.
Cada una de ellas soporta la indexación para acceder a un elemento ``(k\\)'' de una secuencia,
utilizando una sintaxis como ``seq[k]``; también comparten el concepto de bajo nivel de
*array* para poder ser representados. Se diferencian en cómo las instancias de estas
clases son representadas internamente por Python, como veremos en el resto de este capítulo.

Mientras que uno sólo necesita entender la sintaxis y la semántica de la interfaz pública de una clase
de una clase para escribir un código correcto, es importante tener alguna idea sobre la
implementación de la misma clase para lograr un buen nivel de eficiencia de los programas.
El uso básico de las listas, las cadenas y las tuplas es en cierto modo similar,
pero varios detalles pueden afectar el comportamiento y la eficiencia de los programas, si se subestiman.


Sección 2 Matrices de bajo nivel
--------------------------

La memoria primaria de un ordenador está compuesta por *bits* de información
que se agrupan en unidades más grandes de una manera que depende de la arquitectura del sistema.
Una unidad típica es un *byte*, que equivale a 8 bits.
Para llevar la cuenta de la información almacenada en bytes, se utiliza una *dirección de memoria*:
cada byte se asocia a un número binario único que sirve de dirección.
Las direcciones de memoria están coordinadas con la disposición física del sistema de memoria,
por lo que se muestran de forma secuencial.
A pesar de esta naturaleza secuencial, se puede acceder a cualquier byte de la memoria principal de forma eficiente
utilizando su dirección de memoria; esto significa que, en teoría, cualquier byte de memoria puede ser
almacenar o recuperar cualquier byte de memoria en un tiempo de \\(O(1)\\).
La memoria principal de un ordenador funciona como una memoria de acceso aleatorio (RAM).

Una tarea de programación común es llevar la cuenta de una secuencia de objetos relacionados;
en este caso, podríamos utilizar un número de variables diferentes, o podríamos preferir utilizar un
nombre único para el grupo, y números de índice para referirse a los elementos de ese grupo.
Esto se consigue almacenando las variables una tras otra en una porción contigua
de la memoria del ordenador, y denotando tal representación como un *array*.
Por ejemplo, la cadena de texto "STRING" se almacena como una secuencia ordenada de seis caracteres.
Cada carácter se representa utilizando el conjunto de caracteres Unicode
y cada carácter Unicode se representa con 16 bits (es decir, 2 bytes).
Por lo tanto, la cadena de seis caracteres se almacenará en 12 bytes consecutivos de memoria.


.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura31.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center

Nos referimos a esta secuencia como un array de seis caracteres;
cada lugar dentro de una matriz se llama *celda*, y utilizamos un índice entero creciente
para describir su ubicación dentro de la matriz,
empezando por 0 (por ejemplo, la celda de la matriz anterior con índice 2 contiene el carácter R).

Cada celda de un array debe utilizar el mismo número de bytes.
Esto nos permite acceder a una celda arbitraria del array en tiempo constante.
Si se conoce la dirección de memoria en la que comienza un array, el número de bytes por elemento ,
y un índice deseado dentro del array, la dirección de memoria del elemento
es *inicio + tamaño de celda x índice*, y este cálculo puede hacerse a un nivel bajo
de abstracción, siendo totalmente transparente para el programador.

Sección 2.1 Matrices referenciales
==============================

Consideremos una estructura basada en un array que, por alguna razón, mantiene los nombres de algunos colores.
Por ejemplo, en Python podríamos utilizar una lista de nombres, como:
``[ Blanco , Azul , Amarillo , Rojo , Negro ]``.
Los elementos de la lista son cadenas con diferentes longitudes; esto contrasta con
el requisito de Python de que cada celda del array debe utilizar el mismo número de bytes.
Una primera solución podría ser reservar suficiente espacio para que cada celda contenga la cadena con la máxima longitud;
esto es claramente ineficiente (se desperdicia mucha memoria)
y no es general (no sabemos si habrá otro nombre más largo que los que ya están en la lista).

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura32.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center


Una solución más eficiente es representar una instancia de lista mediante una matriz de referencias a objetos:
se almacena una secuencia consecutiva de direcciones de memoria, cada una de las cuales se refiere a un elemento de la secuencia.
Obsérvese que, aunque el tamaño de cada elemento puede variar, el número de
bits utilizados para almacenar la dirección de memoria de cada elemento es fijo;
esto permite un acceso en tiempo constante a una lista de elementos, basado en su índice.

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura33.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center

Dado que, en Phyton, las listas son estructuras referenciales, una instancia de lista puede incluir múltiples referencias
al mismo objeto como elementos de la lista, o un solo objeto puede ser
un elemento de dos listas. Por ejemplo, la lista ``primes=[2,3,5,7,11,13,17]``,
es una lista de referencias a los números, y no una lista de números.
El comando ``temp = primes[3:6]`` devuelve una nueva instancia de la lista ``[7,11,13]``,
y esta nueva lista tiene referencias a los mismos elementos que están en la lista original.

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura34.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center

Cuando los elementos de la lista son objetos inmutables, ninguno de ellos puede provocar un cambio en el objeto compartido;
por ejemplo, el comando ``temp[2] = 15`` no cambia el objeto entero existente 13;
En cambio, cambia la referencia de la celda 2 de la lista ``temp`` por una nueva referencia a un objeto diferente.

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura35.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center

Lo mismo ocurre al hacer una nueva lista como copia de una existente, con el comando ``backup = list(primes)``.
Esto produce una nueva lista que es una *copia superficial*, ya que hace referencia a los mismos elementos
que en la primera lista. Si el contenido de la lista es de tipo mutable, una *copia profunda* (una nueva lista con nuevos elementos),
puede producirse utilizando la función ``deepcopy`` del módulo ``copy``.


Sección 2.2 Matrices compactas
==========================

A diferencia de los arrays referenciales, los *arrays compactos* almacenan los bits que representan los datos
en la estructura, directamente.
Por ejemplo, las cadenas se representan en Phyton como matrices de caracteres
y no como matrices de referencias a caracteres.
Esta solución tiene alguna ventaja computacional sobre las matrices referenciales;
una estructura referencial utiliza 64 bits para cada dirección de memoria almacenada en la matriz,
más el número de bits utilizados para representar los elementos reales.
Además, en un array compacto los datos se almacenan consecutivamente en la memoria,
y no es el caso de una estructura referencial, cuyos elementos residen
en partes de la memoria que no son consecutivas; es ventajoso, para un
cálculo, tener los datos almacenados en memoria cerca de otros datos que podrían utilizarse en los mismos cálculos.

El soporte principal para crear matrices compactas está en el módulo ``array``.
Este módulo define la clase ``array``, que proporciona almacenamiento compacto para arrays de tipos de datos primitivos.
Una matriz compacta de números primos se declara como ``primes = array( i , [2, 3, 5, 7, 11, 13, 17, 19])``,
donde ``i`` representa el tipo de datos que se almacenará en el array, un entero en este caso.
Esto permite al intérprete conocer la cantidad de bits que se necesitarán para
almacenar el array, mientras que la interfaz de la clase es similar a la de una lista.
Los códigos de tipo soportados por el módulo de arrays se basan en los tipos de datos básicos, como
``char`` (con o sin signo), ``int`` (con o sin signo), y ``float``.


Sección 3 Conjunto dinámico
-----------------------

En la sección anterior hemos visto que para crear un array compacto
hay que declarar el tamaño de ese array.
Se asignará un trozo de memoria consecutivo para almacenar esa información.
Si se quiere ampliar la capacidad del array, de forma *dinámica*,
una primera solución es añadir celdas al array previamente definido;
esto no puede hacerse fácilmente, porque las posiciones de memoria vecinas podrían estar ya ocupadas por
otras piezas de información.

Consideremos, por ejemplo, la clase ``list`` de Python.
Una lista tiene una longitud determinada cuando se define, pero se nos permite añadir elementos a la lista, indefinidamente.
Esto se realiza implementando la lista por medio de un *array dinámico*; la instancia de la lista
mantiene un array subyacente que tiene mayor capacidad que la longitud actual de la lista.
Por ejemplo, mientras la lista actual contiene seis elementos
el sistema construye una matriz subyacente que almacena ocho o más referencias a objetos.
Se puede añadir un nuevo elemento a la lista utilizando la siguiente celda disponible de la matriz.
Sin embargo, esta capacidad adicional no es suficiente si el usuario sigue añadiendo elementos a la lista.
En este caso, el sistema debe proporcionar un array más grande,
inicializando el nuevo array para que su prefijo coincida con el del array más pequeño existente.
En lo que sigue, mostramos que la clase ``list`` de Python se basa en esta estrategia::

 1  import sys           # incluye la función getsizeof
 2  data = [ ] 			 # lista vacía
 3  for k in range(n):
 4    a = len(data) 			# a es el número de elementos
 5    b = sys.getsizeof(data) 	# tamaño de los datos en bytes
 6    print( Longitud: {0:3d}; Size in bytes: {1:4d} .format(a, b))
 7    data.append(None) 		# aumentar la longitud en uno

 Longitud: 0; Size in bytes : 72
 Longitud: 1; Size in bytes : 104
 Longitud: 2; Size in bytes : 104
 Longitud: 3; Size in bytes : 104
 Longitud: 4; Size in bytes : 104
 Longitud: 5; Size in bytes : 136
 Longitud: 6; Size in bytes : 136
 Longitud: 7; Size in bytes : 136
 Longitud: 8; Size in bytes : 136
 Longitud: 9; Size in bytes : 200
 Longitud: 10; Size in bytes : 200
 Longitud: 11; Size in bytes : 200
 Longitud: 12; Size in bytes : 200
 Longitud: 13; Size in bytes : 200
 Longitud: 14; Size in bytes : 200
 Longitud: 15; Size in bytes : 200
 Longitud: 16; Size in bytes : 200
 Longitud: 17; Size in bytes : 272
 Longitud: 18; Size in bytes : 272
 Longitud: 19; Size in bytes : 272
 Longitud: 20; Size in bytes : 272
 Longitud: 21; Size in bytes : 272
 Longitud: 22; Size in bytes : 272
 Longitud: 23; Size in bytes : 272
 Longitud: 24; Size in bytes : 272
 Longitud: 25; Size in bytes : 272
 Longitud: 26; Size in bytes : 352

Nótese que una lista vacía ya requiere un número de bytes de memoria (72 en este caso).
Esto ocurre porque cada objeto de Python (como la instancia de nuestra lista) mantiene algunas variables de instancia privadas
como el número de elementos almacenados actualmente en la lista,
el número máximo de elementos que se pueden almacenar en el array,
y la referencia al array actualmente asignado (inicialmente None).
En cuanto se inserta el primer elemento en la lista, el número de bytes salta
de 72 a 104, dada la asignación de un array capaz de almacenar cuatro referencias a objetos.
Después de añadir el quinto elemento a la lista, el uso de memoria salta
de 104 bytes a 136 bytes, lo que significa que la lista puede albergar hasta ocho referencias a objetos.
Tras la novena y la decimoséptima inserción, el tamaño de la lista vuelve a aumentar.

Como una lista es una estructura referencial, el resultado de ``getsizeof`` para una instancia de la lista
sólo incluye el tamaño para representar su estructura primaria; no tiene en cuenta
memoria utilizada por los objetos que son elementos de la lista. En nuestro experimento, añadimos
repetidamente None a la lista, porque no nos importa el contenido, pero
pero podríamos añadir cualquier tipo de objeto sin afectar el número de bytes reportados por ``getsizeof(data)``.


Sección 3.1 Aplicación
==========================

En esta sección, introducimos una implementación de la clase ``DynamicArray``;
dado un array *A* que almacena los elementos de la lista, mostraremos cómo añadir un elemento
cuando se ha alcanzado el tamaño máximo del array, utilizando los cuatro pasos siguientes::

 1. Definir un nuevo array B, con doble capacidad;
 2. B[i] = A[i] (para i = 0,... n-1), siendo n el número de elementos actuales;
 3. A = B, , es decir, utilizar B como el array que soporta la lista;
 4. añadir el nuevo elemento en el nuevo array.

Python soporta la creación de listas con la clase ``list``;
proporcionamos una implementación alternativa en el siguiente código::

 1  import ctypes					# proporciona matrices de bajo nivel
 2
 3  class DynamicArray:
 4
 5    def  __init__ (self):
 6      self._n = 0 				# número de elementos
 7      self._capacity = 1 			# capacidad inicial
 8      self._A = self._make_array(self._capacity) 			# definir matriz de bajo nivel
 9
 10   def __len__ (self):
 11     return self._n
 12
 13   def __getitem__ (self, k):
 14     if not 0 <= k < self._n:
 15       raise IndexError('invalid index')
 16     return self._A[k]
 17
 18   def append(self, obj):
 19     if self._n == self._capacity:			# conjunto completo
 20       self._resize(2*self._capacity) 		# doble capacidad
 21     self._A[self._n] = obj
 22     self._n += 1
 23
 24   def _resize(self, c):
 25     B = self._make_array(c) 			# nuevo conjunto 
 26     for k in range(self._n):
 27       B[k] = self._A[k]
 28     self._A = B 						# A es la nueva matriz
 29     self._capacity = c
 30
 31   def _make_array(self, c):
 32     return (c*ctypes.py_object)( ) # véase la documentación de ctypes

En este código, proporcionamos las funcionalidades ``append``, ``__len__``, y ``__getitem__``.
Creamos un nuevo array de bajo nivel utilizando el módulo ``ctypes``.


Sección 3.2 Análisis de matrices dinámicas
======================================

En esta sección, analizamos el tiempo de ejecución de las operaciones sobre matrices dinámicas,
utilizando una técnica llamada *amortización algorítmica*,
para demostrar que la estrategia introducida antes es suficientemente eficiente.

Reemplazar una matriz completa por una nueva matriz más grande requiere un tiempo de ejecución de \\(\\Omega(n)\\) con
con \\(n\\) el número actual de elementos en la matriz.
Esto ocurre cuando intentamos añadir un nuevo elemento a un array que ya está lleno.
Una vez duplicada la capacidad del array, podemos anexar \\(n\\) nuevos elementos al array,
antes de que tenga que ser reemplazado de nuevo.
Por lo tanto, hay muchas operaciones simples de adición por cada una costosa.

Para mostrar la eficiencia de esta estrategia, imaginemos que hay que pagar una moneda por una cantidad constante de tiempo de cálculo.
cantidad de tiempo de computación. Cuando haya que ejecutar una operación, debemos tener suficientes monedas para
pagar el tiempo de ejecución de esa operación.
La cantidad total de monedas gastadas para cualquier cálculo es proporcional al tiempo total empleado en ese
computación. Demostramos lo siguiente

**Proposición:** Sea \\(S\\) una secuencia implementada mediante una matriz dinámica
con capacidad inicial igual a 1, y duplicando el tamaño del array cuando se llena.
El tiempo total requerido para realizar \\(n\\) las operaciones de apéndice en \\(S\\) es \\(O(n)\\) de la misma.

**Prueba** Supongamos que una moneda es el coste de cada operación de append,
y supongamos que duplicar un array de tamaño \\(k\\) (cuando sea necesario) cuesta \\(k\\) monedas.
La técnica de amortización nos permite sobrecargar algunas operaciones sencillas para pagar
otras que son más caras.
Cobramos tres monedas por cada operación de añadir (dos monedas más que el coste real de la operación),
cuando no cause un desbordamiento del array.
Utilizaremos las monedas extra para pagar el coste de la operación de duplicación.
El desbordamiento se produce cuando el tamaño de la matriz \\(S\\) tiene \\(2^i\\) elementos, para \\(i \\geq 0\\);
duplicar el tamaño de la matriz requiere \\(2^i\\) monedas.
Estas monedas se han guardado durante las operaciones de apéndice anteriores, desde la celda \\(2^{i−1}\\) hasta  \\(2^i −1\\),
y pueden utilizarse para pagar la ampliación de la matriz.
Nuestro esquema de amortización, en el que cada operación se cobra con tres monedas, nos permite
pagar por la ejecución de \\(n\\) añadir operaciones utilizando \\(3n\\) monedas;
el tiempo de ejecución amortizado de cada operación de adición es de \\(O(1)\\), y
el tiempo total de ejecución de las operaciones de apéndice de \\(n\\) monedas es de \\(O(n)\\).

Obsérvese que la clave del rendimiento lineal anterior es que el espacio que añadimos
al expandir el array es siempre proporcional al tamaño actual del propio array;
en este caso, el tamaño del array se duplica, cuando es necesario.
Podríamos elegir aumentar el tamaño del array, por ejemplo, en un 30%, o en un 200%.
Si la ampliación del array sigue un crecimiento geométrico de este tipo
se puede demostrar que el límite de tiempo amortizado anterior sigue siendo válido, utilizando un número constante
de monedas para cada operación.

Si se utiliza una progresión aritmética en lugar de una geométrica, el coste global es cuadrático.
Supongamos, por ejemplo, que se añade una celda cada vez que hay que redimensionar el array; esto lleva a
un redimensionamiento por cada operación de adición, y esto requiere \\(1+2+...+n\\)  operaciones (es decir \\(\\Omega(n^2)\\).
En general, demostramos lo siguiente

**Proposición** Sea \\(S\\) una secuencia implementada mediante una matriz dinámica
con capacidad inicial igual a 1, y utilizando un incremento fijo con cada redimensionamiento.
El tiempo total requerido para realizar \\(n\\) las operaciones de apéndice en \\(S\\) es de \\( O(n^2) \\).

**Prueba** Sea \\(c > 0\\) el número fijo de celdas que se añaden al array en cada redimensionamiento.
Durante las operaciones de adición \\(n\\), se requiere tiempo para inicializar matrices de tamaño
\\(c\\), \\(2c\\), \\(3c\\), ..., \\(mc\\),  para \\(m = n/c\\);
El tiempo total es proporcional a\\(c+2c+3c+ \\ldots +mc\\), y esta suma es \\( \\Omega(n^2) \\) de la capacidad).

Por último, el aumento geométrico de la capacidad al redimensionar un array muestra una propiedad interesante
para una estructura de datos: el tamaño final de la matriz, al final de las operaciones de \\(n\\), es proporcional a
\\(O(n)\\). En general, cualquier contenedor que proporcione operaciones que provoquen la adición o eliminación de
uno o más elementos, requiere un uso de memoria de \\(O(n)\\) memoria.
Las inserciones y eliminaciones repetidas pueden hacer que el array crezca arbitrariamente,
sin relación proporcional entre el número real de elementos y la capacidad del array
después de eliminar muchos elementos.
Una implementación robusta de tal estructura de datos tiene que reducir el array
cuando sea posible, manteniendo el límite amortizado constante en las operaciones individuales.




Sección 4 Rendimiento de los tipos de secuencia de Python: lista, tupla y cadena
-------------------------------------------------------------------------

En esta sección, primero consideramos el comportamiento de las tuplas de Python y las listas *no mutantes*;
después, analizamos lo que ocurre con las listas *mutantes*.

Sección 4.1 Clases de listas y tuplas no mutantes de Python
========================================================

Los métodos no mutantes de la clase ``lista`` y los de la clase ``tupla`` son los mismos.
Las tuplas y las listas inmutables tienen la misma eficiencia, porque no hay necesidad de un array dinámico subyacente.
matriz dinámica subyacente. A continuación resumimos el coste de algunas operaciones sobre instancias de estas clases.

**Operaciones en tiempo constante** La longitud de una instancia de lista o tupla ``len(data)`` se devuelve en
tiempo constante, ya que cada instancia mantiene explícitamente dicha información.
El acceso directo a un elemento ``data[j]`` requiere un tiempo constante, debido al acceso subyacente a un array.

**Búsqueda de ocurrencias de un valor**
``data.count(value)``, ``data.index(value)``, y ``value in data`` requiere iteraciones
en la secuencia de izquierda a derecha.
Nótese que la iteración para ``contar`` debe proceder a través de toda la secuencia de longitud \\(n\\),
lo que implica que su tiempo-complejidad es \\(O(n)\\).
Los otros dos métodos pueden detener la iteración tan pronto como se encuentre el índice o se verifique la contención
o la contención, respectivamente, lo que significa que la complejidad es \\(O(k)\\) de la que \\(k\\) es el índice de las ocurrencias más a la izquierda de
de las ocurrencias más a la izquierda de ``value``.

**Comparaciones de secuencias**
Las comparaciones entre dos secuencias se definen lexicográficamente.
Esto requiere, en el peor de los casos, una iteración que toma un tiempo proporcional a la longitud de la más corta
de las dos secuencias, por lo tanto \\(O(n)\\).

**Creación de nuevas instancias**
El tiempo de ejecución de operaciones como cortar una secuencia ``data[a:b]``
o concatenar dos secuencias ``data1+data2`` es proporcional a la longitud del resultado.

Sección 4.2 Lista mutante de Python
==================================

La operación de mutación más simple en las listas es ``data[j] = val``, con la semántica obvia.
Está soportada por el método ``setitem__``, con una complejidad de tiempo en el peor de los casos \(O(1)\},
porque simplemente sustituye un elemento de una lista por uno nuevo.
A continuación mostramos la complejidad de los métodos que añaden o eliminan elementos de una lista.

**Añadir elementos a una lista**
El método ``append`` requiere \\(\\Omega(n)\\), en el peor de los casos, cuando se necesita un redimensionamiento del array.
En el esquema de complejidad amortizada, utiliza un tiempo de \\(O(1)\\).
Otro método soportado por la clase ``list`` es ``insert(k, value)``, que inserta
un ``value`` dado en la lista en el índice ``k``, desplazando todos los elementos posteriores a la derecha.
Una implementación de este método es::

 1  def insert(self, k, value):
 2
 3    if self._n == self._capacity: 		# no hay suficiente espacio, redimensiona la matriz
 4      self._resize(2*self._capacity)
 5    for j in range(self._n, k, −1): 		# desplazamiento a la derecha, primero a la derecha
 6      self._A[j] = self._A[j−1]
 7    self._A[k] = value
 8    self._n += 1

Tenga en cuenta que para añadir un elemento puede ser necesario redimensionar la matriz.
Esto cuesta un tiempo lineal en el peor de los casos, pero cuesta un tiempo amortizado de \\(O(1)\\).
Desplazar \\(k\\) elementos para hacer sitio al nuevo elemento cuesta \\(O(n−k+1)\\) amortizado.
Al evaluar el tiempo medio por operación, vemos que insertar
al principio de la lista es la más cara, ya que requiere un tiempo lineal por operación;
insertar en el medio requiere aproximadamente la mitad de tiempo que insertar al principio,
pero sigue siendo un tiempo lineal; insertar al final requiere un tiempo constante.

**Eliminar elementos de una lista**
La forma más sencilla de eliminar un elemento de una instancia de la clase ``list`` de Python es
con el método ``pop()``, que elimina el último elemento de una lista;
su ejecución requiere un tiempo amortizado \\(O(1)\\), dado que todos los elementos permanecen en la celda original del array dinámico subyacente.
celda original del array dinámico subyacente, con la excepción de la reducción ocasional del propio array.
El método ``pop(k)`` elimina el \\(k\\)-ésimo elemento de la lista, y desplaza todos los elementos posteriores hacia la izquierda;
Esto requiere una complejidad de \\(O(n−k)\\).

El método ``remove(value)`` elimina la primera aparición de ``value`` de la lista;
requiere un escaneo completo de la lista, primero para buscar el valor
y luego para desplazar todos los demás elementos a la izquierda::

 1  def remove(self, value):
 2    for k in range(self._n):
 3      if self._A[k] == value:
 4        for j in range(k, self._n−1):
 5          self._A[j] = self._A[j+1]
 6        self._A[self._n −1] = None
 7        self._n −= 1
 8        return 					# salir inmediatamente
 9    raise ValueError('value not found')

**Extender una lista**
El método ``extend`` se utiliza para añadir todos los elementos de una lista al final de una segunda lista,
con la llamada ``first.extend(second)``. El tiempo de ejecución es proporcional a la longitud de
la lista ``second``, y se amortiza porque el array que representa a ``first`` puede cambiar de tamaño.

**Construir nuevas listas**
Python ofrece varias formas de construir nuevas listas.
En casi todos los casos, el tiempo requerido es lineal en la longitud de la lista que se crea.
Por ejemplo, es común crear una lista usando el operador de multiplicación
como en ``[0]*n``, que crea una lista de longitud ``n`` con todos los valores 0.


Sección 5 Algunos algoritmos sobre cadenas de Python
---------------------------------------------

En esta sección, analizamos el comportamiento de algunos métodos y algunos algoritmos bien conocidos sobre cadenas.
Sea \\(n\\) y \\(m\\) la longitud de las cadenas. Parece natural evaluar como
lineal (en la longitud de la cadena) la complejidad de aquellos métodos que producen una nueva cadena.
Además, los métodos que comprueban las condiciones booleanas de una cadena, o los operadores de comparación, tardan \\(O(n)\\) en tiempo,
porque tienen que comprobar todos los caracteres \\(n\\) en el peor de los casos, y cortocircuitar tan pronto como se encuentra la respuesta
(por ejemplo, ``islower`` devuelve False si el primer carácter está en mayúsculas); lo mismo ocurre con
operadores de comparación, como ``==`` o ``<=``.

Sección 5.1 Comparación de patrones
============================

En el problema clásico de coincidencia de patrones, se nos da una cadena de texto \\(T\\) de longitud \\(n\\)
y una cadena de patrones \\(P\\) de longitud \\(m\\), y queremos encontrar si \\(P\\) es una subcadena de \\(T\\).
Las soluciones a este problema muestran algunos comportamientos interesantes, desde el punto de vista algorítmico.
Una implementación de fuerza bruta se ejecuta en tiempo \\(O(mn)\\),
porque considera los posibles índices de inicio del patrón \\(n−m+1\\),
y gasta tiempo \\(O(m)\\) en cada posición inicial, comprobando si el patrón coincide.
Las soluciones más refinadas se ejecutan en un tiempo de \\(O(n)\\).

La solución formal del problema consiste en encontrar el índice más bajo \\(j\\) con \\(T\\)
en el que comience \\(P\\) , de forma que \\(T[j:j+m]\\) sea igual a \\(P\\).
El problema de coincidencia de patrones está relacionado con muchos comportamientos de la clase
de Python, como ``in``, ``find``, ``index``, ``count``,
y es una subtarea de otros métodos como ``partition``, ``split`` y ``replace``.

**Primera solución: Fuerza bruta**

En general, el diseño por fuerza bruta consiste en enumerar todas las configuraciones posibles de las entradas
y seleccionar la mejor de todas, según algún tipo de medida.
Es fácil imaginar que enumerar y buscar implica un consumo de tiempo no óptimo.
Así pues, buscamos todas las posibles colocaciones de \\(P\\) en \\(T\\),
devolviendo el índice más bajo de \\(T\\) en el que comienza la subcadena \\(P\\) (o si no -1), como sigue::

 1  def find brute(T, P):
 2
 3    n, m = len(T), len(P)
 4    for i in range(n−m+1):
 5      k = 0
 6      while k < m and T[i + k] == P[k]:
 7        k += 1
 8      if k == m:			# llegamos al final de P,
 9        return i 			# la subcadena T[i:i+m] coincide con P
 10     return −1 			# no hay coincidencia

El algoritmo anterior consta de dos bucles anidados: el bucle exterior que indexa todos los posibles
de inicio del patrón en el texto \\(T\\); el bucle interno indexando a través de cada carácter del patrón \N(P\N), comparándolo con el carácter correspondiente en el texto.
El bucle interno recorre cada carácter del patrón \\(P\\), comparándolo con el carácter correspondiente del texto.
Esto implica que para cada índice candidato en T, realizamos hasta \\(m\\) comparaciones de caracteres
Por lo tanto, en el peor de los casos, realizamos \\(O(mn)\\) operaciones.


**Segunda solución: El algoritmo Boyer-Moore**

El algoritmo Boyer-Moore de concordancia de patrones evita las comparaciones entre \\(P\\) los caracteres de P\Ny una parte de
de los caracteres de \\(T\\). Esto significa que no necesitamos, como en el enfoque de fuerza bruta
escanear cada carácter de \\(T\\) con el fin de encontrar la coincidencia.

Dos consideraciones nos permiten escribir este algoritmo: la *heurística del espejo* y
la *heurística de salto de caracteres*. Con la primera, comenzamos la comparación desde el final
del \\(P\\), moviéndonos hacia atrás hasta el principio; si encontramos una
Si encontramos un desajuste en un lugar determinado de T, podemos evitar todas las comparaciones restantes
Si nos encontramos con un desajuste en un lugar determinado de T, podemos evitar todas las comparaciones restantes desplazando \\(P\\) en relación con \\(T\\) utilizando la heurística de salto de caracteres.
De hecho, si el desajuste se produce entre el carácter de texto \\(T[i]=c\\) y
el carácter del patrón \\(P[k]\\), tenemos dos casos: si \\(c\\) no pertenece a \\(P\\),
entonces desplazamos \\(P\\) completamente más allá de \\(T[i]\\); en caso contrario,
desplazamos \\(P\\) hasta que una ocurrencia del carácter \\(c\\) en P se alinee con T[i].
En ambos casos, no se ejecuta un número de comparaciones.

Más concretamente, supongamos que se encuentra una coincidencia para ese último carácter de \\(P\\):
el algoritmo intenta encontrar una coincidencia para el penúltimo carácter del patrón, y así sucesivamente,
hasta que se encuentre una coincidencia completa, o aparezca una falta de coincidencia en alguna posición del patrón.
De nuevo, si se encuentra una falta de coincidencia, y el carácter no coincidente del texto no aparece
en el patrón, desplazamos todo el patrón más allá de esa posición.
Si el carácter erróneo aparece en otra parte del patrón
debemos considerar dos posibles subcasos dependiendo de si su última aparición
es (1) antes o (2) después del carácter del patrón que estaba alineado con el carácter erróneo.

Sea \\(i\\) el índice del carácter desajustado en el texto,
\\(k\\) representa el índice correspondiente en el patrón,
y \\(j\\) representa el índice de la última ocurrencia de \\(T[i]\\) dentro del patrón.
Si (1) \\(j < k\\), desplazamos el patrón en \\( k− j\\) unidades,
y, por tanto, el índice \\(i\\) se incrementa en \\(m−(j +1)\\);
si (2) \\(j > k\\), desplazamos el patrón en una unidad,
y el índice  \\(i\\) se incrementa en \\(m−k\\).
A continuación, presentamos la implementación en Python del algoritmo::

 1  def boyer_moore(T, P):
 2	  # devuelve el índice del carácter en T donde comienza P, si existe
 3    n, m = len(T), len(P)
 4    if m == 0: return 0
 5    last = { } 						# construir el diccionario 'last'
 6    for k in range(m):
 7      last[ P[k] ] = k 				# la ocurrencia posterior sobrescribe
 8
 9    i = m−1 			# index of T
 10   k = m−1 			# index of P
 11   while i < n:
 12     if T[i] == P[k]: 	# coinciden
 13       if k == 0:
 14         return i 		# el patrón comienza en el índice i del texto
 15       else:
 16         i −= 1
 17         k −= 1
 18     else:				#no coinciden
 19       j = last.get(T[i], −1) 		# last(T[i]) es -1 si no se encuentra
 20       i += m − min(k, j + 1) 		# análisis de casos para el salto de etapa
 21       k = m − 1 					# reiniciar al final del patrón
 22   return −1

La eficiencia del algoritmo se basa en la capacidad de crear una función ``last(c)`` que devuelva el índice de la ocurrencia más a la derecha de ``(c)`` en ``(P)``.
que devuelve el índice de la ocurrencia más a la derecha de \\(c\\) en \\(P\\) (-1, si \\(c\\) si no en \\(P\\)).
Si el alfabeto es de tamaño finito, y los caracteres pueden utilizarse como índices de una matriz,
``last(c)`` puede implementarse como una tabla de búsqueda con coste de tiempo constante,
que contiene los caracteres del patrón.
El tiempo de ejecución en el peor de los casos del algoritmo de Boyer-Moore es \\(O(nm+|\\Sigma|)\\).
La función ``last`` utiliza un tiempo \\(O(m+|\\Sigma|)\\) de ejecución,)
y la búsqueda del patrón toma un tiempo de \\(O(nm)\\) en el peor de los casos,
lo mismo que el anterior algoritmo de fuerza bruta.
El algoritmo de Boyer-Moore es capaz de saltarse grandes porciones de texto, y las pruebas experimentales demuestran que
del texto, y las pruebas experimentales muestran que el número medio de
comparaciones realizadas por carácter es una cuarta parte de las utilizadas con un algoritmo de fuerza bruta.

Las heurísticas más refinadas alcanzan un tiempo de ejecución de \\(O(n+m+|Σ|)\\),
como por ejemplo en el algoritmo de coincidencia de patrones Knuth-Morris-Pratt.


Sección 5.2 Composición de cadenas
=============================

Supongamos que tenemos una cadena llamada ``document``, y que queremos crear una cadena ``letters``
que contenga los caracteres alfabéticos de la cadena original.
Una primera solución es

 letters = ''
 for c in document:
   if c.isalpha( ):
     letters += c

Para cada carácter del documento, lo añadimos a ``letters`` sólo si es alfabético.
Aunque la solución parece bastante natural, es muy ineficiente.
Una cadena es inmutable, lo que significa que cada vez que añadimos una letra, tenemos que ampliar la lista;
Ya hemos visto que esto requiere un tiempo proporcional a la longitud de la lista.
Para un resultado final con \\(n\\)  caracteres,
la serie de concatenaciones tomaría un tiempo proporcional a \\(1+2+3+ \\ldots +n\\),
es decir, el tiempo de \\(O(n^2)\\).
La razón por la que el comando ``letters += c`` es ineficiente, es porque hay que crear una nueva
cadena (la cadena original debe dejarse sin cambios si otra variable del programa se refiere a ella).
otra variable en un programa hace referencia a esa cadena).
Pero si no hay otras referencias a la cadena, el comando podría implementarse de forma más eficiente
utilizando un array dinámico, por ejemplo. Podemos detectar si no hay otras
referencias a la cadena comprobando el *contador de referencias*, un número que se mantiene para cada objeto.

Otra solución consiste en construir una lista temporal para almacenar piezas individuales
y luego utilizar el método ``join`` de la clase ``str`` para componer el resultado final::

 temp = [ ]
 for c in document:
   if c.isalpha( ):
     temp.append(c)
 letters = ''.join(temp)

Las llamadas a ``append`` requieren \\(O(n)\\) tiempo, porque hay a lo sumo \\(n\\) llamadas,
y cada llamada cuesta un tiempo amortizado \\(O(1)\\).
La llamada a ``join`` cuesta un tiempo lineal en la longitud de ``temp``.


Sección 5.3 Ordenación de la selección
==========================

Para una secuencia basada en un array, el algoritmo *selection sort* compara el primer y el segundo elemento del array.
Si el segundo es más pequeño que el primero, los intercambia.
A continuación, considera el tercer elemento de la matriz, intercambiándolo hacia la izquierda hasta que esté en su orden correcto
con los dos primeros elementos.
Luego, considera el cuarto elemento, y lo intercambia hacia la izquierda hasta que esté en el orden adecuado con
los tres primeros.
Este proceso continúa hasta que el array está ordenado::

 Algorithm InsertionSort(A):
   Input: una matriz A de n elementos
   Output: la matriz A con elementos reordenados en orden no decreciente
     for k from 1 to n − 1 do
       Insert A[k] at proper location within A[0], A[1], . . ., A[k].

La implementación en Python de la ordenación por inserción utiliza un bucle externo sobre cada elemento
y un bucle interno que coloca un elemento en su ubicación correcta en el
matriz de elementos que están a su izquierda::

 1  def insertion sort(A):
 3    for k in range(1, len(A)):
 4      cur = A[k]
 5      j = k
 6      while j > 0 and A[j−1] > cur:
 7        A[j] = A[j−1]
 8        j −= 1
 9      A[j] = cur

El tiempo de ejecución en el peor de los casos es de \\(O(n^2)\\), pero si la matriz está casi ordenada o perfectamente ordenada
los algoritmos se ejecutan en un tiempo de \\(O(n)\\).
