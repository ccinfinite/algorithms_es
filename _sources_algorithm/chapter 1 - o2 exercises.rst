

Capítulo uno: Análisis de algoritmos - ejercicios y desafíos
**************************************************************


Sección 1. Revisión de la teoría
-------------------------------

Completa los siguientes fragmentos de código (también se pueden encontrar en el Capítulo Uno), y ejecuta los programas.
Para cada programa, proporcione (1) un análisis asintótico y (2) un análisis experimental del consumo de tiempo,
utilizando la función ``time()`` del módulo ``time``, que devuelve los segundos transcurridos desde un tiempo de referencia;
ejecutar ``time()`` antes y después de la ejecución del algoritmo, para medir el tiempo transcurrido como la diferencia de los dos valores.
Utiliza el Codelens para recorrer el código.

El primer programa ``sumofn`` es un ejemplo de cómo hacerlo; imprime
imprime diez veces el resultado (la suma de los primeros 1.000 enteros) y el tiempo necesario para el cálculo.
Comprueba qué ocurre si se calcula la suma de los primeros 10.000 enteros, con respecto a los resultados del
análisis asintótico (el tiempo de cada ejecución debería requerir 10 veces más segundos).

.. activecode:: sum-of-n2
   :language: python
   :caption: Imprime la suma de los primeros n enteros

   import time

   def sumofn(n):
     start = time.time()
     sum = 0
     for i in range(1,n+1):
       sum = sum + i
     end = time.time()
     return sum,end-start

   for i in range(10):
     print("La suma %d requiere %10.7f segundos"%sumofn(1000))



.. activecode:: max-list
   :language: python
   :caption: Devuelve el elemento máximo de una lista

   def find_max(data):
     # Devuelve el máximo elemento de una lista
     biggest = data[0]
       for val in data:
         if val > biggest:
           biggest = val
       return biggest

      # INSERTE EL NUEVO CÓDIGO AQUÍ



.. tabbed:: prefix-average

   .. tab:: Quadratic time

      .. activecode:: prefix-average1
         :language: python
         :caption: Solución en tiempo cuadrático del problema de la media del prefijo

         def prefix_average(S):
           n = len(S)
           A = [0]*n                # lista de n ceros
           for j in range(n):
             total = 0
             for i in range(j + 1):
                total += S[i]
              A[j] = total / (j+1)    # la media j-ésima
           return A

          # INSERTE EL NUEVO CÓDIGO AQUÍ


   .. tab:: Linear time

       .. activecode:: prefix-average2
         :language: python
         :caption: Solución en tiempo lineal del problema de la media del prefijo

         def prefix_average(S):
           n = len(S)
           A = [0]*n                # lista de n ceros
           for j in range(n):
             total = 0
             for i in range(j + 1):
                total += S[i]
              A[j] = total / (j+1)    # la media j-ésima
           return A

          # INSERTE EL NUEVO CÓDIGO AQUÍ



.. tabbed:: element-uniqueness

   .. tab:: Quadratic time

      .. activecode:: element-uniqueness1
         :language: python
         :caption: Quadratic time solution of the element uniqueness problem

         def unique1(S):
           for j in range(len(S)):
             for k in range(j+1, len(S)):
               if S[j] == S[k]:
                 return False        # there is a duplicate pair
           return True               # all elements are distinct

            # INSERTE EL NUEVO CÓDIGO AQUÍ

   .. tab:: Logaritmic time

       .. activecode:: element-uniqueness2
         :language: python
         :caption: solución en tiempo nlogn del problema de unicidad de elementos

         def unique2(S):
           temp = sorted(S)            # ordenacion de S
           for j in range(1, len(temp)):
             if temp[j-1] == temp[j]:
               return False            # hay un par duplicado
           return True                 # todos los elementos son distintos

                       # INSERTE EL NUEVO CÓDIGO AQUÍ




Sección 2. El problema de la detección de anagramas
----------------------------------------

Dadas dos cadenas de caracteres de igual longitud, la solución del *problema de detección de anagramas* consiste en
escribir una función que devuelva ``true`` si la primera cadena es un reordenamiento de la segunda,
``false`` en caso contrario.
Hay varias soluciones a este problema, con diferente complejidad temporal. A continuación, presentamos
algunas de ellas; antes de revelar las soluciones, intente averiguar el algoritmo.

Sección 2.1 Comprobación de los caracteres
=======================================

Si las cadenas tienen la misma longitud, y si cada carácter de la primera cadena aparece en la segunda
entonces las dos cadenas son anagramas.
Cada vez que un carácter de la primera lista se encuentra en la segunda, tiene que ser *comprobado*, lo que significa que será
que será reemplazado por ``Ninguno`` en la segunda cadena.
Si todos los caracteres están marcados, la función devolverá ``True``.
Esta función puede ser revelada en el siguiente ActiveCode; pruebe y cambie las cadenas para ver cómo funciona la función
funciona.

.. reveal:: check-off
    :showtitle: Mostrar solución
    :hidetitle: Esconder solución

    .. activecode:: checking-off
       :language: python
       :caption: Detecta los anagramas marcando los caracteres

       def anagramdetection1(s1,s2):
         stillgood = True
         if len(s1) != len(s2):
           stillgood = False
         alist = list(s2)

         pos1 = 0
         while pos1 < len(s1) and stillgood:
           pos2 = 0
           found = False
           while pos2 < len(alist) and not found:
               if s1[pos1] == alist[pos2]:
                 found = True
               else:
                 pos2 = pos2 + 1

           if found:
             alist[pos2] = None
           else:
             stillgood = False
           pos1 = pos1 + 1
       return stillgood

       print(anagramSolution1('ajcd','dcba'))


Obsérvese que para el \\(i\\)-ésimo carácter de la lista ``s1`` habrá \\(i\\ visitas en ``s2``, para \\(i=0 \\ldots n\\).
Por lo tanto, el número total de visitas es la suma de los enteros de 1 a \\(n\\), lo que significa que el tiempo
es de \\(O(n^2)\\).


Sección 2.2 Clasificación de las cadenas
===============================

Otra solución consiste en ordenar las dos cadenas alfabéticamente,
convirtiendo cada cadena en una lista y utilizando el método incorporado ``sort`` en las listas;
si son anagramas, serán la misma cadena después de ser ordenadas.
El siguiente ActiveCode muestra esta solución.
De nuevo, intenta averiguar el algoritmo antes de revelar la solución.
Pruebe y cambie las cadenas para ver cómo funciona la función.

.. reveal:: sorting
    :showtitle: Mostrar solución
    :hidetitle: Ocultar solución

    .. activecode:: sorting-the-strings
       :language: python
       :caption: Detecta los anagramas ordenando las cadenas

       def anagramdetection2(s1,s2):
         alist1 = list(s1)
         alist2 = list(s2)

         alist1.sort()
         alist2.sort()

         pos = 0
         matches = True

         while pos < len(s1) and matches:
           if alist1[pos]==alist2[pos]:
              pos = pos + 1
           else:
              matches = False

         return matches

         print(anagramSolution2('abcde','edcba'))

La sección ``while`` del programa anterior tarda en realizarse \\(O(n)\\) un tiempo,
ya que hay \\(n\\) la comparación de los caracteres después de que el proceso de clasificación.
Pero los métodos de ordenación requieren un tiempo de \\(O(n^2)\\) o de \\(O(n logn)\\), por lo que las operaciones de ordenación dominan la iteración.



Sección 2.3 Contar y comparar
=============================

Si dos cadenas son anagramas, tendrán el mismo número de a's, b's, c's, etc.
Primero, podríamos contar el número de veces que aparece cada carácter, utilizando una lista de 26 contadores, 
uno por cada carácter posible.
Cada vez que se encuentre un carácter determinado, se incrementará el contador en esa posición.
Si las dos listas de contadores son idénticas, las cadenas deben ser anagramas.

.. reveal:: counting
    :showtitle: Mostrar solución
    :hidetitle: Ocultar solución

    .. activecode:: counting-the-characters
       :language: python
       :caption: Detecta anagramas contando los caracteres

       def anagramSolution4(s1,s2):
         c1 = [0]*26
         c2 = [0]*26

         for i in range(len(s1)):
           pos = ord(s1[i])-ord('a')
           c1[pos] = c1[pos] + 1

         for i in range(len(s2)):
           pos = ord(s2[i])-ord('a')
           c2[pos] = c2[pos] + 1

         j = 0
         stillOK = True
         while j<26 and stillOK:
           if c1[j]==c2[j]:
              j = j + 1
           else:
              stillOK = False

         return stillOK

       print(anagramSolution4('ajcd','dcba'))


Las dos primeras iteraciones, utilizadas para contar los caracteres, se basan en la longitud de cada cadena;
la tercera iteración tiene 26 pasos. El consumo total de tiempo es de \\(O(n)\\), es decir, un algoritmo de tiempo lineal.
tiempo lineal. Obsérvese que, aunque se trata de una solución óptima, este resultado se consigue mediante dos pequeñas
listas adicionales que guardan los recuentos de caracteres. Esto significa que tenemos que aceptar un compromiso
entre el uso de tiempo y espacio.


Sección 2.4 Encontrar todos los anagramas posibles
=========================================

Una técnica de *fuerza bruta* para resolver este problema intenta
generar una lista de todas las cadenas posibles utilizando los caracteres de ``s1`` y luego comprobar si ``s2`` aparece entre ellas.
El número total de cadenas candidatas es \\(n∗(n−1)∗(n−2)∗ \\ldots ∗3∗2∗1\\), que es \\(n!\\).
La función factorial crece más rápido que \\(2^n\\), lo que significa que un enfoque de fuerza bruta no es aconsejable.



Sección 3. Ejercicios y autoevaluación
----------------------------------------

Dé una caracterización de \\(O\\), en términos de \\(n\\), del tiempo de ejecución del siguiente fragmento de código
(Pistas: considere el número de veces que se ejecuta el bucle y cuántas operaciones primitivas se producen en cada iteración)

**Ejercicio 3.1**::

 1  def example1(S):
 2  ”””Devuelve la suma de los elementos de la secuencia S.”””
 3    n = len(S)
 4    total = 0
 5    for j in range(n):     # bucle de 0 a n-1
 6      total += S[j]
 7    return total

**Ejercicio 3.2**::

 1  def example2(S):
 2  ”””Devuelve la suma de los elementos con índice par en la secuencia S.”””
 3    n = len(S)
 4    total = 0
 5    for j in range(0, n, 2):     # incremento de 2
 6      total += S[j]
 7    return total

**Ejercicio 3.3**::

 1  def example3(S):
 2  ”””Devuelve la suma de las sumas de los prefijos de la secuencia S”””
 3    n = len(S)
 4    total = 0
 5    for j in range(n):         # bucle de 0 a n-1
 6      for k in range(1+j):     # bucle de 0 a j
 7        total += S[k]
 8    return total

**Ejercicio 3.4**::

 1  def example4(S):
 2  ”””Devuelve la suma de las sumas de los prefijos de la secuencia S.”””
 3    n = len(S)
 4    prefix = 0
 5    total = 0
 6    for j in range(n):
 7      prefix += S[j]
 8      total += prefix
 9    return total

**Ejercicio 3.5**::

 1  def example5(A, B): # assume that A and B have equal length
 2  ”””Devuelve el número de elementos de B igual a la suma de las sumas de los prefijos de A”””
 3    n = len(A)
 4    count = 0
 5    for i in range(n):         # bucle de 0 a n-1
 6      total = 0
 7      for j in range(n):       # bucle de 0 a n-1
 8        for k in range(1+j):   # bucle de 0 a j
 9          total += A[k]
 10     if B[i] == total:
 11       count += 1
 12   return count


**Ejercicio 3.5**

El algoritmo \\(A\\) ejecuta un cálculo de tiempo \\(O(\\log n)\\) para cada entrada de una
\\(n\\) elementos de una secuencia. ¿Cuál es su tiempo de ejecución en el peor de los casos?
(Sugerencia: el cálculo de \\(O(\\log n)\\)️ se realiza \\(n\\) veces)

**Ejercicio 3.6**

Dada una secuencia de \\(n\\) elementos \\(S\\), el algoritmo \\(B\\) elige \\(\\log n\\) elementos en
\\(S) al azar y ejecuta un cálculo en tiempo O(n) para cada uno de ellos. ¿Cuál es el
tiempo de ejecución en el peor de los casos de \\(B\\)?
(Sugerencia: el cálculo\\(O(n)\\) se realiza \\(\\log n\\) veces)

**Ejercicio 3.7**

Dada una secuencia \\(S\\) de \\(n\\)-elementos de enteros, el algoritmo \\(C\\) ejecuta un
\\(O(n)\\) para cada número par en \\(S\\), y un tiempo de computación \\(O(\\log n)\\) para cada número impar.
para cada número impar de \\(S\\). ¿Cuáles son los tiempos de ejecución en el mejor y en el peor de los casos
caso mejor y peor de los tiempos de ejecución de \\(C\\)?
(Pista: considere los casos en que todas las entradas de \\(S\\) son pares o impares)

**Ejercicio 3.8**

Dada una secuencia de elementos \\(n\\), el algoritmo \\(S\\) llama al algoritmo \\(E\\) en cada
elemento \N(S[i]\Nde la secuencia.) El algoritmo \N(E\N) se ejecuta en tiempo \\(O(i)\\) cuando es llamado en \\(S[i]\\).
¿Cuál es el tiempo de ejecución en el peor de los casos de \\(D\\)?
(Sugerencia: caracterizar el tiempo de ejecución de \\(D\\) utilizando una suma)

**Ejercicio 3.9**

Describa un algoritmo eficiente para encontrar los diez elementos más grandes de una
secuencia de tamaño \\(n\\). ¿Cuál es el tiempo de ejecución de su algoritmo?
(Pista: 10 es un número constante)

**Ejercicio 3.10**

Describa un algoritmo para encontrar tanto el mínimo como el máximo de \\(n\\) números
números usando menos de \\(3n/2\\) comparaciones.
(Pista: construir un grupo de candidatos a mínimos y un grupo de candidatos a máximos)

**Ejercicio 3.11**

Una secuencia \\(S\\) contiene \\(n−1\\) enteros únicos en el rango \\([0,n−1]\\) de la secuencia, es decir
es decir, que hay un número de este rango que no está en \\(S\\). Diseñe un algoritmo de tiempo\\(O(n)\\) que encuentre ese número.
que encuentre ese número. Puedes utilizar sólo espacio adicional \\(O(1)\\) para ello.
(Sugerencia: utiliza una función de los enteros en \\(S\\) que identifique inmediatamente qué número falta)

**Ejercicio 3.12**

Una secuencia \\(S\\) contiene \\(n\\) enteros tomados del intervalo \\([0,4n]\\) de la secuencia, con repeticiones
permitidas. Describa un algoritmo eficiente para determinar un valor entero
\\(k\\) que aparece con mayor frecuencia en \\(S\\). ¿Cuál es el tiempo de ejecución del algoritmo?
(Sugerencia: utilice una matriz que guarde los recuentos de cada valor)
