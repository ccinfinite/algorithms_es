



Capítulo 5: Listas enlazadas - ejercicios y retos
*****************************************************

Sección 1. Revisión de la teoría
-------------------------------

**Ejercicio 1.1**

Completa y ejecuta el siguiente trozo de código (que puedes encontrar en el capítulo cinco)
en el que se implementa un ADT de pila mediante una lista enlazada simple.
Recuerda que cada instancia de la pila mantiene dos variables: ``_head`` (una referencia al nodo que encabeza la lista)
y ``_size`` (que lleva la cuenta del número actual de elementos).
Añade algunos ejemplos de cómo funciona la pila, utilizando algunas llamadas a los métodos ``pop``, ``push`` o ``top``.

.. activecode:: stack-as-a-singly-linked-list
   :language: python
   :caption: Implementación de una pila mediante una lista enlazada simple

   class LinkedStack:
   # Implementación de la pila con una lista enlazada individualmente

     # Clase de nodo anidado en la pila
     class Node:
       __slots__ = '_element' , '_next'
       def __init__(self, element, next):
         self._element = element
         self._next = next

     # Métodos de pila
     def __init__(self):
       self._head = None
       self._size = 0

     def __len__(self):
       return self._size

     def is_empty(self):
       return self._size == 0

     def push(self, e):
       self._head = self._Node(e, self._head)
       self._size += 1

     def top(self):
       if self.is_empty( ):
         raise Empty('Stack is empty')
       return self._head._element

     def pop(self):
       if self.is_empty( ):
         raise Empty('Stack is empty')
       answer = self._head._element
       self._head = self._head._next
       self._size −= 1
       return answer

           # INSERTE EL NUEVO CÓDIGO AQUÍ


**Ejercicio 1.2**

Completa y ejecuta el siguiente trozo de código (que se encuentra en el capítulo cinco)
en el que se implementa un ADT de cola mediante una lista enlazada simple.
Recuerda que realizamos operaciones en ambos extremos de la cola, lo que significa que tenemos que mantener una ``_head`` y una ``_tail``.
y una ``_tail`` como variables de instancia para cada cola, junto con su ``_size``.
La parte delantera de la cola está alineada con la cabeza de la lista, y la parte trasera de la cola está alineada con la cola de la lista,
lo que nos permite poner en cola los elementos de la parte trasera y retirarlos de la parte delantera.
Añade algunos ejemplos de cómo funciona la cola, utilizando algunas llamadas a los métodos ``first``, ``enqueue``, o ``dequeue``.

.. activecode:: queue-as-a-singly-linked-list
   :language: python
   :caption: Implementación de una cola por medio de una lista unidireccional

   class LinkedQueue:
   # Implementación de la cola con una lista enlazada simple

     class Node:
       __slots__ = '_element' , '_next'
       def __init__(self, element, next):
         self._element = element
         self._next = next

     def __init__ (self):
       self._head = None
       self._tail = None
       self._size = 0

     def __len__ (self):
       return self._size

     def is_empty(self):
       return self._size == 0

     def first(self):
       if self.is_empty( ):
         raise Empty('Queue is empty')
       return self._head._element

     def dequeue(self):
       if self.is_empty( ):
         raise Empty('Queue is empty')
       answer = self._head._element
       self._head = self._head._next
       self._size −= 1
       if self.is_empty( ):       # caso especial si la cola está vacía
         self._tail = None        # la cabeza removida había sido la cola
       return answer

     def enqueue(self, e):
       newnode = self._Node(e, None)
       if self.is_empty( ):
         self._head = newnode
       else:
         self._tail._next = newnode
       self._tail = newnode
       self._size += 1

              # INSERTE EL NUEVO CÓDIGO AQUÍ



**Ejercicio 1.3**

Completa y ejecuta el siguiente trozo de código (que se encuentra en el capítulo cinco)
en el que se implementa un ADT de cola mediante una lista enlazada circularmente.
Recuerda que la cola tiene una cabeza y una cola, con la siguiente referencia de la cola enlazada a la cabeza.
Tenemos que mantener las variables de instancia ``_tail`` (una referencia al nodo cola)
y ``_size`` (el número de elementos de la cola); la cabeza de la cola se encuentra siguiendo la siguiente referencia de la cola,
es decir, ``self._tail._next``.
Con el método ``rotate``, la antigua cabeza se convierte en la nueva cola
y el nodo después de la antigua cabeza se convierte en la nueva cabeza.
Añade algunos ejemplos de cómo funciona la cola, utilizando algunas llamadas a los métodos ``first``, ``enqueue`` o ``dequeue``.


.. activecode:: queue-as-a-circularly-list
   :language: python
   :caption: Implementación de una cola mediante una lista circular

   class CircularQueue:
   # Implementación de colas mediante listas enlazadas circularmente

     class _Node:
       __slots__ = '_element' , '_next'
       def __init__(self, element, next):
         self._element = element
         self._next = next

     def __init__(self):
       self._tail = None
       self._size = 0

     def __len__ (self):
       return self._size

     def is_empty(self):
       return self._size == 0

     def first(self):
       if self.is_empty( ):
         raise Empty('Queue is empty')
       head = self._tail._next
       return head._element

     def dequeue(self):
       if self.is_empty( ):
         raise Empty('Queue is empty')
       oldhead = self._tail._next
       if self._size == 1:
         self._tail = None
       else:
         self._tail._next = oldhead._next
       self._size −= 1
       return oldhead._element

     def enqueue(self, e):
       newnode = self._Node(e, None)
       if self.is_empty( ):
         newnode._next = newnode
       else:
         newnode._next = self._tail._next
         self._tail._next = newnode
       self._tail = newnode
       self._size += 1

     def rotate(self):
       if self._size > 0:
         self._tail = self._tail._next


      # INSERTE EL NUEVO CÓDIGO AQUÍ


**Ejercicio 1.4**

Completa y ejecuta el siguiente trozo de código (que se encuentra en el capítulo cinco)
en el que se implementa un ADT deque mediante una lista doblemente enlazada.
Observa que la clase ``LinkedDeque`` hereda de la clase ``_DoublyLinkedBase``,
utilizando sus métodos para inicializar una nueva instancia, para insertar un elemento al principio o al final
y para eliminar un elemento.
El primer elemento del deque se almacena después de la cabecera
y el último elemento del deque se almacena en el nodo anterior al trailer.
Añade algunos ejemplos de cómo funciona el deque, utilizando algunas llamadas a los métodos ``insert_first``, ``insert_last``,
``delete_first``, y ``delete_last``.

.. activecode:: deque-as-a-doubly_linked-list
   :language: python
   :caption: Implementación de un deque mediante una lista doblemente enlazada

   class _DoublyLinkedBase:

     class Node:
       __slots__ = '_element' , '_prev' , '_next'
       def __init__(self, element, prev, next):
         self._element = element
         self._prev = prev                # referencia del nodo anterior
         self._next = next                # referencia del siguiente nodo

     def __init__(self):
       self._header = self._Node(None, None, None)
       self._trailer = self._Node(None, None, None)
       self._header._next = self._trailer        # el remolque está después de la cabecera
       self._trailer._prev = self._header        # la cabecera está antes del remolque
       self._size = 0

     def __len__(self):
       return self._size

     def is_empty(self):
       return self._size == 0

     def _insert_between(self, e, predecessor, successor):
       newnode = self._Node(e, predecessor, successor)
       predecessor._next = newnode
       successor._prev = newnode
       self._size += 1
       return newnode

     def _delete_node(self, node):
       predecessor = node._prev
       successor = node._next
       predecessor._next = successor
       successor._prev = predecessor
       self._size −= 1
       element = node._element                             # registro de elemento borrado
       node._prev = node._next = node._element = None      # útil para la recogida de basura
       return element                                      # devolver el elemento eliminado

   class LinkedDeque(_DoublyLinkedBase):

     def first(self):
       if self.is_empty( ):
         raise Empty("Deque is empty")
       return self._header._next._element

     def last(self):
       if self.is_empty( ):
         raise Empty("Deque is empty")
       return self._trailer._prev._element

     def insert_first(self, e):
       self._insert_between(e, self._header, self._header._next)

     def insert_last(self, e):
       self._insert_between(e, self._trailer._prev, self._trailer)

     def delete_first(self):
       if self.is_empty( ):
         raise Empty("Deque is empty")
       return self._delete_node(self._header._next)

     def delete_last(self):
       if self.is_empty( ):
         raise Empty("Deque is empty")
       return self._delete_node(self._trailer._prev)




Sección 2. Problemas en las listas enlazadas
-----------------------------------

En esta sección, abordaremos algunos problemas clásicos que pueden resolverse utilizando listas enlazadas.
Estudia cada uno de los ejercicios, luego comprueba, completa (si es necesario) y ejecuta el ActiveCode que se te proporciona.

**Ejercicio 2.1**

Escribe una función que cuente el número de nodos de una lista enlazada simple dada, tanto en versión iterativa como recursiva.
La solución iterativa a este problema está en el siguiente pseudocódigo::

 initialize count as 0
 initialize a node pointer, current = head.
 do following while current is not NULL
   current = current -> next
   count++;
 return count

Comprueba, completa y ejecuta el siguiente ActiveCode.

.. activecode:: number-of-nodes-list-iterative
   :language: python
   :caption: Devuelve el número de nodos de una lista, con una iteración

   class Node:
     def __init__(self, data):
       self.data = data
       self.next = None

   class LinkedList:
     def __init__(self):
       self.head = None

     def push(self, new_data):
       new_node = Node(new_data)
       new_node.next = self.head
       self.head = new_node

     def getCount(self):
       temp = self.head
       count = 0
       while (temp):
         count += 1
         temp = temp.next
       return count

   if __name__=='__main__':
     llist = LinkedList()
     llist.push(1)
     llist.push(3)
     llist.push(1)
     llist.push(2)
     llist.push(1)
     print ("Count of nodes is :",llist.getCount())


La solución recursiva a este problema está en el siguiente pseudocódigo::

 int getCount(head)
   if head is NULL, return 0
   else return 1 + getCount(head->next)

Comprueba, completa y ejecuta el siguiente ActiveCode.

.. activecode:: number-of-nodes-list-iterative-2
   :language: python
   :caption: Devuelve el número de nodos de una lista, con una iteración

   class Node:
     def __init__(self, data):
       self.data = data
       self.next = None

   class LinkedList:
     def __init__(self):
       self.head = None

     def push(self, new_data):
       new_node = Node(new_data)
       new_node.next = self.head
       self.head = new_node

     def getCountRec(self, node):
        if (not node):
          return 0
        else:
          return 1 + self.getCountRec(node.next)

     def getCount(self):
       return self.getCountRec(self.head)

   if __name__=='__main__':
     llist = LinkedList()
     llist.push(1)
     llist.push(3)
     llist.push(1)
     llist.push(2)
     llist.push(1)
     print 'Count of nodes is :',llist.getCount()


**Ejercicio 2.2**

Dada una Lista Enlazada y un número \\(n\\), escriba una función que devuelva el valor en el
\\(n\\)-ésimo desde el final de la Lista Enlazada.
La solución directa consiste en calcular la longitud *len* de la lista
y luego imprimir el *len-n+1*-ésimo nodo desde el principio de la lista.
El código activo es el siguiente.

.. activecode:: Nth-node-from-the-end
   :language: python
   :caption: Devuelve el n-ésimo nodo de la y de la lista

   class Node:
     def __init__(self, new_data):
       self.data = new_data
       self.next = None

   class LinkedList:
     def __init__(self):
       self.head = None

     def push(self, new_data):
       new_node = Node(new_data)
       new_node.next = self.head
       self.head = new_node

     def printNthFromLast(self, n):
       temp = self.head
       length = 0
       while temp is not None:
         temp = temp.next
         length += 1

        if n > length:
          print('La ubicación es mayor que la longitud de la lista')
          return
        temp = self.head
        for i in range(0, length - n):
          temp = temp.next
        print(temp.data)

   llist = LinkedList()
   llist.push(20)
   llist.push(4)
   llist.push(15)
   llist.push(35)
   llist.printNthFromLast(4)

Otra solución interesante es mantener dos punteros, el de referencia y el principal.
Inicialmente ambos apuntan a la cabeza de la lista, y el puntero de referencia se mueve a \\(n\\) nodos de la cabeza.
Luego, ambos punteros se mueven uno a uno hasta que el puntero de referencia llega al final.
Ahora el puntero principal apuntará al nodo \\(n\\)-ésimo desde el final.
El código activo es el siguiente.

.. activecode:: Nth-node-from-the-end-2
   :language: python
   :caption: Devuelve el n-ésimo nodo de la y de la lista

   class Node:
     def __init__(self, new_data):
       self.data = new_data
       self.next = None

   class LinkedList:
     def __init__(self):
       self.head = None

     def push(self, new_data):
       new_node = Node(new_data)
       new_node.next = self.head
       self.head = new_node

     def printNthFromLast(self, n):
       main_ptr = self.head
       ref_ptr = self.head

       count = 0
       if(self.head is not None):
         while(count < n ):
           if(ref_ptr is None ):
             print('La ubicación es mayor que la longitud de la lista')
             return
           ref_ptr = ref_ptr.next
           count += 1

       if(ref_ptr is None):
         self.head = self.head.next
         if(self.head is not None):
           print('El nodo número. %d del último es %d', n, main_ptr.data)
      else:
        while(ref_ptr is not None):
          main_ptr = main_ptr.next
          ref_ptr = ref_ptr.next
        print('El nodo número %d del último es %d', n, main_ptr.data)

   llist = LinkedList()
   llist.push(20)
   llist.push(4)
   llist.push(15)
   llist.push(35)
   llist.printNthFromLast(4)


**Ejercicio 2.3**

Este problema lleva el nombre de Flavio Josefo, un historiador judío que luchó contra los romanos.
Él y su grupo de soldados fueron acorralados por los romanos dentro de una cueva,
y optaron por el asesinato y el suicidio en lugar de la rendición y la captura.
Decidieron que todos los soldados se sentarán en un círculo y, empezando por el soldado sentado en la primera posición,
cada soldado matará al soldado de al lado.
Así, supongamos que hay 5 soldados sentados en un círculo con posiciones numeradas como 1, 2, 3, 4, 5.
El soldado 1 mata al 2, luego el 3 mata al 4, luego el 5 mata al 1, luego el 3 mata al 5, y como el 3 es el único que queda entonces el 3 se suicida.
Ahora Josefo no quiere ser asesinado o suicidarse, y tiene que averiguar en qué posición debe sentarse en un círculo
para ser el ultimo hombre en pie, y en lugar de suicidarse se entregara a los romanos.

Hay varias soluciones a este problema (en general, para \\(n=2^a +k\\) soldados, el sobreviviente es el \\(2k+1\\)-ésimo soldado);
aquí mostramos una solución que utiliza una lista enlazada circular.

.. activecode:: josephus-problem
   :language: python
   :caption: Problema de Josephus con una lista enlazada circular

   class Node:
     def __init__(self, x):
       self.data = x
       self.next = None

   def getJosephusPosition(m, n):
     # Crea una lista circular enlazada de tamaño n.
    head = Node(1)
    prev = head
    for i in range(2, n + 1):
      prev.next = Node(i)
      prev = prev.next
    prev.next = head

     # mientras sólo queda un nodo en la lista enlazada
     ptr1 = head
     ptr2 = head
     while (ptr1.next != ptr1):
       count = 1
       while (count != m):
         ptr2 = ptr1
         ptr1 = ptr1.next
         count += 1
       ptr2.next = ptr1.next
       ptr1 = ptr2.next

     print("La última persona que queda en pie (posición de Josefo) es ", ptr1.data)

   if __name__ == '__main__':
     n = 14
     m = 2
     getJosephusPosition(m, n)


**Exercise 2.4**

The task here is to create a doubly linked list, with head and tail pointers, by inserting nodes such that list
remains in ascending order on printing from left to right.
The pseudo code is as follows::

 si la lista está vacía, entonces
   hacer que los punteros izquierdo y derecho apunten al nodo que se va a insertar
   hacer que sus campos anterior y siguiente apunten a NULL
 si el nodo a insertar tiene un valor inferior al del primer nodo de la lista, entonces
   conectar ese nodo desde el campo anterior del primer nodo
 si el nodo a insertar tiene un valor superior al del último nodo de la lista, entonces
   conecte ese nodo desde el campo siguiente del último nodo
  si el nodo a insertar tiene un valor entre el valor del primer y el último nodo, entonces
    comprueba la posición adecuada y realiza las conexiones

Check, complete and run the following ActiveCode.

.. activecode:: insert-in-sorted-list
   :language: python
   :caption: Insertar un nuevo valor en una lista ordenada

   class Node:
   def __init__(self, data):
     self.info = data
     self.next = None
     self.prev = None

   head = None
   tail = None

   def nodeInsert( key) :
     global head
     global tail

     p = Node(0)
     p.info = key
     p.next = None

     # si el primer nodo se inserta en la lista doblemente enlazada
     if ((head) == None):
       (head) = p
       (tail) = p
       (head).prev = None
       return

     # si el nodo a insertar tiene un valor inferior al primer nodo
     if ((p.info) < ((head).info)):
       p.prev = None
       (head).prev = p
       p.next = (head)
       (head) = p
       return

     # si el nodo a insertar tiene un valor superior al último nodo
     if ((p.info) > ((tail).info)):
       p.prev = (tail)
       (tail).next = p
       (tail) = p
       return

     # encontrar el nodo antes del cual necesitamos insertar p.
     temp = (head).next
     while ((temp.info) < (p.info)):
       temp = temp.next

     # insertar nuevo nodo antes de temp
     (temp.prev).next = p
     p.prev = temp.prev
     temp.prev = p
     p.next = temp

   # imprimir los nodos de izquierda a derecha
   def printList(temp) :
     while (temp != None):
       print( temp.info, end = " ")
       temp = temp.next

   nodeInsert( 30)
   nodeInsert( 50)
   nodeInsert( 90)
   nodeInsert( 10)
   nodeInsert( 40)
   nodeInsert( 110)
   nodeInsert( 60)
   nodeInsert( 95)

   print("Lista doblemente enlazada al imprimir de izquierda a derecha\n" )
   printList(head)









Sección 3. Ejercicios y autoevaluación
----------------------------------------

**Ejercicio 3.1**

Escriba un algoritmo para encontrar el penúltimo nodo de una lista
en la que el último nodo está indicado por una referencia siguiente de None.

**Ejercicio 3.2**

Escriba un algoritmo para concatenar dos listas simples L y
M, dadas sólo las referencias al primer nodo de cada lista, en una única lista L
que contiene todos los nodos de L seguidos de todos los nodos de M.
(Pista: La concatenación no busca todos los elementos de las dos listas)

**Ejercicio 3.3**

Escriba un algoritmo recursivo que cuente el número de nodos de una lista unidireccional.
(Sugerencia: utilizar el puntero ``next`` como parámetro para la llamada de la función)

**Ejercicio 3.4**

Escribe una función que cuente el número de nodos de una lista enlazada circularmente.
(Sugerencia: Manténgase al tanto del punto de partida, y deténgase cuando lo alcance)

**Ejercicio 3.5**

Supongamos que x e y son referencias a nodos de listas enlazadas circularmente
aunque no necesariamente la misma lista. Escribe un algoritmo para saber si x e y pertenecen a la misma lista.
(Pista: Recorrer una vez una de las listas)

**Ejercicio 3.6**

En el capítulo cinco, sección 2.1, se puede encontrar una implementación de una cola con una lista enlazada circularmente.
La clase ``CircularQueue`` proporciona un método ``rotate()``, que tiene una semántica equivalente a Q.enqueue(Q.dequeue()); 
básicamente, la antigua cabeza se convierte en la nueva cola, y el nodo después de la antigua cabeza se convierte en la nueva cabeza.
Escribe el mismo método para la clase ``LinkedQueue`` de la Sección 1.2, donde una cola se implementa con una
lista individual.
(Sugerencia: Ajusta los enlaces para que el primer nodo se mueva al final de la lista)

**Ejercicio 3.7**

Escribe un método para encontrar el nodo central de una lista doblemente enlazada con centinelas de cabecera y de cola,
mediante el salto de enlaces (sin utilizar un contador).
En el caso de un número par de nodos, informe del nodo ligeramente a la izquierda del centro como el del medio.
(Sugerencia: Busca en la lista empezando por ambos extremos)

**Ejercicio 3.8**

Escribe un algoritmo rápido para concatenar dos listas doblemente enlazadas L y M
con nodos centinela de cabecera y de cola, en una sola lista L.
(Pista: Conectar adecuadamente el final de L y el principio de M)

**Ejercicio 3.9**

Escribe un método ``max(L)`` que devuelva el elemento máximo de una instancia de ``PositionalList`` L
que contiene enteros.

**Ejercicio 3.10**

Actualiza la clase ``PositionalList`` del capítulo cinco, sección 4.2, para que soporte un método adicional ``find(e)``,
que devuelve la posición del (primer) elemento e en la lista.

**Ejercicio 3.11**

Describa una implementación de los métodos ``PositionalList``, ``add_last`` y
``add_before``, utilizando únicamente los métodos ``is_empty``, ``first``, ``last``,
``prev``, ``next``, ``add_after``, y ``add_first``.

**Ejercicio 3.12**

Escribe una implementación completa del ADT de la pila y del ADT de la cola mediante una lista 
que utiliza un centinela de cabecera.

**Ejercicio 3.13**

Escribe un método ``concatenado(Q2)`` para la clase ``LinkedQueue`` que
tome todos los elementos de ``Q2`` y los añada al final de la cola original.
(Pista: Trabaja con los miembros de la cabeza y la cola de ambas listas)

**Ejercicio 3.14**

Dé una implementación recursiva de una clase de lista enlazada simple, tal que una instancia de una lista no vacía
almacena su primer elemento y una referencia a una lista de elementos restantes.
A continuación, escriba un algoritmo recursivo para invertir la lista.
(Pista: La cadena de nodos que sigue al nodo cabeza es una lista en sí misma;
para invertir la lista, recurre a las primeras posiciones de \\(n−1\\))

**Ejercicio 3.15**

Implemente una función de ``bubble-sort`` que tome como parámetro una lista L (simple o doblemente enlazada).
(Sugerencia: bubble-sort escanea la lista \\(n−1\\) veces; en cada escaneo, compara el elemento actual
con el siguiente, y los intercambia si están fuera de orden.
Una función ``swap`` sería de ayuda)

**Ejercicio 3.16**

Una matriz \\(A\\) es *espaciosa* si la mayoría de sus entradas están vacías.
Se puede utilizar una lista L para implementar un array de este tipo de forma eficiente.
Por cada celda no vacía \\(A[i]\\)de, podemos almacenar una entrada \\((i,e)\\) en L, donde e es el
elemento almacenado en \\(A[i]\\).
Esto nos permite representar \\(A\\) utilizando \\(O(m)\\) de almacenamiento, con \\(m\\) el número de entradas no vacías en \\(A\\).
Escribe la clase ``SparseArray`` con los métodos ``getitem(j)`` y ``setitem (j,e)``.
