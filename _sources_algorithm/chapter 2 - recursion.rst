Capítulo 2: Recursión
**********************

Sección 1. Introducción
-----------------------

Los bucles e iteraciones (como los bucles for y while) son la forma más habitual de implementar el concepto de repetición
de una tarea en Python. Una forma alternativa, más cercana a la naturaleza interna de las funciones, es la recursión.

La palabra *recursiva* tiene su origen en el verbo latino *recurrere*, que significa *recurrir*.
El ejemplo más usado, y abusado, de una definición recursiva es el de la función factorial, que puede definirse como
\\(n! = n * (n-1)!\\), si \\(n > 1\\) and \\(0! = 1\\). Por ejemplo, \\(4!=4*3!=4*3*2!=4*3*2*1!=4*3*2*1*0!=24\\).
Los patrones fractales son recursivos.
Las muñecas Matryoshka son recursivas.
Cada muñeca es de madera o contiene otra muñeca.

En general, una función (o un programa) se llama recursiva cuando se llama a sí misma en su cuerpo,
devolviendo el valor de esta llamada a la función.
Cada llamada debe aplicarse sobre una versión más pequeña de la entrada,
avanzando hacia un caso base (un caso en el que la función puede ser calculada sin más recursión).
En el ejemplo anterior, cuando el cómputo llega al caso \\(0!\\), se devuelve el valor  \\(1\\) a la llamada anterior.


The definition of the factorial in Python is the following::

 1  def factorial(n):
 2    if n == 0:
 3      return 1
 4    else:
 5      return n * factorial(n-1)

Como puede ver el lector, ``factorial(n)`` llama a ``factorial(n-1)``, y así sucesivamente, hasta llegar al caso base \\(0!\\).
Esta función no utiliza ningún bucle explícito, y la repetición es proporcionada por las llamadas recursivas de ``factorial``.
Cada vez que se invoca la función, su argumento es menor en uno, y cuando se alcanza el caso base
se devuelve el número entero \\(1\\).

Para seguir el comportamiento de la recursión, podemos añadir un par de ``print()`` al código::

 1  def factorial(n):
 2    print("Se llama a factorial con n = " + str(n))
 3    if n == 1:
 4      return 1
 5    else:
 6      res = n * factorial(n-1)
 7      print("Resultado intermedio para ", n, " * factorial(" ,n-1,"): ",res)
 8      return res
 9    print(factorial(4))

Este script tiene los siguientes resultados, cuando ``factorial`` es llamado con la entrada 4:

| Se llama a factorial con n = 4
| se llama a factorial con n = 3
| Se llama a factorial con n = 2
| Se llama a factorial con n = 1
| Resultado intermedio para 2 * factorial( 1 ): 2
| Resultado intermedio para 3 * factorial( 2 ): 6
| Resultado intermedio para 4 * factorial( 3 ): 24
| 24


El programa iterativo que calcula la misma función es::

 1  def iterfactorial(n):
 2    res == 1:
 3    for i in range(2,n+1):
 4      res *= i
 5    return res

Algunos lenguajes de programación (como Scheme o Smalltalk) no soportan construcciones de bucle y en su lugar se basan directamente
en la recursión para expresar la repetición. La mayoría de los lenguajes de programación soportan
la recursión utilizando el mismo mecanismo que se utiliza para apoyar las llamadas a funciones:
si una llamada de la función es recursiva, esa llamada se suspende hasta que la llamada recursiva se complete.

En Python (y en la mayoría de los lenguajes de programación), cada vez que se llama a una función
se crea una estructura conocida como *registro de activación* o *frame*
para almacenar información sobre esa invocación a la función. El registro de activación mantiene un registro
de los parámetros de la llamada a la función, de las variables locales y de la información sobre qué comando
en el cuerpo de la función se está ejecutando actualmente.
Cuando la ejecución de una función lleva a otra llamada de función, la ejecución de la primera llamada se suspende y su
de la primera llamada se suspende y su registro de activación almacena el lugar en el
código fuente en el que el flujo de control debe regresar tras la ejecución de la llamada anidada.
Este proceso se utiliza tanto en el caso estándar de una función que llama a otra
función, o en el caso recursivo en el que una función se invoca a sí misma.
Así, hay un registro de activación diferente para cada llamada activa.

La recursividad es de suma importancia en el estudio de las estructuras de datos y los algoritmos.




Sección 2. Algunos ejemplos de algoritmos recursivos
------------------------------------------------

Sección 2.1 Búsqueda binaria
=========================

La búsqueda binaria es un algoritmo recursivo que encuentra un valor objetivo dentro de una secuencia ordenada de \\(n\\) elementos.
Esta secuencia puede ser, en Python, cualquier secuencia indexable, como una lista o un array.
El enfoque directo para buscar un valor objetivo dentro de una secuencia no ordenada es utilizar el algoritmo de
algoritmo de *búsqueda secuencial*.
Se utiliza un bucle para examinar cada elemento, hasta que se encuentre el objetivo o se agote el conjunto de datos.
Este algoritmo se ejecuta en tiempo \\(O(n)\\) (es decir, tiempo lineal), ya que cada elemento debe ser inspeccionado, en el peor de los casos.

Cuando una secuencia de ``data`` está ordenada y es indexable, la búsqueda binaria es mucho más eficiente.
Obsérvese que para cualquier índice \\(j\\), todos los valores almacenados en los índices \\(0, \\ldots, j-1\\)
son menores o iguales que el valor en el índice \\(j\\),
y todos los valores almacenados en los índices \\(j+1, \\ldots ,n-1\\) son mayores o iguales que el valor del índice \\(j\\).


.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura21.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center

Un elemento de la secuencia es un *candidate* si, en la etapa actual, no podemos descartar que este elemento coincida con el objetivo.
El algoritmo tiene dos parámetros, *low* y *high*, de forma que todos los candidatos potenciales tienen un índice entre *low* y *high*.
Inicialmente, \\(low = 0\\) y \\(high =n-1\\) (es decir, todos los elementos de la secuencia son candidatos).

Comparamos el valor objetivo con el candidato mediano, es decir, el elemento ``data[mid]`` con índice
\\(mid = \\lfloor(low+high)/2 \\rfloor\\). Tenemos tres casos:

- el objetivo es igual a ``data[mid]``, entonces la búsqueda es exitosa;
- el objetivo es menor que ``data[mid]``, entonces recurrimos en la primera mitad de la secuencia (en el intervalo de índices de low a mid-1);
- el objetivo es mayor que ``data[mid]``, entonces recurrimos a la segunda mitad de la secuencia (en el intervalo de índices desde mid+1 hasta high).

Una búsqueda infructuosa se produce cuando \\(low > high\\). Obsérvese que, mientras que la búsqueda secuencial se ejecuta en tiempo \\(O(n)\\) de,
la búsqueda binaria se ejecuta en tiempo \\(O(\log n)\\) de. A continuación se presenta una implementación en Python de la búsqueda binaria::

 1  def binary_search(data, target, low, high):
 2    # Devuelve True si el objetivo se encuentra en los datos, entre el low y el high
 3    if low > high:
 4      return False               # no se ha encontrado ninguna coincidencia
 5    else:
 6      mid = (low + high) / 2
 7      if target == data[mid]:    # se ha encontrado una coincidencia
 8        return True
 9      elif target < data[mid]:
 10       return binary_search(data, target, low, mid - 1)   # llamada en la parte izquierda
 11     else:
 12       return binary_search(data, target, mid + 1, high)  # llamada en la parte derecha

En la siguiente figura buscamos el valor 18.

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura22.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center


Sección 2.2 Invertir una secuencia
================================

Sea \\(A\\) una secuencia indexada, y consideremos el problema de invertir los elementos de \\(A\\);
el primer elemento se convierte en el último, el segundo en el penúltimo, y así sucesivamente.
Nótese que es posible invertir una secuencia intercambiando el primer y el último elemento,
y revirtiendo recursivamente los elementos restantes.
A continuación se ofrece una implementación en Python::

 1  def reverse(A, first, last):
 2    # Elementos invertidos en A, entre el primero y el último
 3    if first < last - 1:                           # si hay al menos 2 elementos
 4      A[first], A[last-1] = A[last-1], A[first]    # intercambiar el nombre y el apellido
 5      reverse(A, first+1, last-1)

Si ``first == last``, no hay secuencia que invertir; si ``first == last-1``,
la secuencia está compuesta por un solo elemento. En ambos casos, hemos llegado al caso base de la recursión.
Si hay una secuencia que revertir, el algoritmo se detendrá después de \\(1+\\lfloor n/2 \\rfloor\\) pasos.
Dado que cada llamada requiere una cantidad constante de trabajo, el algoritmo se ejecuta en tiempo \\(O(n)\\) de.

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura23.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center


Sección 2.3 Suma de los elementos de una secuencia
==============================================

Sea \\(A\\) una secuencia indexada en números enteros, y consideremos el problema de sumar los elementos de \\(A\\).
Obsérvese que la suma de todos los \\(n\\) enteros de \\(A\\) es la suma de los primeros \\(n-1\\) enteros de \\(A\\),
más su último elemento.
Esto es claramente una definición recursiva, y se llama *recurrencia lineal*. A continuación proporcionamos la codificación en Python::

 1  def linear_sum(A, n):
 2     # Devuelve la suma de los primeros n números de A
 3     if n == 0:
 4       return 0
 5     else:
 6       return A[n-1] + linear_sum(A, n-1)

Para una entrada de tamaño \\(n\\), el algoritmo realiza \\(n+1\\) llamadas a funciones.
Dado que cada llamada requiere una cantidad constante de trabajo, el algoritmo se ejecuta en tiempo \\(O(n)\\).

Otra aproximación a la solución de este problema puede ser la siguiente
en lugar de sumar el último elemento de \\(A\\) con la suma de los elementos restantes,
podemos calcular la suma de la primera mitad de \\(A\\) y la suma de la segunda mitad de \\(A\\),
recursivamente, y luego sumar estos números.
Este es un caso de *recurrencia binaria*, donde se hacen dos llamadas recursivas::

 1  def binary_sum(A, first, last):
 2    # Devuelve la suma de los números de A entre el primero y el último
 3    if first <= last:     # ningún elemento
 4      return 0
 5    elif first == last-1:     # un elemento
 6      return A[first]
 7    else:
 8      mid = (first + last) / 2
 9      return binary_sum(A, first, mid) + binary_sum(A, mid, last)

Para analizar la complejidad de esta versión de la suma, consideramos el caso en el que \\(n\\) es una potencia de 2.
En cada llamada recursiva, el tamaño de la secuencia se divide por 2
lo que significa que la profundidad de la recursión es \\(1+ \\log n\\). El tiempo de ejecución del algoritmo es
sigue siendo \\(O(n)\\), porque hay \\(2n-1\\) llamadas recursivas, cada una de las cuales requiere un tiempo constante.
El mismo resultado es válido para secuencias de cualquier longitud.

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura24.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center


Sección 3. La recursión puede ser ineficiente
---------------------------------------

En esta sección, examinamos algunos problemas en los que los algoritmos recursivos provocan una drástica ineficiencia.

Sección 3.1 Los números de Fibonacci
=================================

La secuencia de Fibonacci debe su nombre al matemático Leonardo da Pisa, más conocido como Fibonacci.
En su libro "Liber Abaci" de 1200, introdujo la secuencia como un ejercicio que trataba de una población artificial de conejos, que satisfacía las necesidades de la población.
población artificial de conejos, que satisface las siguientes condiciones:

- la población inicial consiste en una pareja de conejos recién nacidos, un macho y una hembra;
- los conejos son capaces de aparearse a la edad de un mes
- los conejos son inmortales
- una pareja que se aparea produce siempre una nueva pareja (un macho, una hembra) cada mes a partir del segundo mes.

Los números de Fibonacci \\(F_n\\) son el número de parejas de conejos después de \\(n\\) meses,
es decir, después de 10 meses tendremos \\( F_{10}\\) conejos.
Dejando a un lado la importancia matemática de esta secuencia, hay que tener en cuenta que los números de Fibonacci se pueden definir como
\\(F_0 =0\\), o \\(F_1 = 1\\), o \\(F_n = F_{n-2} + F_{n-1}\\).
Este es un ejemplo típico de recursión binaria, y podemos escribir fácilmente el código Python para esta función::

 1  def fibonacci(n):
 2    # Devuelve el número n-ésimo de Fibonacci
 3    if n <= 1:
 4      return n
 5    else:
 6      return fibonacci(n-2) + fibonacci(n-1)

Sea \\(c_n\\) el número de llamadas realizadas en una ejecución de ``fibonacci(n)``.
Tenemos los siguientes valores para \\(c_n\\):

| \\(c_0 = 1\\)
| \\(c_1 = 1\\)
| \\(c_2 = 1+c_0+c_1 = 1+1+1 = 3\\)
| \\(c_3 = 1+c_1+c_2 = 1+1+3 = 5\\)
| \\(c_4 = 1+c_2+c_3 = 1+3+5 = 9\\)
| \\(c_5 = 1+c_3+c_4 = 1+5+9 = 15\\)
| \\(c_6 = 1+c_4+c_5 = 1+9+15 = 25\\)
| \\(c_7 = 1+c_5+c_6 = 1+15+25 = 41\\)
| \\(c_8 = 1+c_6+c_7 = 1+25+41 = 67\\)

Vemos que la aplicación natural de la fórmula de Fibonacci da lugar a un crecimiento casi exponencial del
el número de llamadas;
Esto sucede porque nuestra definición no tiene memoria de los valores previamente calculados de la secuencia, lo que significa que
cada llamada tiene que volver a calcular valores que ya han sido calculados. Por ejemplo, al calcular ``fibonacci(6)``,
tenemos dos llamadas a ``fibonacci(4)``, una hecha directamente por ``fibonacci(6)``, y la segunda hecha por ``fibonacci(5)``.
Del mismo modo, tenemos tres llamadas a ``fibonacci(3)``: la primera, hecha por ``fibonacci(5)``;
la segunda y la tercera hechas por las dos instancias de ``fibonacci(4)``.

Hay formas más eficientes de redefinir este programa. Una primera aproximación consiste en definir un programa que devuelva
dos números de Fibonacci consecutivos, \\( ( F_n ,F_{n-1} ) \\) empezando por \\( F_{1}=0 \\).
Esto nos permite recordar el valor
valor calculado de la secuencia, y pasarlo al siguiente nivel del cálculo, en lugar de volver a calcularlo::

 1  def good_fibonacci(n):
 2    # Devuelve el n-ésimo y el (n-1)-ésimo número de Fibonacci
 3    if n <= 1:
 4      return (n,0)
 5    else:
 6      (a,b) = good_fibonacci(n-1)
 7      return (a+b,a)

La función ``good_fibonacci(n)`` puede ser calculada en tiempo \\(O(n)\\).
En efecto, cada llamada recursiva disminuye en 1 el argumento \\(n\\) de la función, lo que implica \\(n\\) necesidad de llamar a la función.
El trabajo no recursivo de cada llamada requiere un tiempo constante; por lo tanto, el cálculo global se ejecuta en tiempo de \\(O(n)\\).

También podemos implementar una memoria utilizando un diccionario para guardar los valores previamente calculados::

 1  memo = {0:0, 1:1}
 2  def fibm(n):
 3    if not n in memo:
 4      memo[n] = fibm(n-1) + fibm(n-2)
 5    return memo[n]

Un programa final puede ser definido usando el método ``__call__``:

 1  class Fibonacci:
 2    def __init__(self, a=0, b=1):
 3      self.memo = { 0:i1, 1:i2 }
 4    def __call__(self, n):
 5      if n not in self.memo:
 6        self.memo[n] = self.__call__(n-1) + self.__call__(n-2)
 7      return self.memo[n]
 8
 9  fib = Fibonacci()
 10 lucas = Fibonacci(2, 1)
 11
 12 for i in range(1, 16):
 13   print(i, fib(i), lucas(i))

El programa devuelve la siguiente salida:

| 1 1 1
| 2 1 3
| 3 2 4
| 4 3 7
| 5 5 11
| 6 8 18
| 7 13 29
| 8 21 47
| 9 34 76
| 10 55 123
| 11 89 199
| 12 144 322
| 13 233 521
| 14 377 843
| 15 610 1364

Los *números de Lucas* o la *serie de Lucas* son una secuencia de números enteros que llevan el nombre del matemático
Francois Edouard Anatole Lucas (1842-91). Los números de Lucas tienen la misma regla de creación que el
número de Fibonacci, pero los valores de 0 y 1 son diferentes.


Sección 3.2 Problema de unicidad de los elementos
======================================

Dada una secuencia \\(A\\) con \\(n\\) elementos, el *problema de unicidad de elementos*
consiste en decidir si todos los elementos de esa secuencia son distintos entre sí.

La primera solución a este problema utiliza un algoritmo iterativo. En concreto, para cada par de índices
\\(i\\) y \\(j\\), con \\(i<j\\), se comprueba si \\(A[i]\\) y \\(A[j]\\) son iguales::

 1  def unique1(A):
 2    # Devuelve True si no hay elementos duplicados en A
 3    for i in range(len(A)):
 4      for j in range(i+1, len(A)):
 5        if A[i] == A[j]:
 6          return False       # hay un par duplicado
 7    return True              # no hay elementos duplicados

Como el lector puede ver, hay dos bucles anidados; las iteraciones del bucle exterior provocan
\\(n-1, n-2, \\ldots, 2, 1\\) iteraciones del bucle interior, respectivamente. Esto implica que en el peor de los casos
(cuando no hay elementos duplicados) el programa se ejecuta durante \\((n-1)+(n-2)+ \\ldots +2+1\\) pasos,
es decir, para \\(O(n^2)\\) pasos.

Un segundo algoritmo para el problema de la unicidad del elemento se basa en el requisito de que
la secuencia \\(A\\) tiene que estar ordenada.
Esto implica que, si existen duplicados de un elemento, estas copias se colocarán todas juntas en la secuencia.
Se necesita una sola pasada sobre la secuencia, comparando cada elemento con el siguiente.
Una codificación en Python de este algoritmo es la siguiente::

 1  def unique2(A):
 2    # Devuelve True si no hay elementos duplicados en A
 3    temp = sorted(A)
 4    for i in range(1, len(temp)):
 5      if temp[i-1] == temp[i]:
 6        return False       # hay un par duplicado
 7    return True            # no hay elementos duplicados

Si la secuencia \\(A\\) está ordenada, el algoritmo sólo necesita \\(O(n)\\) pasos para decidir el problema; el consumo total de tiempo
El consumo total de tiempo es de \\(O(n \\log n)\\), dada la complejidad temporal de la función de ordenación.

Un algoritmo recursivo muy ineficiente se basa en el siguiente supuesto
como caso base, cuando \\(n = 1\\), los elementos son trivialmente únicos;
para \\(n > 2\\), los elementos son únicos si y sólo si
los primeros elementos de \\(n-1\\) son únicos, los últimos elementos de\\(n-1\\) son únicos, y el primer y el último elemento
son diferentes. A continuación se ofrece una implementación recursiva::

 1  def unique3(A, first, last):
 2    # Devuelve True si no hay elementos duplicados en A[first:last]
 3    if last - first <= 1:
 4      return True               # sólo hay un artículo
 5    elif not unique3(A, first, last-1):
 6      return False              # la primera parte tiene un duplicado
 7    elif not unique3(a, first+1, last):
 8      return False              # la segunda parte tiene duplicado
 9    else:
 10     return A[first] != A[last-1]       # el primer y el último elemento difieren

Sea \\(n\\) el número de elementos de la secuencia, es decir, \\(n\\)= ``last-first``.
Si \\(n=1\\), entonces el tiempo de ejecución de ``unique3`` es \\(O(1)\\). En el caso general, hay que tener en cuenta que una
a ``unique3`` da lugar a dos llamadas recursivas en problemas de tamaño \\(n-1\\).
Esas dos llamadas podrían dar lugar a cuatro llamadas en problemas de tamaño \\(n-2\\), y así sucesivamente.
El número total de llamadas a la función viene dado por el sumatorio  \\(1+2+4+ \\ldots + 2^{n-1}\\),
que es igual a \\(2^{n-1}\\). Por lo tanto, el tiempo de ejecución de la función ``unique3`` es \\(O(2^n)\\) de la función.)

Una solución mejorada se da en el siguiente script::

 1  def unique(A, first, last):
 2    if last-first <= 1:                # sólo hay un artículo
 3      return True
 4    elif A[first] in A[first+1:]:      # si el primer elemento está en el resto de la lista
 5      return False
 6    else:
 7      return unique(A, first+1, last)  # siguiente elemento
