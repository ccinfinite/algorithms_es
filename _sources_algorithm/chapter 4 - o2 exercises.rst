

Capítulo 4: Pila, colas y decolas - ejercicios y retos
********************************************************************



Sección 1. Revisión de la teoría
-------------------------------

**Ejercicio 1.1**

Completa y ejecuta el siguiente trozo de código (que puedes encontrar en el capítulo cuatro)
en el que se implementa un ADT de pila mediante una lista de Python.
Muestra algunos ejemplos de cómo funciona la pila, y luego añade la clase de excepción ``Empty``,
que se lanza cuando el usuario llama a ``pop`` o ``top`` en una pila vacía.

.. activecode:: stack-as-a-list
   :language: python
   :caption: Implementación de una pila mediante una lista

   class ArrayStack:
   # Implementación de la pila LIFO utilizando una lista de Python como almacenamiento

     # Crea una pila vacía
     def __ init __ (self):
       self._data = [ ]

     # Devuelve el número de elementos de la pila
     def __ len __ (self):
       return len(self._data)

     # Devuelve True si la pila está vacía
     def is_empty(self):
       return len(self._data) == 0

     # Añade el elemento e a la parte superior de la pila
     def push(self, e):
       self._data.append(e)

     # Devuelve (pero no elimina) el elemento en la parte superior de la pila
     # Lanza una excepción si la pila está vacía
     def top(self):
       if self.is_empty( ):
         raise Empty( Stack is empty )
       return self._data[−1]

     # Eliminar y devolver el elemento de la parte superior de la pila
     # Lanza una excepción si la pila está vacía
     def pop(self):
       if self.is_empty( ):
         raise Empty( 'Stack is empty' )
       return self._data.pop( )

           # INSERTE EL NUEVO CÓDIGO AQUÍ


**Ejercicio 1.2**

Escribe una implementación sencilla de un ADT de cola, adaptando una lista de Python;
mostrar algunos ejemplos de cómo funciona la cola (ver Sección 2.1 del Capítulo Cuatro para algunas sugerencias:
cuando se llama a ``pop(0)`` para eliminar el primer elemento de la lista,
todos los elementos restantes deben ser desplazados a la izquierda).

.. activecode:: queue-as-a-list
   :language: python
   :caption: Implementación de una cola mediante una lista

     # INSERTE EL NUEVO CÓDIGO AQUÍ



A continuación, escriba una implementación más eficiente de una cola ADT basada en un array circular (como en la Sección 2.2
del Capítulo 4); muestra algunos ejemplos de cómo funciona la cola.

.. activecode:: queue-as-a-circular-array
   :language: python
   :caption: Implementación de una cola mediante una matriz circular

   class ArrayQueue:
   # cola implementada con una lista
     DEFAULT = 10          # capacidad de todas las colas

     def __init__(self):
       self._data = [None]*ArrayQueue.DEFAULT
       self._size = 0
       self._front = 0

     def __len__(self):
       return self._size

     def is_empty(self):
       return self._size == 0

     def first(self):
       if self.is_empty( ):
         raise Empty('Queue is empty')
       return self._data[self._front]

     def dequeue(self):
       if self.is_empty( ):
         raise Empty('Queue is empty')
       element = self._data[self._front]
       self._data[self._front] = None
       self._front = (self._front + 1) % len(self._data)
       self._size −= 1
       return element

     def enqueue(self, e):
       if self._size == len(self._data):
         self._resize(2*len(self.data))           # duplicar el tamaño de la matriz
      position = (self._front + self._size) % len(self._data)
      self._data[position] = e
      self._size += 1

     def _resize(self, capacity):
       old = self._data                   # mantiene el seguimiento de la lista existente
       self._data = [None]*capacity       # asignar la lista con la nueva capacidad
       walk = self._front
       for k in range(self._size):
         self._data[k] = old[walk]        # desplaza los índices
         walk = (1 + walk) % len(old)     # utilizar el tamaño antiguo
       self._front = 0                    # frente de realigs

          # INSERTE EL NUEVO CÓDIGO AQUÍ


**Ejercicio 1.3**

Escribe una implementación sencilla de un ADT de cola de doble terminación, adaptando una lista de Python;
mostrar algunos ejemplos de cómo funciona la cola (ver Sección 3 del Capítulo 4).

.. activecode:: deque-as-a-list
   :language: python
   :caption: Implementación de un deque mediante una lista

     # INSERTE EL NUEVO CÓDIGO AQUÍ




Sección 2. Problemas de pila, colas, deque's
---------------------------------------------

En esta sección, nos enfrentaremos a algunos problemas clásicos que pueden resolverse utilizando el ADT de pila, cola y deque.
Estudia cada ejercicio, luego comprueba, completa (si es necesario) y ejecuta el ActiveCode proporcionado.

**Ejercicio 2.1**

Proporcione un algoritmo que convierta números enteros decimales (es decir, números representados en base 10) en números binarios
(es decir, números representados en base 2).

La solución a este problema utilizará una pila para llevar la cuenta de los dígitos del resultado binario parcial.
Comenzamos con un número decimal mayor que 0.
Entonces iteramos la división del número entre 2, y empujamos el resto (0 o 1) en la pila.
Esto significa que estamos construyendo el número binario como una secuencia de dígitos;
el primer resto que calculemos será el último dígito de la secuencia.
Al final de la iteración, los dígitos binarios se sacan de la pila y se añaden al extremo derecho de una cadena,
que representará el número en base 2.

Comprueba, completa y ejecuta el siguiente ActiveCode.

.. activecode:: decimal-binary-convertion
   :language: python
   :caption: Conversion of a decimal number to binary representation

   from pythonds.basic import Stack

   def dectobin(decnumber):
     remstack = Stack()
     while decnumber > 0:
       remstack.push(decnumber % 2)
       decnumber = decnumber // 2

     binnumber = ""
     while not remstack.isEmpty():
       binnumber = binnumber + str(remstack.pop())
     return binnumber

   print(dectobin(51))




**Ejercicio 2.2**

Un palíndromo es una cadena que se lee igual hacia delante y hacia atrás (por ejemplo, radar, señora, o señora soy Adán).
Escribe un algoritmo para introducir una cadena de caracteres y comprobar si es un palíndromo.

La solución a este problema utilizará un deque para almacenar los caracteres de la cadena.
Añadimos cada nuevo carácter a la parte posterior del deque;
entonces, comparamos la parte delantera del deque (es decir, el primer carácter de la cadena)
y la parte posterior del deque (es decir, el último carácter de la cadena), y continuamos si coinciden.
Finalmente, nos quedamos sin caracteres o sólo tenemos uno en el deque, dependiendo de la longitud de la cadena original
original (par o impar, respectivamente). En ambos casos, la cadena debe ser un palíndromo.

Comprueba, completa y ejecuta el código en el siguiente ActiveCode.

.. activecode:: palindrome-checker
   :language: python
   :caption: Comprobar si una cadena es un palíndromo o no

   from pythonds.basic import Deque

   def palcheck(aString):
     word = Deque()
     for c in aString:
       word.addRear(c)

     equal = True
     while word.size() > 1 and equal:
       first = word.removeFront()
       last = word.removeRear()
       if first != last:
         equal = False
     return equal

   print(palcheck("lsdkjfskf"))
   print(palcheck("radar"))



**Ejercicio 2.3**

*La notación postfija es una forma inequívoca de escribir una expresión aritmética
sin paréntesis. Si *(exp1) op (exp2)* es una expresión *infija* cuyo operador es *op*,
la versión postfija de ésta es *pexp1 pexp2 op*, donde *pexp1* es la versión postfija de
*exp1* y *pexp2* es la versión postfija de *exp2*.
Por ejemplo, la versión postfija de \\( ((3+2) * (5−3))/2\\) es \\(3 2 + 5 3 − * 2 / \\).

Para proporcionar un algoritmo que evalúe una expresión que está escrita en notación postfija,
nota que cada vez que un operador se ve en la entrada, los dos más recientes
se utilizarán en la evaluación.
Por ejemplo, consideremos la expresión postfija \\(3 2 5 * +\\)
(es decir, la expresión infija \\( 3 + 2 * 5 \\) ).

Al recorrer la expresión de izquierda a derecha, se encuentran primero los operandos 3, 2 y 5, 
y coloca cada uno de ellos en una pila, haciéndolos disponibles si viene un operador.
Cuando se encuentra un operador (``*``, en este ejemplo) los dos operandos más recientes
deben ser utilizados en una operación de multiplicación. Al abrir la pila dos veces,
podemos obtener los operandos adecuados y luego realizar la multiplicación
(en este caso obteniendo el resultado 10).
Ahora podemos empujar este resultado a la pila para que pueda ser utilizado como un operando
para los operadores posteriores de la expresión.
Cuando se procese el último operador, sólo quedará un valor en la pila,
el valor de la expresión. A continuación, mostramos un pseudocódigo de este algoritmo::

 Crear una pila vacía S
 Escanear la expresión de izquierda a derecha
     if the element is an operand
       push it onto S;
     if the element is an operator, *, /, +, or -
       pop from S twice
       perform the arithmetic operation
       push the result back onto S
 Cuando la expresión termina, el resultado está en la pila.

Complete y ejecute el siguiente ActiveCode, en el que la definición de una función
para la evaluación de una expresión postfija.
Para ayudar con la aritmética, se define una función de ayuda ``doMath`` que tomará dos operandos
y un operador y luego realiza la operación aritmética adecuada.

.. activecode:: postfix-evaluation
   :language: python
   :caption: Evaluación de una expresión postfija mediante una pila

   from pythonds.basic import Stack

   def postfixEval(postfixExpr):
     operandStack = Stack()
     tokenList = postfixExpr.split()

     for token in tokenList:
       if token in "0123456789":
         operandStack.push(int(token))
       else:
         operand2 = operandStack.pop()
         operand1 = operandStack.pop()
         result = doMath(token,operand1,operand2)
         operandStack.push(result)
     return operandStack.pop()

   def doMath(op, op1, op2):
     if op == "*":
       return op1 * op2
     elif op == "/":
       return op1 / op2
     elif op == "+":
       return op1 + op2
     else:
       return op1 - op2

   print(postfixEval('7 8 + 3 2 + /'))













Sección 3. Ejercicios y autoevaluación
----------------------------------------

**Ejercicio 3.1**

¿Qué valores se devuelven cuando se ejecutan las siguientes series de operaciones sobre una pila vacía?
``push(5)``, ``push(3)``, ``pop()``, ``push(2)``, ``push(8)``, ``pop()``, ``pop()``,
``push(9)``, ``push(1)``, ``pop()``, ``push(7)``, ``push(6)``, ``pop()``, ``pop()``, ``push(4)``, ``pop()``, ``pop()``.
(Pista: Utiliza una de las implementaciones anteriores del ADT de la pila)

**Ejercicio 3.2**

Cuál es el tamaño actual de una pila S, inicialmente vacía, tras la ejecución de 25 ``push``,
12 ``top``, y 10 ``pop``, 3 de los cuales generaron errores ``Empty`` que fueron capturados e ignorados?
(Pista: Utiliza una de las implementaciones anteriores del ADT de la pila)

**Ejercicio 3.3**

Escribe una función ``transfer(S,T)`` que transfiera los elementos de la pila S a la pila T
de forma que el elemento que está en la parte superior de S sea el primero en ser insertado en T,
y el elemento que está en la parte inferior de S acabe en la parte superior de T.
(Pista: Transfiere los elementos de uno en uno)

**Ejercicio 3.4**

Escribe un método recursivo que elimine todos los elementos de una pila.

**Ejercicio 3.5**

Escriba una función que invierta una lista de elementos.
(Sugerencia: Empuje los elementos de la lista en una pila en un orden, y sáquelos y escríbalos de nuevo a la lista)

**Ejercicio 3.6**

¿Qué valores se devuelven cuando se ejecuta la siguiente serie de operaciones sobre una cola vacía?
``enqueue(5)``, ``enqueue(3)``, ``dequeue()``, ``enqueue(2)``, ``enqueue(8)``, ``dequeue()``, ``enqueue(9)``,
``enqueue(1)``, ``dequeue()``, ``enqueue(7)``, ``enqueue(6)``, ``dequeue()``, ``dequeue()``,
``enqueue(4)``, ``dequeue()``, ``dequeue()``.
(Pista: Utiliza una de las implementaciones anteriores del ADT de cola)

**Ejercicio 3.7**

¿Cuál es el tamaño actual de una cola Q, inicialmente vacía, tras la ejecución de 32 ``enqueue``?
10 ``first``, y 15 ``dequeue``, 5 de los cuales generaron errores ``Empty`` que fueron capturados e ignorados?
(Pista: Utiliza una de las implementaciones anteriores del ADT de cola)

**Ejercicio 3.8**

¿Qué valores se devuelven durante la ejecución de la siguiente secuencia de operaciones sobre un deque vacío?
``add_first(4)``, ``add_last(8)``, ``add_last(9)``, ``add_first(5)``, ``back()``, ``delete_first( )``,
``delete_last( )``, ``add_last(7)``, ``first( )``, ``last( )``, ``add_last(6)``, ``delete_first( )``, ``delete_first( )``.
(Pista: Utiliza una de las implementaciones anteriores del ADT deque)

**Ejercicio 3.9**

Un deque D contiene los números (1,2,3,4,5,6,7,8), en este orden, y
la cola Q está inicialmente vacía. Escribe un fragmento de código que utilice sólo D y Q
y que dé como resultado que D almacene los elementos en el orden (1,2,3,5,4,6,7,8).
(Sugerencia: Utiliza el valor de retorno de un método de eliminación como parámetro de un método de inserción)

**Ejercicio 3.10**

Repita el ejercicio anterior utilizando una pila S en lugar de la cola Q.

**Ejercicio 3.11**

Dados tres enteros distintos en una pila S, en orden aleatorio,
escriba un fragmento de pseudocódigo (sin bucles ni recursividad) que utilice sólo una comparación
y una sola variable x, y que tenga como resultado que la variable x almacene el mayor de los tres enteros.
(Sugerencia: Haga aparecer el mayor de los enteros, y recuérdelo)

**Ejercicio 3.12**

Dadas tres pilas no vacías R, S y T, describa una secuencia de operaciones que dé como resultado que S almacene todos los elementos
originalmente en T debajo de todos los elementos originales de S, con ambos conjuntos de elementos en su orden original.
Por ejemplo, si R = [1,2,3], S = [4,5], y T = [6,7,8,9], inicialmente
la configuración final debería tener S = [6,7,8,9,4,5].
(Sugerencia: Utiliza R como almacenamiento temporal, sin saltar su contenido original)

**Ejercicio 3.13**

Describa cómo implementar el ADT de la pila utilizando una sola cola como variable de instancia,
y sólo constante memoria local adicional dentro de los cuerpos de los métodos.
(Sugerencia: rotar los elementos dentro de la cola)

**Ejercicio 3.14**

Describa cómo implementar la ADT de cola utilizando dos pilas como variables de instancia.
(Sugerencia: Utilizar una pila para recoger los elementos entrantes, y la otra como un buffer para los elementos a entregar)

**Ejercicio 3.15**

Describa cómo implementar el ADT de cola doble utilizando dos pilas como variables de instancia.
(Sugerencia: Utilice una pila para cada extremo de la cola)

**Ejercicio 3.16**

Dada una pila S que contiene \\(n)\\ elementos y una cola Q, inicialmente vacía,
muestre cómo usar Q para escanear S para ver si contiene un determinado elemento \\(x\\);
el algoritmo debe devolver los elementos a S en su orden original.

**Ejercicio 3.17**

Dé una implementación completa de ``ArrayDeque`` de la cola doble ADT.



**Ejercicio 3.18**

Las pilas se pueden utilizar para proporcionar soporte de "undo" en aplicaciones como un navegador web o un editor de texto.
Esta funcionalidad puede ser implementada con una pila no limitada,
pero muchas aplicaciones lo soportan con una pila de capacidad fija, como sigue:
cuando se invoca ``push`` con la pila a plena capacidad
el elemento empujado se inserta en la parte superior,
mientras que el elemento más antiguo se escapa de la parte inferior de la pila para hacer espacio.
Dé una implementación de tal ``Stack``, usando un array circular
con la capacidad de almacenamiento adecuada.
