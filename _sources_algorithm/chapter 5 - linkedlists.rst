
Capítulo 5: Listas enlazadas
**************************

En este capítulo, introducimos una estructura de datos conocida como *lista enlazada*
que proporciona una alternativa a la secuencia basada en arrays de Python.
Tanto las secuencias basadas en arrays como las listas enlazadas mantienen los elementos en un cierto orden, utilizando un estilo muy diferente.
Un array proporciona una representación centralizada,
con una parte de la memoria utilizada para acomodar las referencias a los elementos.
Una lista enlazada se basa en una representación más distribuida en la que un objeto, conocido como *nodo*,
se asigna a cada elemento. Cada nodo mantiene una referencia a su elemento y una o más referencias a los nodos vecinos
para representar el orden lineal de la secuencia.

Como es habitual, hay que tener en cuenta una contrapartida al comparar las secuencias basadas en arrays y las listas enlazadas.
No se puede acceder a los elementos de una lista enlazada de forma eficiente mediante un índice numérico,
y no podemos saber, examinando un nodo, su posición en la lista.
Sin embargo, las listas enlazadas evitan algunas desventajas señaladas anteriormente para las secuencias basadas en arrays.


Sección 1. Listas simples enlazadas
------------------------------

Una *lista enlazada simple* es una colección de nodos que forman una secuencia lineal.
Cada nodo almacena un campo *elemento*, que es una referencia a un objeto de la secuencia,
así como un campo *siguiente*, que es una referencia al siguiente nodo de la lista,
como en la siguiente figura.

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura51.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center

El primer y el último nodo de una lista enlazada se conocen como *cabeza* y *cola* de la
lista, respectivamente. El proceso de pasar de un nodo al siguiente,
siguiendo la *siguiente* referencia de cada nodo, hasta alcanzar la cola de la lista,
se llama *atravesar* la lista enlazada, o *saltar de enlace*, o *saltar de puntero*.
La cola es el nodo que tiene *Ninguna* como siguiente referencia.

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura52.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center

Cada nodo se representa como un objeto que almacena una referencia a su elemento y una referencia al siguiente nodo.
Para representar toda la lista debemos guardar una referencia a la cabeza de la lista.
Todos los demás nodos pueden localizarse recorriendo la lista;
esto implica que no es necesario almacenar una referencia directa a la cola de la lista,
aunque sea una práctica habitual para evitar ese recorrido.
Del mismo modo, se almacena el número total de nodos (el *tamaño*) de la lista
con el fin de evitar recorrer la lista para contar los nodos.
Para simplificar, consideraremos que el elemento de un nodo está incrustado directamente en la estructura del nodo,
como en la siguiente figura.

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura53.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center

**Inserción de un elemento en la cabecera de una lista enlazada simple**

Una lista enlazada no tiene un tamaño fijo predeterminado, siendo este tamaño igual o proporcional
al número actual de elementos.
Se puede insertar un nuevo elemento en la cabeza de la lista, como se muestra en el siguiente pseudocódigo.
Se crea una nueva instancia de nodo, su elemento se establece en el nuevo elemento, su siguiente enlace se refiere
a la cabeza actual, y la cabeza de la lista se establece para apuntar al nuevo nodo::

 1  add_first(L,e):
 2    newnode = Node(e)
 3    newnode.next = L.head
 4    L.head = newnode
 5    L.size = L.size+1

En la siguiente figura mostramos la inserción de un elemento en la cabeza de la lista, antes de
la inserción, después de la creación de un nuevo nodo, y después de la reasignación de la cabeza, respectivamente.

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura54.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center


**Inserción de un elemento en la cola de una lista unicatenaria**

Se puede insertar un elemento en la cola de la lista, siempre y cuando mantengamos una referencia al nodo cola,
como se muestra en el siguiente pseudocódigo. Se crea un nuevo nodo, con su siguiente referencia igual a
la siguiente referencia de la cola actual apuntando a este nuevo nodo,
y con la propia referencia de la cola actualizada a este nuevo nodo::

 1  add_last(L,e):
 2    newnode = Node(e)
 3    newnode.next = None
 4    L.tail.next = newnode
 5    L.tail = newnode
 6    L.size = L.size+1


En la siguiente figura mostramos la inserción de un elemento en la cola de una lista unifilar,
antes de la inserción, después de la creación de un nuevo nodo, y después de la reasignación de la referencia de la cola,
respectivamente.

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura55.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center


**Eliminación de un elemento de una lista enlazada**

Esta operación se ilustra en el siguiente fragmento de código::

 1  remove_first(L):
 2    if L.head == None: error  # the list is empty
 3    L.head = L.head.next
 4    L.size = L.size−1

En la siguiente figura mostramos la eliminación de un elemento en la cabecera de una lista unifilar,
antes de la eliminación, después de dimitir la antigua cabeza, y en su configuración final,
respectivamente.

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura56.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center

Hay que tener en cuenta que borrar el último nodo de una lista unifilar no es una tarea fácil.
Para ello, debemos ser capaces de acceder al nodo *before* del último nodo,
y esto se puede hacer de forma única empezando por la cabeza de la lista y buscando
todo el camino a través de ella. Esto requiere un número de saltos de enlace que es igual al
tamaño de la lista. Si queremos realizar esta operación de forma eficiente,
tendremos que hacer nuestra lista doblemente enlazada.


Sección 1.1 Implementación de una pila con una lista doblemente enlazada
===========================================================

En esta sección, proporcionamos una implementación en Python del tipo de datos abstracto pila, mediante una lista enlazada simple.
Como todas las operaciones de la pila afectan a la parte superior, orientamos la parte superior de la pila en la cabeza de la lista.
Esta es la mejor opción: podemos insertar y eliminar elementos de forma eficiente y en tiempo constante en la cabeza de la lista.

Para representar cada nodo de la lista se define una clase ``Node``;
esta clase no debe ser accesible directamente por el usuario de la pila,
por lo que la definimos como una clase no pública y anidada de la clase ``LinkedStack``.
La definición de ``Node`` se muestra en el siguiente código::

 class _Node:
   __slots__ = '_element' , '_next'        # racionalizar el uso de la memoria
   def __init__ (self, element, next):     # inicializar los campos del nodo
     self._element = element               # referencia al elemento del usuario
     self._next = next                     # referencia al siguiente nodo

Un nodo tiene dos variables de instancia: ``_element`` y ``_next``.
Definimos ``__slots__`` para agilizar el uso de la memoria, ya que puede haber muchas instancias de nodos en una misma lista.
El constructor de la clase ``_Node`` está diseñado para permitirnos
especificar los valores iniciales de los dos campos de un nodo recién creado.

En el siguiente código, cada instancia de la pila mantiene dos variables.
El miembro ``_head`` es una referencia al nodo que encabeza la lista (Ninguno, si la pila está vacía).
La variable ``_size`` mantiene un registro del número actual de elementos.

La implementación de ``push`` es similar al pseudocódigo de inserción en la cabeza de una lista
de una lista enlazada, como se ha explicado anteriormente.
Cuando se inserta un nuevo elemento en la pila, se cambia la estructura enlazada
llamando al constructor de la clase ``_Node`` como sigue::

 self._head = self._Node(e, self._head)

Tenga en cuenta que el campo ``_next`` del nuevo nodo se establece en el nodo superior existente, y luego
``self._head`` se reasigna al nuevo nodo::

 1  class LinkedStack:
 2    # Implementación de la pila con una lista enlazada individualmente
 3
 4    # Clase de nodo anidado en la pila
 5    class Node:
 6      __slots__ = '_element' , '_next'
 7      def __init__(self, element, next):
 8        self._element = element
 9        self._next = next
 10
 11   # Métodos de pila
 12   def __init__(self):
 13     self._head = None
 14     self._size = 0
 15
 16   def __len__(self):
 17     return self._size
 18
 19   def is_empty(self):
 20     return self._size == 0
 21
 22   def push(self, e):
 23     self._head = self._Node(e, self._head)
 24     self._size += 1
 25
 26   def top(self):
 27     if self.is_empty( ):
 28       raise Empty('Stack is empty')
 29     return self._head._element
 30
 31   def pop(self):
 32     if self.is_empty( ):
 33       raise Empty('Stack is empty')
 34     answer = self._head._element
 35     self._head = self._head._next
 36     self._size −= 1
 37     return answer

El objetivo del método ``top`` es devolver el elemento que está en la parte superior de la pila.
Cuando la pila está vacía, se lanza una excepción ``Empty``.
Cuando la pila no está vacía, ``self._head`` es la referencia al primer nodo de la lista enlazada,
y el elemento superior es ``self._head._element``.
En cuanto a ``pop``, se mantiene una referencia local al elemento que se almacena en el nodo que se está eliminando
y se devuelve ese elemento.
Nótese que todos los métodos, en el peor de los casos, utilizan un tiempo constante.


Sección 1.2 Implementación de una cola con una lista enlazada simple
==========================================================

En esta sección, proporcionamos una implementación en Python del tipo de datos abstracto cola, mediante una lista enlazada simple.
Necesitamos realizar operaciones en ambos extremos de la cola, lo que significa que mantenemos
una referencia a la cabeza y a la cola como variables de instancia para cada cola.
A continuación, alineamos la parte delantera de la cola con la cabeza de la lista, y la parte trasera de la cola con la cola de la lista;
De este modo, podemos poner en cola los elementos de la parte trasera y retirarlos de la parte delantera.
La implementación de una clase LinkedQueue se da en el siguiente código::

 1  class LinkedQueue:
 2    # Implementación de la cola con una lista enlazada simple
 3
 4    class Node:
 5      __slots__ = '_element' , '_next'
 6      def __init__(self, element, next):
 7        self._element = element
 8        self._next = next
 9
 10   def __init__ (self):
 11     self._head = None
 12     self._tail = None
 13     self._size = 0
 14
 15   def __len__ (self):
 16     return self._size
 17
 18   def is_empty(self):
 19     return self._size == 0
 20
 21   def first(self):
 22     if self.is_empty( ):
 23       raise Empty('Queue is empty')
 24     return self._head._element
 25
 26   def dequeue(self):
 27     if self.is_empty( ):
 28       raise Empty('Queue is empty')
 29     answer = self._head._element
 30     self._head = self._head._next
 31     self._size −= 1
 33     if self.is_empty( ):       # caso especial si la cola está vacía
 33       self._tail = None        # la cabeza removida había sido la cola
 34     return answer
 35
 36   def enqueue(self, e):
 37     newnode = self._Node(e, None)
 38     if self.is_empty( ):
 39       self._head = newnode
 40     else:
 41       self._tail._next = newnode
 42     self._tail = newnode
 43     self._size += 1

Muchos aspectos de la implementación anterior son similares a los de la clase ``LinkedStack``;
por ejemplo, la definición de la clase ``Node``.
La implementación de ``dequeue`` para ``LinkedQueue`` es similar a la de ``pop`` para ``LinkedStack``:
ambos eliminan la cabeza de la lista enlazada, con la diferencia de que la cola mantiene la referencia de la cola.
En general, una operación en la cabeza no tiene efecto en la cola,
pero cuando se llama a ``dequeue`` en una cola con un elemento, eliminamos la cabeza y la cola de la lista
simultáneamente; por lo tanto, ponemos ``self._tail`` a None.
Algo similar ocurre en la implementación de ``enqueue``.
El nuevo nodo es la nueva cola, pero cuando el nuevo nodo es el único nodo de la lista, también se convierte en la nueva cabeza;
En caso contrario, el nuevo nodo debe enlazarse inmediatamente después del nodo de cola ya existente.
Todas las operaciones se ejecutan en tiempo constante en el peor de los casos, y el uso de espacio es lineal en el número actual de elementos.


Sección 2. Listas enlazadas circularmente
----------------------------------

Una lista enlazada en la que la cola de la lista utiliza su siguiente referencia para apuntar a la cabeza
de la lista se llama *lista enlazada circularmente*.
Esto ofrece un modelo general para conjuntos de datos que son cíclicos, es decir
que no tienen ninguna noción particular de un principio y un final, sino que mantienen una
referencia a un nodo particular para utilizar la lista, en su lugar.
Dicho nodo se denomina nodo *actual*, y avanzamos por los nodos de la lista
utilizando current.next.

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura57.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center

An example of use of a circularly linked list is the *round-robin scheduler*,
which iterates through a collection of elements in a circular fashion, and *services*
each element by performing a given action on it.
For example, this scheduler is used to allocate a resource that is shared by a number of clients
(such as slices of CPU time assigned to applications running on a computer).
A round-robin scheduler repeatedly performs the following steps on a queue Q::

 1 e = Q.dequeue()
 2 Service element e
 3 Q.enqueue(e)

Estos pasos se pueden realizar fácilmente utilizando la clase ``LinkedQueue`` introducida anteriormente;
Sin embargo, hay que tener en cuenta que tenemos que eliminar un nodo de la lista
(ajustando la cabeza de la lista y su tamaño),
crear un nuevo nodo, e insertarlo en la cola de la lista, aumentando el tamaño.
Esto puede evitarse con una lista enlazada circularmente, en la que la transferencia de un elemento desde
de la cabeza a la cola de la lista puede realizarse simplemente avanzando una referencia
que marca el límite de la cola.

En la siguiente sección, proporcionamos una implementación de una clase ``CircularQueue`` que soporta el ADT de la cola
que soporta el ADT de cola, con un nuevo método ``rotate( )`` que mueve el primer elemento de la cola
a la parte posterior. Un programa de rotación puede ser implementado realizando repetidamente los siguientes pasos::

 1. Service element Q.front()
 2. Q.rotate()

Sección 2.1 Implementación de una cola con una lista enlazada circularmente
==============================================================

Para implementar el ADT de cola con una lista enlazada circularmente, nos basamos en la idea anterior de que
la cola tiene una cabeza y una cola, con la siguiente referencia de la cola vinculada a la cabeza.
En este caso, las dos únicas variables de instancia son ``_tail``,
que es una referencia al nodo de cola (o None cuando está vacío),
y ``_size``, el número actual de elementos en la cola.
Podremos encontrar la cabeza de la cola simplemente siguiendo la siguiente referencia de la cola,
es decir, ``self._tail._next``.
Podemos poner en cola un nuevo nodo colocándolo después de la cola y antes de la cabeza actual, y
y convirtiéndolo en la nueva cola.
Para implementar la operación de quitar la parte delantera de la cola e insertarla
en la parte de atrás, añadimos el método ``rotate``, que básicamente establece ``self._tail = self._tail._next``
(la antigua cabeza se convierte en la nueva cola, y el nodo después de la antigua cabeza se convierte en la nueva cabeza).
La implementación completa se da en el siguiente código::

 1  class CircularQueue:
 2    # Implementación de colas mediante listas enlazadas circularmente
 3
 4    class _Node:
 6      __slots__ = '_element' , '_next'
 7      def __init__(self, element, next):
 8        self._element = element
 9        self._next = next
 7
 8    def __init__(self):
 9      self._tail = None
 10     self._size = 0
 11
 12   def __len__ (self):
 13     return self._size
 14
 15   def is_empty(self):
 16     return self._size == 0
 17
 18   def first(self):
 19     if self.is_empty( ):
 20       raise Empty('Queue is empty')
 21     head = self._tail._next
 22     return head._element
 23
 24   def dequeue(self):
 25     if self.is_empty( ):
 26       raise Empty('Queue is empty')
 27     oldhead = self._tail._next
 28     if self._size == 1:
 29       self._tail = None
 30     else:
 31       self._tail._next = oldhead._next
 32     self._size −= 1
 33     return oldhead._element
 34
 35   def enqueue(self, e):
 36     newnode = self._Node(e, None)
 37     if self.is_empty( ):
 38       newnode._next = newnode
 39     else:
 40       newnode._next = self._tail._next
 41       self._tail._next = newnode
 42     self._tail = newnode
 43     self._size += 1
 44
 45   def rotate(self):
 46     if self._size > 0:
 47       self._tail = self._tail._next


Sección 3. Listas doblemente enlazadas
------------------------------

Hay algunas limitaciones en el diseño de una lista de doble enlace.
Mientras que podemos insertar eficientemente un nodo en cualquiera de los extremos de dicha lista
y podemos borrar un nodo en la cabeza de la lista, no podemos borrar eficientemente un nodo
en la cola de la lista, o un nodo de cada posición interna.
Esto sucede porque necesitamos actualizar la referencia ``next`` del
del nodo que precede al nodo que se va a borrar.

Una *lista doblemente enlazada* es una lista en la que cada nodo mantiene una referencia a
el nodo que le sigue (es decir, ``next``), y al nodo que le precede (es decir, ``prev``).
Veremos que esto permite la inserción y eliminación sencilla en posiciones arbitrarias dentro de la lista.

**Centinelas de cabecera y de cola**

La implementación estándar de una lista doblemente enlazada implica dos nodos especiales
en ambos extremos de la lista, el *cabecera* y el *remolque*, respectivamente.
Actúan como centinelas y no almacenan ningún elemento.

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura58.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center

En una lista vacía, el campo ``next`` de la cabecera apunta al tráiler
y el campo ``prev`` del trailer apunta a la cabecera.
En una lista no vacía, el campo ``next`` de la cabecera se refiere al nodo que contiene el primer
elemento de la secuencia, y el campo ``prev`` del trailer se refiere al nodo que contiene el
último elemento de la secuencia.
La adición de los dos nodos adicionales simplifica las operaciones necesarias.
En particular, todas las inserciones se hacen de la misma manera, porque un nuevo nodo siempre se
entre un par de nodos existentes.
Del mismo modo, cada elemento que se elimine se guardará siempre entre dos vecinos, a cada lado.
No se requiere ningún caso especial cuando la lista está vacía.

**Inserción y borrado con una lista doblemente enlazada**

Cada inserción en una lista doblemente enlazada tendrá lugar entre un par de nodos existentes.
Por ejemplo, cuando se inserta un nuevo elemento al principio de la secuencia
el nuevo nodo va entre la cabecera y el nodo que está actualmente después de la cabecera.
En la siguiente figura mostramos lo que ocurre cuando añadimos un elemento a una lista doblemente enlazada
con centinelas de cabecera y de cola, antes de la operación, después de crear el nuevo nodo y
después de conectar los vecinos al nuevo nodo.

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura59.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center

La siguiente figura muestra cómo añadir un elemento al frente de una lista doblemente enlazada;
nótese que el procedimiento es el mismo de la inserción general.

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura510.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center

En cuanto a la eliminación de un nodo, los dos vecinos del nodo que se va a eliminar se vinculan directamente
entre sí, evitando el nodo original. Por lo tanto, ese nodo ya no es
no es más un miembro de la lista. La misma implementación se utiliza cuando se borra el primer
(o el último) elemento de una secuencia, porque incluso ese elemento se almacenará en un
nodo que se encuentra entre otros dos.

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura511.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center

Sección 3.1 Implementación básica de una lista doblemente enlazada
========================================================

A continuación, proporcionamos una implementación de una lista doblemente enlazada, concretamente la clase ``DoublyLinkedBase``.
Esta es una clase de bajo nivel, lo que significa que no proporcionamos una interfaz para ella.
Veremos que las inserciones y eliminaciones pueden realizarse en tiempo \\(O(1)\\), en el peor de los casos,
cuando somos capaces de identificar la ubicación de un nodo específico en la lista.

La clase ``Node`` que utilizamos aquí es similar a la de una lista unicatenaria,
pero incluye un atributo ``prev``, además de los atributos ``next`` y ``elemento``.
El constructor de la lista instanciará los dos nodos centinela y los enlazará directamente entre sí.
entre sí. Se mantiene un miembro ``size``::

 1  class _DoublyLinkedBase:
 2
 3    class Node:
 4      __slots__ = '_element' , '_prev' , '_next'
 5      def __init__(self, element, prev, next):
 6        self._element = element
 7        self._prev = prev                # referencia del nodo anterior
 8        self._next = next                # referencia del siguiente nodo
 9
 10   def __init__(self):
 11     self._header = self._Node(None, None, None)
 12     self._trailer = self._Node(None, None, None)
 13     self._header._next = self._trailer        # el remolque está después de la cabecera
 14     self._trailer._prev = self._header        # la cabecera está antes del remolque
 15     self._size = 0
 16
 17   def __len__(self):
 18     return self._size
 19
 20   def is_empty(self):
 21     return self._size == 0
 22
 23   def _insert_between(self, e, predecessor, successor):
 25     newnode = self._Node(e, predecessor, successor)
 26     predecessor._next = newnode
 27     successor._prev = newnode
 28     self._size += 1
 29     return newnode
 30
 32   def _delete_node(self, node):
 33     predecessor = node._prev
 34     successor = node._next
 35     predecessor._next = successor
 36     successor._prev = predecessor
 37     self._size −= 1
 38     element = node._element                             # registro de elemento borrado
 39     node._prev = node._next = node._element = None      # útil para la recogida de basura
 40     return element                                      # devuelve el elemento eliminado

Los métodos ``_insert_between`` y ``_delete_node`` están diseñados como utilidades no públicas;
soportan inserciones y eliminaciones, respectivamente, y requieren una o más referencias a nodos
como parámetros.
La primera crea un nuevo nodo, con los campos inicializados para conectar con los nodos vecinos especificados;
A continuación, los campos de los nodos vecinos se actualizan para incluir el nodo más nuevo de la lista.
El método devuelve una referencia al nuevo nodo.
Este último vincula entre sí a los vecinos del nodo que se va a eliminar, lo que significa que éste se omite de la lista.
Los campos ``prev``, ``next`` y ``element`` se establecen como None,
porque esto podría ayudar al recolector de basura de Python,


Sección 3.2 Implementación de un Deque con una lista doblemente enlazada
==========================================================

El ADT de cola doble (deque) puede ser implementado con un array,
y todas las operaciones funcionan en tiempo *amortizado* (\\(O(1)\\), debido a la necesidad de redimensionar el array.
La misma estructura de datos puede implementarse con una lista doblemente enlazada;
en este caso, todas las operaciones de deque funcionan en *peor caso* \\(O(1)\\) de tiempo.
La siguiente implementación de una clase ``LinkedDeque`` hereda de
la clase anterior ``_DoublyLinkedBase``;
nos apoyamos en los métodos heredados ``__init__`` (para inicializar una nueva instancia),
``__len__``, ``is_empty``, así como ``_insert_between`` (para insertar un elemento al principio o al final)
y ``_delete_node`` (para eliminar un elemento).
Como es de esperar, al insertar un elemento en la parte delantera del deque, lo colocamos inmediatamente
entre la cabecera y el nodo justo después de la cabecera; una inserción al final del
el deque se coloca inmediatamente antes del nodo de remolque.
Estas operaciones tienen éxito incluso cuando el deque está vacío;
en este caso, el nuevo nodo se coloca entre los dos centinelas::

 1  class LinkedDeque(_DoublyLinkedBase):
 2
 3    def first(self):
 4      if self.is_empty( ):
 5        raise Empty("El deque está vacío")
 6      return self._header._next._element
 7
 8    def last(self):
 9      if self.is_empty( ):
 10       raise Empty("El deque está vacío")
 11    return self._trailer._prev._element
 12
 13   def insert_first(self, e):
 14       self._insert_between(e, self._header, self._header._next)
 15
 16   def insert_last(self, e):
 17     self._insert_between(e, self._trailer._prev, self._trailer)
 18
 19   def delete_first(self):
 20     if self.is_empty( ):
 21      raise Empty("El deque está vacío")
 22     return self._delete_node(self._header._next)
 23
 24   def delete_last(self):
 25     if self.is_empty( ):
 26       raise Empty("El deque está vacío")
 27     return self._delete_node(self._trailer._prev)


Sección 4. Listas de posiciones
---------------------------

Las pilas, las colas y las colas dobles permiten realizar operaciones al principio o al final
de una secuencia (o ambas, en el último caso).
En esta sección, introducimos un tipo de datos abstracto que proporciona
una forma de referirse a elementos en cualquier lugar de una secuencia, y de realizar inserciones y eliminaciones arbitrarias;
Este tipo se denomina *lista posicional ADT*.

Una forma directa de implementar una estructura de este tipo es utilizar una secuencia basada en un array
en la que los índices enteros indican la ubicación de un elemento, o la
ubicación en la que debe producirse una inserción o una eliminación.
Sin embargo, no se puede encontrar eficientemente un elemento de una lista enlazada conociendo sólo su índice
porque esta operación requiere recorrer la lista, empezando por el principio o el final, y contar
los elementos. Además, el índice de una entrada puede cambiar, debido a las inserciones
o eliminaciones que se produzcan en la secuencia.

Necesitamos una abstracción en la que haya alguna otra forma de describir una posición
y para manejar situaciones como la eliminación o la inserción de un elemento desde / dentro de la lista en \\(O(1)\\) tiempo.
Esta abstracción podría basarse en una lista enlazada, ya que la referencia del nodo podría utilizarse
para describir la posición de cada elemento en la secuencia; la inserción y la eliminación aleatorias ya se
aleatorias ya se implementan con métodos como ``_insert_between`` y ``_delete_node``, que aceptan
referencias a nodos como parámetros.
Aunque esta es una solución atractiva, hay algunas razones para no utilizarla.
En primer lugar, un uso tan directo de la referencia de un nodo viola los principios de abstracción y
principios de encapsulación, porque permite a los usuarios manipular los nodos directamente,
invalidando potencialmente la consistencia de la lista.
Entonces, es preferible diseñar una estructura de datos más flexible, utilizable y robusta
en la que los detalles de bajo nivel no sean accesibles para el usuario, y que pueda ser fácilmente rediseñada
para mejorar su rendimiento.
De este modo, también podemos proporcionar una noción de posición no numérica, aunque se utilice una secuencia basada en arrays.
Por estas razones, en lugar de basarnos directamente en los nodos, introducimos una abstracción de posición independiente
abstracción de posición para denotar la ubicación de un elemento dentro de una lista, y
una lista posicional completa ADT que puede encapsular una lista doblemente enlazada.


Sección 4.1 El tipo de datos abstracto de la lista posicional
==================================================

Para definir una abstracción de una secuencia de elementos, junto con una forma de
identificar la ubicación de un elemento, introducimos una *lista posicional ADT* y un tipo de datos abstracto *posición*.
Una posición es un marcador dentro de la lista, y no debe verse afectada por los cambios en la lista;
una posición sólo deja de ser válida si se emite una orden explícita para borrarla.
El método ``p.element( )`` devuelve el elemento almacenado en la posición ``p``.

Los métodos soportados por una lista **L** son:

- **L.first( )**: devuelve la posición del primer elemento de **L**, o None si **L** está vacío;
- **L.last( )**: devuelve la posición del último elemento de **L**, o None si **L** está vacío;
- **L.before(p)**: devuelve la posición de **L** inmediatamente anterior a la posición **p**, o Ninguna si **p** es la primera posición;
- **L.after(p)**: devuelve la posición de **L** inmediatamente después de la posición **p**, o None si **p** es la última posición;
- **L.is_empty( )**: devuelve True si la lista **L** no contiene ningún elemento;
- **len(L)**: devuelve el número de elementos de la lista;
- **iter(L)**: devuelve un iterador hacia adelante para los elementos de la lista.

El ADT de la lista posicional también incluye los siguientes métodos de actualización:

- **L.add_first(e)**: inserta un nuevo elemento **e** al frente de **L**, devolviendo su posición;
- **L.add_last(e)**: inserta un nuevo elemento **e** en la parte posterior de **L**, devolviendo su posición;
- **L.add_before(p, e)**: inserta un nuevo elemento **e** antes de la posición **p** en **L**, devolviendo su posición;
- **L.add_after(p, e)**: inserta un nuevo elemento **e** después de la posición **p** en L, devolviendo su posición;
- **L.replace(p, e)**: sustituye el elemento en la posición **p** por el elemento **e**, devolviendo el elemento que estaba en la posición **p**.
- **L.delete(p)**: elimina y devuelve el elemento en la posición **p** en **L**, invalidando la posición.

Se produce un error si ``p`` no es una posición válida para la lista.
Nótese que los métodos ``first( )`` y ``last( )`` devuelven las posiciones asociadas;
el primer elemento de una lista posicional puede determinarse invocando ``L.first().element()``.
Esto se utiliza para navegar por la lista.
Por ejemplo, el siguiente código imprime todos los elementos de una lista posicional ``poslis``::

 marker = poslis.first( )
 while marker is not None:
   print(marker.element( ))
   marker = poslis.after(marker)

Se devuelve None cuando se llama a ``after`` en la última posición.
Del mismo modo, se devuelve None cuando se invoca el método ``before`` al principio de la lista, o
cuando se llama a ``first`` o ``last`` en una lista vacía.
La siguiente tabla muestra una serie de operaciones sobre una lista posicional L.
Las variables p, q y r se utilizan para identificar las instancias de posición.

==================== ===================== ==============
Operaciones 		 Valor devuelto 	   L
==================== ===================== ==============
L.add_last(3)        p                     3p
L.first( )           p                     3p
L.add_after(p,5)     q                     3p, 5q
L.before(q)          p                     3p, 5q
L.add_before(q,3)    r                     3p, 3r, 5q
r.element( )         3                     3p, 3r, 5q
L.after(p)           r                     3p, 3r, 5q
L.add first(7)       s                     7s, 3p, 3r, 5q
L.delete(L.last( ))  5                     7s, 3p, 3r
L.replace(p,10)      3                     7s, 10p, 3r
==================== ===================== ==============



Sección 4.2 Implementación de la lista doblemente enlazada
=============================================

En esta sección, proporcionamos una implementación de una clase ``PositionalList`` utilizando una lista doblemente enlazada.
utilizando una lista doblemente enlazada; cada método de la clase se ejecuta en el peor de los casos \\(O(1)\\)
en el peor de los casos. Para ello, utilizamos la ``DoublyLinkedBase`` introducida anteriormente,
y construimos una interfaz pública de acuerdo con los requisitos de la lista posicional ADT.

La clase pública ``Position`` está anidada dentro de la clase ``PositionalList``, y sus
instancias representan las ubicaciones de los elementos dentro de la lista.
Los métodos de ``PositionalList`` pueden crear instancias de ``Position`` que sean redundantes,
es decir, que hacen referencia al mismo nodo (por ejemplo, cuando el primer y el último nodo son el mismo).
Por ello, se definen los métodos ``__eq__`` y ``__ne__`` para comprobar si dos posiciones
se refieren al mismo nodo.

El método no público ``_validate`` comprueba si una posición es válida, y determina el nodo subyacente.
Ten en cuenta que una posición mantiene una referencia a un nodo de la lista enlazada
y también una referencia a la instancia de la lista que contiene ese nodo.
Esto implica que somos capaces de detectar cuando una instancia de posición no pertenece a la lista indicada,
y cuando una instancia de posición es válida, pero se refiere a un nodo que ya no forma parte de esa lista
(esto se puede hacer fácilmente, porque el ``_delete_node`` de la clase base
establece las referencias anterior y siguiente de un nodo eliminado a None).
Para acceder a la lista posicional definimos métodos que utilizan ``_validate``.
para "desenvolver" cualquier posición, y ``_make_position`` para "envolver" nodos como instancias de ``Position``.
para devolver al usuario::

 1  class PositionalList(_DoublyLinkedBase):
 2    # Un contenedor secuencial de elementos con acceso posicional
 3
 4    class Position:
 5      # La ubicación de un solo elemento
 6      def __init__ (self, container, node):
 7        self._container = container
 8        self._node = node
 9
 10     def element(self):
 11       return self._node._element
 12
 13     def __eq__ (self, other):
 14       # Devuelve True si otra es una posición que representa el mismo lugar
 15       return type(other) is type(self) and other._node is self._node
 16
 17     def __ne__ (self, other):
 18       # Devuelve True si otro no representa la misma ubicación
 19       return not (self == other)
 20
 21   #------------------------------- utility methods -------------------------------
 22
 23   def _validate(self, p):
 24     # Devuelve el nodo en la posición p, o genera un error si no es válido
 25     if not isinstance(p, self.Position):
 26       raise TypeError('p must be proper Position type')
 27     if p._container is not self:
 28       raise ValueError('p does not belong to this container')
 29     if p._node._next is None:                  # nodo inválido
 30       raise ValueError('p is no longer valid')
 31     return p._node
 32
 33   def _make_position(self, node):
 34     # Devuelve la instancia de la Posición para el nodo dado (Ninguno si es centinela)
 35     if node is self._header or node is self._trailer:
 36       return None
 37     else:
 38       return self.Position(self, node)
 39
 40   #------------------------------- accessors -------------------------------
 41
 42   def first(self):
 43     # Devuelve la primera posición de la lista
 44     return self._make_position(self._header._next)
 45
 46   def last(self):
 47     # Devuelve la última posición de la lista
 48     return self._make_position(self._trailer._prev)
 49
 50   def before(self, p):
 51     # Devuelve la Posición antes de la Posición p
 52     node = self._validate(p)
 53     return self._make_position(node._prev)
 54
 55   def after(self, p):
 56     # Devuelve la Posición después de la Posición p
 57     node = self._validate(p)
 58     return self._make_position(node._next)
 59
 60   def __iter__(self):
 61     # Generar una iteración hacia adelante de los elementos de la lista
 62     cursor = self.first( )
 63     while cursor is not None:
 64       yield cursor.element( )
 65       cursor = self.after(cursor)

En el siguiente código introducimos los métodos de actualización::

 66   # versión heredada anulada, devuelve Position, en lugar de Node
 67   def _insert_between(self, e, predecessor, successor):
 68     # Añade un elemento entre los nodos existentes y devuelve la nueva posición
 69     node = super()._insert_between(e, predecessor, successor)
 70     return self._make_position(node)
 71
 72   def add_first(self, e):
 73     # Inserta el elemento e al principio de la lista y devuelve Posición
 74     return self._insert_between(e, self._header, self._header._next)
 75
 76   def add_last(self, e):
 77     # Inserta el elemento e al final de la lista y devuelve Posición
 78     return self._insert_between(e, self._trailer._prev, self._trailer)
 79
 80   def add_before(self, p, e):
 81     # Inserta el elemento e en la lista antes de la Posición p y devuelve la Posición
 82     original = self._validate(p)
 83     return self._insert_between(e, original._prev, original)
 84
 85   def add_after(self, p, e):
 86     # Inserta el elemento e en la lista después de la Posición p y devuelve la Posición
 87     original = self._validate(p)
 88     return self._insert_between(e, original, original._next)
 89
 90   def delete(self, p):
 91     # Elimina y devuelve el elemento en la Posición p
 92     original = self._validate(p)
 93     return self._delete_node(original)
 94
 95   def replace(self, p, e):
 96     # Replace the element at Position p with e
 97     original = self._validate(p)
 98     old_value = original._element           # almacenar temporalmente el elemento antiguo
 99     original._element = e                   # sustituir por un nuevo elemento
 100    return old_value                        # devolver el valor del elemento antiguo




Sección 5. Clasificación de una lista posicional
------------------------------------

En esta sección, introducimos una implementación del algoritmo de *ordenación por inserción
que opera sobre una lista posicional. Mantenemos la variable ``marker``
que representa la posición más a la derecha de la parte de la lista actualmente ordenada.
El pivote es la posición inmediatamente posterior al marcador, y utilizamos la variable
``move_left`` para moverse hacia la izquierda desde el marcador, siempre que haya un elemento precedente
con valor mayor que el del pivote. En la siguiente figura mostramos la situación de una lista
durante el proceso.

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura512.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center

A continuación mostramos el código para la ordenación por inserción::

 1  def insertion_sort(L):
 2
 3    if len(L) > 1:
 4      marker = L.first( )
 5      while marker != L.last( ):
 6        pivot = L.after(marker)
 7        value = pivot.element( )
 8        if value > marker.element( ):
 9          marker = pivot                        # el pivote se convierte en un nuevo marcador
 10       else:
 11         move_left = marker                    # encontrar el elemento más a la izquierda mayor que el valor
 12         while move_left != L.first( ) and L.before(move_left).element( ) > value:
 13           move_left = L.before(move_left)
 14         L.delete(pivot)
 15         L.add_before(move_left, value)        # insertar valor antes de move_left




Sección 6. Secuencias basadas en matrices y enlaces 
-----------------------------------------------

Las estructuras de datos basadas en matrices y en enlaces tienen pros y contras.
En esta sección, discutiremos algunas de ellas.

Sección 6.1 Ventajas de las secuencias basadas en arrays
===============================================

En una secuencia basada en arrays, podemos acceder a un elemento genérico en un tiempo \\(O(1)\\);
en cambio, localizar el \\(k\\)-ésimo elemento de una lista enlazada requiere
\\(O(k)\\) de tiempo (es decir, complejidad de *tiempo lineal*), para recorrer la lista.

Las representaciones basadas en matrices utilizan menos memoria que las estructuras enlazadas.
Aunque la longitud de un array dinámico puede ser mayor que el número de elementos
que almacena, observamos que tanto las listas basadas en matrices como las listas enlazadas son estructuras referenciales,
por lo que la memoria necesaria para almacenar los elementos reales es la misma para cualquiera de las dos estructuras.
Por otra parte, para un contenedor basado en arrays de \\(n\\) elementos, el peor caso típico ocurre cuando un
El peor caso se produce cuando una matriz dinámica redimensionada asigna memoria para 2 referencias a objetos.
En el caso de las listas enlazadas, la memoria debe dedicarse no sólo a almacenar una referencia a cada objeto contenido
sino también para las referencias que enlazan los nodos.
Así, una lista enlazada individualmente de longitud \\(n\\) siempre requiere \\(2n\\) referencias
(la referencia de un elemento y la siguiente referencia de cada nodo);
con una lista doblemente enlazada, hay 3 referencias.

Las operaciones con límites asintóticos equivalentes suelen ser más eficientes de un factor constante
con la secuencia basada en arrays, con respecto a las mismas operaciones realizadas por una estructura enlazada.
Esto sucede porque se necesita instanciar un nuevo nodo cuando se realizan las versiones enlazadas de algunas
de algunas operaciones enlazadas, como la típica operación enqueue de una cola.


Sección 6.2 Ventajas de las secuencias basadas en enlaces
=================================================

Si algunas operaciones sobre una determinada estructura de datos se utilizan en un sistema de tiempo real
que está diseñado para proporcionar respuestas inmediatas, porque un gran retraso causado por una sola
(amortizada) puede tener un efecto adverso.
En este caso, observamos que los límites amortizados están asociados a la expansión o
de una matriz dinámica, mientras que las estructuras basadas en enlaces proporcionan
mientras que las estructuras basadas en enlaces proporcionan directamente límites de tiempo en el peor de los casos para sus operaciones.

Las estructuras basadas en enlaces admiten inserciones y eliminaciones en tiempo \\(O(1)\\) en posiciones arbitrarias.
Esta es la ventaja más importante de la lista enlazada, en contraste con la secuencia basada en arrays.
Insertar o borrar un elemento del final de una lista basada en arrays puede hacerse en tiempo constante;
pero las inserciones y eliminaciones más generales son caras.
Una llamada para insertar o borrar con índice \\(k\\), con la clase de lista basada en array de Python,
utiliza un tiempo de \\(O(n−k+1)\\) debido a que debemos desplazar todos los elementos subsiguientes.
