===================================
Algoritmos
===================================

.. toctree::
   :maxdepth: 1

   capítulo 1 - analysis.rst
   capítulo 1 - o2 exercises.rst
   capítulo 2 - recursion.rst
   capítulo 2 - o2 exercises.rst
   capítulo 3 - array based sequences.rst
   capítulo 3 - o2 exercises.rst
   capítulo 4 - sqd.rst
   capítulo 4 - o2 exercises.rst
   capítulo 5 - linkedlists.rst
   capítulo 5 - o2 exercises.rst
   capítulo 6 - sorting.rst
   capítulo 6 - o2 exercises.rst
