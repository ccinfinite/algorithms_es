
Capítulo 6: Clasificación
********************

Sección 1. Introducción
-----------------------

Dada una colección de elementos, y suponiendo que existe (en el lenguaje) un orden sobre ellos,
*clasificar* significa reordenarlos para que se ordenen de menor a mayor.
En Python, el orden por defecto de los objetos se define utilizando el operador \\( < \\), con la propiedad de ser
irreflexivo (\\(k\\) no es menor que él mismo) y transitivo
(si \\(k_1 < k_2\\) y \\(k_2 < k_3\\), entonces \\(k_1 < k_3\\) ).
Además, Python tiene funciones incorporadas para ordenar los datos, en forma del método ``sort`` de la clase
de la clase ``list`` (que reordena el contenido de una lista), y la función incorporada ``sorted`` que produce una nueva lista que contiene los elementos de la lista.
que produce una nueva lista que contiene los elementos de una colección arbitraria en orden
ordenada. Estas funciones incorporadas utilizan algoritmos avanzados y están muy optimizadas.

Un programador suele utilizar las funciones de ordenación incorporadas, ya que no es habitual tener una petición tan especial que le obligue a implementar una nueva función de ordenación.
que le obligue a implementar un algoritmo de ordenación nuevo y diferente.
Sin embargo, es importante entender cómo se construyen los algoritmos de ordenación, para
evaluar su rendimiento.

La ordenación es uno de los problemas más estudiados en informática;
Los conjuntos de datos se almacenan a menudo ordenados, por ejemplo, para permitir búsquedas eficientes
con el algoritmo de búsqueda binaria. Muchos algoritmos avanzados para diversos problemas se basan en la ordenación como subrutina.


Sección 2. Algoritmos de ordenación basados en matrices
-----------------------------------------
Sección 2.1 Ordenación de burbujas
=======================

La *ordenación por burbujas* realiza una tarea muy sencilla: hace múltiples pasadas por una lista
comparando parejas de valores adyacentes, e intercambiándolos si no están en el orden correcto.
En cada pasada, el valor más grande se burbujea hasta su ubicación correcta.

El código Python de la ordenación por burbujas se da a continuación::

 1  def bubbleSort(alist):
 2    for passnum in range(len(alist)-1,0,-1):
 3      for i in range(passnum):
 4        if alist[i] > alist[i+1]:
 5          temp = alist[i]
 6          alist[i] = alist[i+1]
 7          alist[i+1] = temp

Obsérvese que si la lista o array tiene \\(n\\) elementos, habrá \\(n-1\\) comparaciones durante la primera pasada,
\\(n-2\\) durante la segunda pasada, y así sucesivamente, hasta que se necesite una comparación en la última \\(n-1\\)-ésima pasada,
con el valor más pequeño en su posición correcta.
El número total de comparaciones es la suma de los primeros números enteros \\(n-1\\), es decir, \\((n-1)n/2\\).
Esto significa que el algoritmo tiene una complejidad de \\(O(n^2)\\).
En el mejor de los casos la lista ya está ordenada, y no habrá intercambios.
Pero, en el peor de los casos, cada comparación provocará un intercambio, lo que convierte a la ordenación por burbujas en un método de ordenación muy ineficiente.
Observe que si durante una pasada no hay intercambios, la lista ya está ordenada.

La siguiente figura muestra la primera pasada de una ordenación por burbujas.
Los elementos sombreados se comparan y se intercambian, si es necesario.

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura61.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center

Podemos escribir una versión ligeramente modificada del algoritmo, el *short bubble sort*,
que se detiene antes si encuentra que la lista ha sido ordenada, ya::

 1  def shortbubbleSort(alist):
 2    exchanges = True
 3    passnum = len(alist)-1
 4    while passnum > 0 and exchanges:
 5      exchanges = False
 6      for i in range(passnum):
 7        if alist[i] > alist[i+1]:
 8          exchanges = True
 9          temp = alist[i]
 10         alist[i] = alist[i+1]
 11         alist[i+1] = temp
 12     passnum = passnum-1


Sección 2.2 Ordenación de la selección
==========================

La *ordenación por selección* realiza múltiples pasadas por una lista de longitud \\(n\\) de
buscando el valor más grande y moviéndolo a su ubicación correcta;
Esto significa que sólo hay un intercambio por cada pase.
Después de la primera pasada, el elemento más grande está en el lugar correcto.
Después de la segunda pasada, el siguiente más grande está en su lugar.
El proceso continúa hasta la última pasada \\(n-1\\), cuando el último elemento está en su lugar.

El código Python de la ordenación por selección es el siguiente::

 1  def selectionSort(alist):
 2    for fillslot in range(len(alist)-1,0,-1):
 3      positionOfMax=0
 4      for location in range(1,fillslot+1):
 5        if alist[location] > alist[positionOfMax]:
 6           positionOfMax = location
 7      temp = alist[fillslot]
 9      alist[fillslot] = alist[positionOfMax]
 10     alist[positionOfMax] = temp

Incluso si sólo hay un intercambio para cada pase, la ordenación por selección hace el mismo número de comparaciones que
que la ordenación por burbujas; por lo tanto, su complejidad es \\(O(n^2)\\).

La siguiente figura muestra el proceso de ordenación.
En cada pasada, se selecciona el elemento restante más grande y se coloca en su lugar correspondiente.

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura62.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center

Sección 2.3 Ordenación de la inserción
==========================

El *insertion sort* crea una sublista ordenada (inicialmente, un solo elemento) en las posiciones iniciales de la lista
y inserta adecuadamente en esta sublista un nuevo elemento, produciendo una nueva sublista ordenada.
Partiendo de una lista con un elemento (el primer elemento de la lista), cada elemento desde el segundo hasta el último
se compara con los de la sublista ya ordenada;
los elementos que son mayores se desplazan hacia la derecha, y el elemento actual se inserta en la sublista
cuando se alcanza un elemento menor.

El código de Python para la ordenación por inserción se escribe a continuación::

 1  def insertionSort(alist):
 2    for index in range(1,len(alist)):
 3      currentvalue = alist[index]
 5      position = index
 6      while position > 0 and alist[position-1] > currentvalue:
 8        alist[position]=alist[position-1]
 9        position = position-1
 10     alist[position]=currentvalue

Para ordenar \\(n\\) elementos, hay \\(n-1\\) pases.
El número máximo de comparaciones para una ordenación por inserción es, de nuevo, la suma de los primeros números enteros \\(n-1\\).
Es decir, \\(O(n^2)\\).
En el mejor de los casos, sólo hay que hacer una comparación en cada pasada. Este sería el caso de una lista ya ordenada.

La siguiente figura muestra el proceso de ordenación por inserción.
Los elementos sombreados representan las sublistas ordenadas en cada pasada.

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura63.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center


Sección 2.4 Ordenación de la fusión
======================

El *merge sort* es un algoritmo recursivo.
Si la lista está vacía o tiene un elemento, se ordena (el caso base).
Si la lista tiene más de un elemento, la lista se divide y la ordenación por fusión se invoca recursivamente en ambas mitades.
Una vez que las dos mitades están ordenadas, se realiza una fusión, tomando dos listas ordenadas más pequeñas y combinándolas
en una única lista ordenada.

Este algoritmo es un ejemplo clásico de estrategia de *divide y vencerás*.
En el paso *dividir*, cuando el tamaño de la entrada es menor que un umbral dado, la solución puede encontrarse directamente.
directamente; en caso contrario, la entrada se divide en subconjuntos disjuntos.
En el paso *conquistar*, se resuelven los subproblemas asociados a los subconjuntos, de forma recursiva.
En el paso *combinar*, la solución de los subproblemas se fusiona para obtener una solución al problema original.

El algoritmo general para Mergesort se da a continuación, sin tener en cuenta si la secuencia \\(S\\) se implementa
con un array o una lista enlazada:

- Dividir: si \\(S\\) tiene cero o un elemento, devuelve \\(S\\);
  en caso contrario, divide \\(S\\) por la mitad y coloca los elementos en dos secuencias, \\(S_1\\) y \\(S_2\\).
- Conquistar: ordenar recursivamente las secuencias \\(S_1\\) y \\(S_2\\).
- Combinar: volver a poner los elementos en \\(S\\) fusionando las secuencias ordenadas \\(S_1\\) y \\(S_2\\) en una secuencia ordenada.

El código de Python para una secuencia basada en un array es el siguiente::

 1  def mergeSort(alist):
 2    print("Dividiendo ",alist)
 3    if len(alist) > 1:
 4      mid = len(alist)//2
 5      lefthalf = alist[:mid]
 6      righthalf = alist[mid:]
 7
 8      mergeSort(lefthalf)
 9      mergeSort(righthalf)
 10
 11     i=0
 12     j=0
 13     k=0
 14     while i < len(lefthalf) and j < len(righthalf):
 15       if lefthalf[i] <= righthalf[j]:
 16         alist[k] = lefthalf[i]
 17         i=i+1
 18       else:
 19         alist[k] = righthalf[j]
 20         j=j+1
 21       k=k+1
 22
 23     while i < len(lefthalf):
 24       alist[k]=lefthalf[i]
 25       i=i+1
 26       k=k+1
 27
 28     while j < len(righthalf):
 29       alist[k]=righthalf[j]
 30       j=j+1
 31       k=k+1
 32   print("Juntando ",alist)

La llamada recursiva de la función ``mergesort`` se realiza sobre la mitad izquierda y la mitad derecha de la lista,
asumiendo que han sido ordenadas.
El resto de la función fusiona las dos listas ordenadas en una lista ordenada más grande.
Esto se hace colocando los elementos en la lista original ``alist`` de uno en uno,
tomando el elemento más pequeño de las listas ordenadas.

Las siguientes figuras muestran los procesos de división y fusión, respectivamente.

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura64.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura65.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center

Para analizar la complejidad de este algoritmo, consideremos que la lista se divide recursivamente en dos mitades:
esto puede repetirse durante \\(\\log n\\) veces, si la lista tiene longitud \\(n\\).
Cada par de sublistas se fusiona en una sola lista, y esto requiere \Noperaciones \\(n\\),
ya que hay que procesar cada elemento de las sublistas.
Por lo tanto, el coste global del algoritmo es \\(O(n \\log n)\\).
Nótese que Mergesort requiere espacio para almacenar las sublistas extraídas con las operaciones de corte.
Este requisito adicional puede hacer que este algoritmo sea crítico cuando se aplica a grandes conjuntos de datos.


Sección 2.5 Quicksort
=====================

El *quick sort* utiliza el enfoque de divide y vencerás y, con respecto al mergesort, no utiliza espacio adicional
para almacenar las sublistas extraídas con las operaciones de corte.
Como contrapartida, puede ocurrir que las dos sublistas no tengan la misma longitud, lo que hace que el algoritmo sea más lento.
Quicksort comienza seleccionando un valor, el *pivote*, que es el primer elemento de la lista, normalmente;
se encuentra su posición correcta en la lista, y esta posición (el *punto de división*) se utiliza en el proceso
de dividir la lista en dos sublistas. La ordenación rápida se aplicará recursivamente en estas sublistas.

Más concretamente, una vez seleccionado el pivote, se definen dos marcas (*marca izquierda* y *marca derecha*)
como la primera y la última posición de los elementos restantes de la lista, respectivamente.
Leftmark se incrementa hasta que se encuentra un valor mayor que el pivote;
entonces, rightmark se decrementa hasta que se encuentra un valor menor que el pivote.
Los dos valores están fuera de lugar con respecto al punto de división, y se intercambian.

Este procedimiento continúa hasta que el valor de la marca derecha sea menor que el de la marca izquierda; ahora se ha encontrado el punto de división.
El valor del pivote se intercambia con el contenido del punto de división, lo que significa que el pivote está en su posición correcta.
En la siguiente figura se encuentra el punto de división correcto para 54 y se intercambian los dos valores.

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura66.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center

Observe que los elementos a la izquierda del punto de división son menores que el valor del pivote,
y los elementos a la derecha del punto de división son mayores que el valor del pivote.
La lista se divide en el punto de división y el Quicksort se invoca recursivamente en las dos sublistas.

La función quickSort se define como sigue::

 1  def quickSort(alist,first,last):
 2    if first < last:
 3      splitpoint = partition(alist,first,last)
 4      quickSort(alist,first,splitpoint-1)
 5      quickSort(alist,splitpoint+1,last)
 6
 7  def partition(alist,first,last):
 8    pivotvalue = alist[first]
 9    leftmark = first+1
 10   rightmark = last
 11
 12   done = False
 13   while not done:
 14     while leftmark <= rightmark and alist[leftmark] <= pivotvalue:
 15       leftmark = leftmark + 1
 16     while leftmark <= rightmark and alist[rightmark] >= pivotvalue:
 17       rightmark = rightmark -1
 18
 19     if rightmark < leftmark:
 29       done = True
 20     else:
 21       temp = alist[leftmark]
 22       alist[leftmark] = alist[rightmark]
 23       alist[rightmark] = temp
 24
 25   temp = alist[first]
 26   alist[first] = alist[rightmark]
 27   alist[rightmark] = temp
 28   return rightmark

Si la partición se produce siempre en la mitad de la lista, habrá de nuevo \\(\\log n\\) particiones,
para una lista de longitud \\(n\\).
Para encontrar el punto de división, cada uno de los elementos de \\(n\\) tiene que ser comparado con el valor del pivote.
Esto implica que los pasos globales son \\(O(n \\log n)\\) de los que hay que ocuparse.) Además, no se utiliza memoria adicional.

Los puntos de partición no siempre se encuentran en el centro de la lista;
en el peor de los casos, la función de partición divide una lista de \\(n\\) elementos en una lista de \\(1\\) elemento
y una lista de \\(n-1\\) elementos. A continuación, la lista de \\(n-1\\) elementos se divide en una lista de \\(1\\) elementos
y una lista de \\(n-2\\) elementos, respectivamente. Esto conduce a un número de pasos de \\(O(n^2)\\).



Sección 3. Ordenación en tiempo lineal
------------------------------

Se ha demostrado que se necesita un tiempo de \\(O(n \\log n)\\) para ordenar una secuencia de \\(n\\) elementos con un algoritmo de ordenación basado en la comparación.)
elementos con un algoritmo de ordenación basado en la comparación.
Una pregunta natural es si existen algoritmos de ordenación más rápidos;
Estos algoritmos existen, pero requieren restricciones especiales sobre la secuencia a ordenar.

Sección 3.1 Ordenación en cubos
=======================

Dada una secuencia \\(S\\) de \\(n\\) elementos, cuyas claves son números enteros en el rango \\([0,N -1]\\) de,
para \\(N \\geq 2\\), supongamos que \\(S\\)  tiene que ser ordenado según las claves de las entradas.
En este caso, es posible escribir un algoritmo para ordenar \\(S\\) dentro de \\(O(n+N)\\) de tiempo.
Por lo tanto, podemos lograr una complejidad de tiempo lineal para un algoritmo de ordenación; esto depende de la
restricción que se haga sobre los elementos.

La ordenación por cubos no se basa en comparaciones, sino en el uso de claves como índices en un *array de cubos* \\(B\\)
que tiene celdas indexadas desde \\(0\\) hasta \\(N-1\\). Una entrada con clave \\(k\\) se coloca en el cubo  \\(B[k]\\),
que a su vez es una secuencia de entradas con clave \\(k\\).
Después de que cada entrada de la secuencia de entrada \\(S\\) se ha colocado en su cubo, que se ponen de nuevo en \\(S\\) en orden,
simplemente enumerando el contenido de los cubos \\(B[0], B[1], \\ldots, B[N -1]\\).
El pseudocódigo para la ordenación de cubos es el siguiente::

 1  BucketSort(S):
 2  Input: secuencia S de entradas con claves enteras en el rango [0,N-1]
 3  Output: secuencia S ordenada en orden creciente de las claves
 4 	B es una matriz de N secuencias, inicialmente vacía
 5
 6  for each entry e in S do
 7    k = the key of e
 8    eliminar e de S e insertarlo al final del cubo B[k]
 9  for i = 0 to N-1 do
 10   for each entry e in sequence B[i] do
 11     eliminar e de B[i] e insertarlo al final de S

La ordenación por cubos se ejecuta en tiempo \\(O(n+N)\\) y utiliza la misma cantidad de espacio;
Esto significa que es eficiente sólo cuando el rango \\(N\\) de los valores de las claves es pequeño en comparación con
con el tamaño de la secuencia \\(n\\), digamos \\(N = O(n)\\) o \\(N = O(n \\log n)\\).
Una propiedad importante del algoritmo de ordenación por cubos es que funciona correctamente
incluso si hay diferentes elementos con la misma clave.
En particular, muestra la propiedad de ser estable.
Sea \\(S = ((k_0,v_0), \\ldots , (k_{n-1},v_{n-1}))\\) una secuencia de entradas;
un algoritmo de ordenación es *estable* si, para cualquier pareja \\((k_i,v_i)\\) y \\((k_j ,v_j)\\) de
\\(S\\) tal que \\(k_i = k_j\\), y \\((k_i,v_i)\\) precede a \\((k_j ,v_j)\\) en \\(S\\) antes de su ordenación,
tenemos que \\((k_i,v_i)\\) precede a \\((k_j ,v_j)\\) después de la ordenación.
La estabilidad es importante para un algoritmo de ordenación porque las aplicaciones pueden querer preservar
el orden inicial de los elementos con la misma clave.

La descripción informal de la ordenación por cubos dada anteriormente garantiza la estabilidad
siempre que nos aseguremos de que todas las secuencias actúan como colas, con elementos procesados
y eliminados de la parte delantera de una secuencia e insertados en la parte trasera.


Sección 3.2 Ordenación de radicales
======================

Consideremos el caso general en el que queremos ordenar las entradas con claves que son pares \\( (k, l)\\),
donde \\(k\\) y \\(l\\) son números enteros en el rango \\([0,N-1]\\) para \\(N \\geq 2\\).
Se puede definir un orden en estas claves utilizando la convención lexicográfica
donde \\((k_1, l_1) < (k_2, l_2)\\) si \\(k_1 < k_2\\) o si \\(k_1 =k_2\\) y \\(l_1 < l_2\\).

El algoritmo de ordenación radix ordena una secuencia \\(S\\) de entradas con claves que son pares,
aplicando una ordenación de cubo estable en la secuencia dos veces; primero, utilizando un componente del par como la clave
a la hora de ordenar, y luego utilizando el segundo componente.
Surge un problema: ¿qué orden es el correcto?
Es decir, ¿debemos ordenar sobre el primer componente y luego sobre el segundo, o viceversa?

Por ejemplo, consideremos la secuencia
\\(S = ((3,3), (1,5), (2,5), (1,2), (2,3), (1,7), (3,2), (2,2))\\).
Si ordenamos \\(S\\) en la primera componente, obtenemos la secuencia
\\(S_1 = ((1,5), (1,2), (1,7), (2,5), (2,3), (2,2), (3,3), (3,2))\\).
Si a continuación ordenamos \\(S_1\\) utilizando la segunda componente, obtenemos la secuencia
\\(S_{1,2} = ((1,2), (2,2), (3,2), (2,3), (3,3), (1,5), (2,5), (1,7))\\),
que claramente no es una secuencia ordenada. Por otro lado, repitiendo el proceso en la
segunda y primera componente, respectivamente, obtenemos la secuencia final
\\(S_{2,1} = ((1,2), (1,5), (1,7), (2,2), (2,3), (2,5), (3,2), (3,3))\\),
que está ordenado lexicográficamente.

Este resultado puede extenderse al caso general; ordenando la secuencia por la segunda componente y luego de nuevo por
la primera componente, garantizamos que si dos entradas son iguales en la segunda ordenación (por la primera componente)
se mantiene su orden relativo en la secuencia inicial (ordenada por el segundo componente).

En general, dada una secuencia \\(S\\) de \\(n\\) pares clave-valor, cada uno de los cuales tiene una clave
\\((k_1,k_2, \\ldots ,k_d)\\), donde \\(k_i\\) es un número entero en el rango \\([0,N -1]\\) para \\(N \\geq 2\\),
\\(S\\) puede ser ordenado lexicográficamente en tiempo \\(O(d(n+N))\\) mediante la ordenación radial.
La ordenación radix puede aplicarse a cualquier clave compuesta. Por ejemplo, podemos utilizarla para ordenar cadenas de caracteres de longitud moderada
longitud moderada, ya que cada carácter individual puede representarse como un valor entero.


Sección 4. El mejor algoritmo de ordenación
----------------------------------------

Existen varios algoritmos para ordenar una secuencia. Entre ellos, los de inserción, burbuja o selección
tienen una complejidad media y en el peor de los casos \\(O(n^2)\\), lo que los hace poco adecuados para ser utilizados en escenarios reales.
Otros métodos, como el merge y el quicksort, tienen una complejidad de tiempo \\(O(n \\log n)\\) (ésta es también la complejidad del caso óptimo del problema de ordenación genérico).
complejidad del problema de ordenación genérico).
Para tipos especializados de llaves, hay algoritmos que se ejecutan en tiempo lineal, como bucketsort y radixsort.

En general, hay muchos factores a tener en cuenta a la hora de evaluar la calidad de un algoritmo;
La complejidad temporal es la más importante, pero siempre hay compensaciones con la eficiencia, el uso de la memoria y la estabilidad.
Por ejemplo, el tiempo de ejecución de la ordenación por inserción puede ser \\(O(n+m)\\), donde \\(m\\) es el número de inversiones
(es decir, el número de pares no ordenados) en la secuencia.
Las secuencias con un número bajo de inversiones pueden ordenarse de forma eficiente mediante la ordenación por inserción.

En cuanto a la ordenación rápida, su complejidad de tiempo es \\(O(n \\log n)\\) cuando la lista está dividida
en dos sublistas de la misma longitud; por desgracia, no podemos garantizar que esto ocurra siempre, y
la complejidad en el peor de los casos sigue siendo \\(O(n^2)\\); además, la ordenación rápida no es un método estable, debido al intercambio
de elementos durante la partición de la lista.
A pesar de todos estos problemas, se considera la mejor opción para un algoritmo de ordenación general;
Por ejemplo, se utiliza en las bibliotecas de C, en el sistema operativo Unix y en varias versiones de Java.

En cuanto a la ordenación por fusión, la complejidad en el peor de los casos es \\(O(n \\log n)\\), pero no es un método *in-place*,
debido a los requisitos de memoria adicionales para asignar matrices temporales y copiar entre las matrices.
La ordenación por fusión no es tan atractiva cuando se compara con un algoritmo in situ como quick-sort, pero
es una excelente opción cuando la entrada está estratificada en varios niveles de la jerarquía de
jerarquía de memoria del ordenador (por ejemplo, caché, memoria principal, memoria externa). En este caso
la forma en que el merge-sort procesa las tiradas de datos en largos flujos de fusión hace el mejor
uso de todos los datos traídos como bloque a un nivel de memoria, reduciendo el número total de transferencias de memoria.
Las versiones recientes del sistema operativo Linux utilizan una ordenación merge multidireccional.
El método de ordenación estándar de la clase lista de Python y de los arrays en Java 7 es esencialmente una ordenación por fusión.

Por último, si tenemos que ordenar entradas con claves enteras pequeñas o cadenas de caracteres, la ordenación en cubo o la ordenación radix es
es una excelente opción, ya que se ejecuta en un tiempo \\(O(d(n+N))\\), donde \\([0,N-1]\\) es el rango de claves enteras (y \\(d = 1\\) para la ordenación del cubo).
Por lo tanto, si \\(d(n+N)\\) está por debajo de
la función \\(n \\log n\\), entonces este método de ordenación se ejecuta más rápido que los otros.

Sección 5. Selección
--------------------

La *selección* es el problema de seleccionar el \\(k\\)-ésimo menor elemento de una colección no ordenada de \\(n\\) elementos.
Una solución inmediata es ordenar la colección y luego indexar en la secuencia en el índice \\(k-1\\).
Esto requiere un tiempo de \\(O(n \\log n)\\).

El problema puede ser resuelto en tiempo \\(O(n)\\) mediante una técnica conocida como *prune-and-search*.
En este caso, un problema definido sobre una colección de \\(n\\) objetos se resuelve podando una parte de los objetos,
y resolviendo el problema más pequeño, recursivamente.
Entonces, cuando un problema definido sobre una colección de objetos de tamaño constante
puede ser resuelto mediante algún método directo.
Volviendo a todas las llamadas recursivas se completa la solución.
Por ejemplo, la *búsqueda binaria* es un ejemplo de la técnica de poda y búsqueda.

La *selección rápida aleatoria* es una aplicación del método de poda y búsqueda para resolver el problema de selección.
Dada una secuencia no ordenada \\(S\\) de \\(n\\) elementos comparables, y un número entero \\(k\\) en \\([1,n]\\),
escoge un elemento pivote de \\(S\\) al azar y úsalo para subdividir \\(S\\) en tres subsecuencias
\\(L\\), \\(E\\), y \\(G\\), que contienen los elementos de \\(S\\) menores, iguales y mayores que el pivote,
respectivamente.
En el paso de poda, se encuentra cuál de los tres subconjuntos contiene el elemento deseado, basándose en el valor
de \\(k\\) y en los tamaños de esos subconjuntos. A continuación, se aplica la recursividad sobre el subconjunto adecuado, observando
que el rango del elemento deseado en el subconjunto puede diferir de su rango en el conjunto completo.

A continuación se muestra una implementación de la selección rápida aleatoria::

 1  def quickselect(S, k):
 2    # devuelve el k-ésimo elemento más pequeño de S, para k desde 1 hasta len(S)
 3    if len(S) == 1:
 4      return S[0]
 5    pivot = random.choice(S)            # elegir un elemento pivote al azar de S
 6    L = [x for x in S if x < pivot]     # elementos menos que el pivote
 7    E = [x for x in S if x == pivot]    # elementos iguales al pivote
 8    G = [x for x in S if pivot < x]     # elementos mayores que el pivote
 9    if k <= len(L):
 10     return quickselect(L, k)     # el k-ésimo más pequeño está en L
 11   elif k <= len(L) + len(E):
 12     return pivot                 # el k-ésimo más pequeño es igual al pivote
 13   else:
 14     j = k-len(L)-len(E)
 15     return quickselect(G, j)     # el k-ésimo más pequeño es el j-ésimo en G

Por medio de un argumento probabilístico (que no mostramos), este algoritmo se ejecuta en tiempo \\(O(n)\\) sobre todas las posibles elecciones aleatorias realizadas por el algoritmo,
sobre todas las posibles elecciones aleatorias realizadas por el algoritmo.
En el peor de los casos, la selección rápida aleatoria se ejecuta en tiempo \\(O(n^2)\\).
