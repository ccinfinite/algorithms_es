
Capítulo uno: Análisis de algoritmos
***************************************************************


Sección 1. Introducción
-----------------------

Una *estructura de datos* consiste en una definición formal de cómo se organiza y se accede a un conjunto de datos;
un *algoritmo* es un procedimiento que se define y utiliza para realizar algunas tareas sobre la estructura de datos
dentro de una cantidad finita y computable de tiempo y/o espacio.

El tiempo y el espacio son los recursos que utiliza todo modelo de computación.
Queremos clasificar las estructuras de datos y los algoritmos como buenos o malos
según la cantidad de recursos consumidos durante la computación;
Por lo tanto, estamos interesados en desarrollar herramientas de análisis precisas que capturen el tiempo de ejecución
y el uso de espacio de cada algoritmo.

Por razones teóricas, el tiempo y el espacio están relacionados, y el tiempo se considera el recurso más interesante en informática.
Muchos factores pueden afectar a la complejidad temporal de un algoritmo (entorno de hardware, sistema operativo, lenguaje de programación, etc.),
pero es importante caracterizar el tiempo de ejecución de un algoritmo en función del tamaño de la entrada;
Si todos los demás factores son iguales, la complejidad de un algoritmo se evalúa como una
relación entre el tamaño de la entrada y el tiempo de ejecución del propio algoritmo.

A continuación, presentaremos algunas herramientas que nos permitirán realizar estudios experimentales
sobre la complejidad de un algoritmo, y mostraremos que el uso de experimentos
para evaluar la eficiencia de los algoritmos tiene varias limitaciones.
A continuación, nos centraremos en el desarrollo de algunas herramientas matemáticas que pueden utilizarse para
expresar la relación entre el tiempo de ejecución de un algoritmo y el tamaño de su entrada.


Sección 2. Análisis experimental de algoritmos
----------------------------------------------

Una forma fácil de estudiar la complejidad de un algoritmo es ejecutarlo en varias entradas de prueba significativas
y registrar el tiempo empleado durante cada ejecución.
En Python, esto puede hacerse utilizando la función ``time()`` del módulo ``time``.
Esta función informa del número de segundos transcurridos desde un tiempo de referencia;
si ``time()`` se lanza inmediatamente antes y después de la ejecución del algoritmo
podemos medir el tiempo transcurrido como la diferencia de los dos valores.

Medir el tiempo de la forma anterior es un buen reflejo de la complejidad del algoritmo, pero no es en absoluto perfecto.
El tiempo transcurrido dependerá, por ejemplo, de otros procesos que se estén ejecutando en el ordenador cuando se realice la prueba
(porque la unidad central de procesamiento se comparte entre varios procesos):
En cambio, una medida justa debería considerar pruebas estadísticamente significativas repetidas en sistemas informáticos idénticos,
con entradas y algoritmos idénticos.
El resultado de estas pruebas podría visualizarse en un gráfico (tamaño de la entrada x tiempo de ejecución),
proporcionando alguna intuición sobre la relación entre el tamaño de la entrada y el tiempo de ejecución del algoritmo.
Esto puede llevar a comprender la mejor función del tamaño de entrada con respecto a los datos experimentales.

Aunque el análisis experimental de los tiempos de ejecución es valioso, sobre todo cuando se trata de afinar el código de calidad de producción,
hay algunas limitaciones:

- los tiempos de ejecución experimentales de dos algoritmos son difícilmente comparables
  a menos que los experimentos se realicen en los mismos entornos de hardware y software;
- sólo se dispone de un conjunto limitado de entradas de prueba;
- para estudiar experimentalmente el tiempo de ejecución de un algoritmo, éste debe estar completamente implementado
  (es decir, tiene que estar totalmente traducido a un programa específico).

Estos son serios inconvenientes para el uso de estudios experimentales.


Sección 3. Análisis formal de algoritmos
----------------------------------------

Con el término *análisis formal* describimos un enfoque para analizar la complejidad de los algoritmos
de una manera que es independiente del entorno de hardware y software,
estudiando una descripción de alto nivel del algoritmo
(en lugar de una implementación completa), y teniendo en cuenta todas las entradas posibles.
Para ello, definimos un conjunto de operaciones primitivas y realizamos un análisis
sobre la combinación de operaciones que forman el algoritmo
(ya sea en forma de un fragmento de código real, o de un pseudocódigo independiente del lenguaje).
El conjunto de operaciones primitivas incluye

- el acceso a un objeto para su asignación o lectura
- operaciones aritméticas o de comparación
- llamar a una función o a un método;
- acceso a una lista por índice;
- retorno de una función.

Cada operación primitiva corresponde a una instrucción de bajo nivel con un tiempo de ejecución constante.
En lugar de medir el tiempo de ejecución del algoritmo
contamos cuántas operaciones primitivas se ejecutan, y utilizamos este número como medida del tiempo de ejecución del algoritmo
(asumimos que el recuento de operaciones se correlacionará con un tiempo de ejecución real en un ordenador específico,
dado que cada operación primitiva corresponde a un número constante de instrucciones,
y que sólo hay un número fijo de operaciones primitivas).

Para capturar el crecimiento del tiempo de ejecución de un algoritmo, asociaremos
a cada algoritmo una función \\(f(n)\\) que caracteriza el número de operaciones primitivas
primitivas que se realizan en función del tamaño de la entrada \\(n\\).
Algunas funciones se utilizan ampliamente en el marco de la complejidad computacional:

- la función *constante* :math:`f(n)=c`, para un número constante \\(c\\);
- la función *logarítmica* \\( f (n) = \log_b n \\), para un número constante \N( b \N);
- la función *lineal* \\( f(n)=n \\);
- la función \\( f(n)= n \log n \\);
- la función *cuadrática* \\( f(n)=n^2 \\);
- la función *polinómica* \\( f(n)=n^k \\), para un número constante \\( k \\);
- la *función exponencial* \\( f(n)=b^n \\), para un número constante \\( b \\).

A continuación le ofrecemos algunas pistas sobre estas funciones.

**La función constante**
Para cualquier argumento \\(n\\), la función constante devuelve el valor \\(c\\)).
Por simple que sea, esta función es útil en el análisis de algoritmos, porque
expresa el número de pasos necesarios para realizar una operación básica en un ordenador,
como sumar dos números, asignar un valor a alguna variable o comparar dos números.
Una complejidad constante es la mínima que puede alcanzar un algoritmo.

**La función logarítmica.**
Esta función se define como \\(x = \log_b n\\), si y sólo si \\(b^x = n\\).
Por definición,\\(\log_b 1 = 0\\). El valor constante \\(b\\) se llama la *base* del logaritmo.
La base más común para la función logaritmo en informática es 2,
ya que los ordenadores representan los enteros en notación binaria, y porque una operación común en muchos
algoritmos es dividir repetidamente una entrada por la mitad.
Omitimos la base de la notación cuando es 2, lo que significa que \\(\log n = \log_2 n\\).
Una complejidad logarítmica es típica de aquellos algoritmos recursivos que trabajan sobre
partes cada vez más pequeñas de la entrada.


**La función lineal.**
Dado un valor de entrada \\(n\\), la función lineal devuelve el propio valor \\(n\\).
Esta función se cumple en el análisis de algoritmos cada vez que realizamos una única operación básica para cada uno de los elementos de \\(n\\).
Por ejemplo, la comparación de un número \\(x\\) con cada elemento de una secuencia de tamaño \\(n\\) requiere \\(n\\) comparaciones.
La función lineal representa el mejor tiempo de ejecución para cualquier algoritmo que procese
objetos que no estén ya en la memoria del ordenador (la lectura de \\(n\\) objetos requiere \\(n\\) operaciones).
En general, los algoritmos con complejidad lineal se consideran entre los más eficientes.

**La función \\(n\log n\\).**
Esta función asigna a una entrada \\(n\\) el valor de \\(n\\) multiplicado por el logaritmo de base dos de \\(n\\).
La tasa de crecimiento de esta función es obviamente más pronunciada que la función lineal,
pero sigue siendo inferior a la función cuadrática;
Por lo tanto, un algoritmo con un tiempo de ejecución que es proporcional a \\(n \log n\\)
tiene que ser preferido a uno con tiempo de ejecución cuadrático.
Varios algoritmos importantes presentan un tiempo de ejecución proporcional a esta función.
Por ejemplo, los mejores algoritmos para ordenar \\(n\\) valores arbitrarios requieren un tiempo proporcional a \N(n\\Nlog n\N).

**La función cuadrática**
Dado un valor de entrada \\(n\\), esta función devuelve el producto \\(n^2\\);
esta función aparece en el análisis de algoritmos cuando se encuentran bucles anidados, en los que el bucle interior
interno realiza un número lineal de operaciones, y el bucle externo se realiza un número lineal de veces.
En estos casos, el algoritmo realiza \\(n\\) x \\(n = n^2\\) operaciones; por ejemplo, algunos algoritmos de ordenación tienen
complejidad cuadrática.

**La función polinómica**
Continuando con nuestra discusión de las funciones que son potencias de la entrada, consideramos la función polinómica,
\\(f(n) = n^k\\), con \\(k\\) un número natural fijo. Esta función asigna a un valor de entrada \\(n\\) el producto
de \\(n\\) por sí mismo, \N(k\N) veces.
Esta función es muy común en el contexto del análisis de algoritmos, y representa, por razones teóricas
el límite de lo que se considera *factible* con un ordenador.
Sin embargo, hay que tener en cuenta que un algoritmo con un tiempo de ejecución polinómico
como \\( n^\{354\} \\) no se considera factible, por lo general.

**La función exponencial**
Otra función que se utiliza en el análisis de algoritmos es la función exponencial, \\(f(n)=b^n\\),
siendo \\(b\\) una constante positiva, llamada *base*.
Esta función asigna al argumento de entrada \\(n\\) el valor que se obtiene al multiplicar la base \\(b\\) por sí misma \\(n\\) veces.
Al igual que con la función logaritmo, la base más común para la función exponencial en el análisis de algoritmos es 2.
Por ejemplo, si tenemos un bucle que comienza realizando una operación
y luego duplica el número de operaciones realizadas con cada iteración, entonces el
número de operaciones realizadas en la iteración número \\(n-\\) es \\(2^n\\).
La función exponencial, y toda función que crezca de forma similar, se considera *inferible* en informática.
Si podemos demostrar que un algoritmo, en el mejor de los casos, tiene una complejidad exponencial,
ese algoritmo apenas podría ser ejecutado
en un ordenador, por muy potente que sea el hardware. Esto se debe al crecimiento extremadamente pronunciado de la función.
La mayoría de los problemas abiertos en informática están relacionados de alguna manera con la complejidad exponencial.

Un análisis formal de la complejidad de un algoritmo requiere que el tiempo de ejecución se exprese como una función
del tamaño de la entrada; normalmente se tienen en cuenta los escenarios *mejor*, *peor* y *caso medio*.
El mejor de los casos ocurre cuando la configuración de entrada es tal que el algoritmo trabaja a su mejor velocidad;
por ejemplo, ordenar una colección de datos que ya están ordenados debería requerir la mínima cantidad de tiempo.
Por el contrario, el peor de los casos ocurre cuando el algoritmo tiene que realizar cada
pasos posibles para lograr su objetivo;
imaginemos una colección de números que deben ser ordenados desde el menor
y que, en cambio, se ordenan a partir del mayor.
Estos dos casos son mucho más fáciles de evaluar que el caso medio. En este caso, deberíamos ser capaces de
expresar el tiempo de ejecución de un algoritmo como función de
del tamaño de la entrada, que se obtiene sacando la media de todas las entradas posibles del mismo
tamaño. Esto requiere definir una distribución probabilística de las entradas,
y va más allá de nuestra motivación aquí. Siempre consideramos los tiempos de ejecución en términos del peor caso,
como una función del tamaño de la entrada del algoritmo.


Sección 4. Análisis asintótico
------------------------------

Ya hemos entendido que, en el análisis de algoritmos, queremos caracterizar la complejidad temporal
mediante el uso de funciones que mapean el tamaño de la entrada, \\(n\\), a valores que corresponden a
los principales factores que proporcionan la tasa de crecimiento en términos de \\(n\\).
En lo que sigue, proporcionaremos algunas notaciones que expresan esta relación.

Cada paso básico en la descripción de un algoritmo (así como en su implementación)
corresponde a un número fijo de operaciones primitivas, y queremos
queremos contar el número de operaciones primitivas ejecutadas, hasta un factor constante.

En el siguiente fragmento de código, la función ``find_max`` busca el elemento más grande de una lista
lista de Python ``data``::

 1  def find_max(data):
 2    # Devuelve el máximo elemento de una lista
 3    biggest = data[0]
 4    for val in data:
 5      if val > biggest:
 6        biggest = val
 7    return biggest

Este algoritmo se ejecuta de forma proporcional a \\(n\\), ya que el bucle se ejecuta una vez por cada elemento de ``data``,
y un número fijo de operaciones primitivas (comparación y asignación) se ejecuta en cada pasada.

**La notación \\( O \\).**
Sean las funciones \\(f\\) y \\(g\\) que mapean números enteros positivos a números reales positivos.
Decimos que \\(f(n)\\) es \\( O(g(n)) \\), si existe un real \\(c > 0\\) y un entero \\(n_0 \\geq 1\\)
tal que \\(f(n) \\leq cg(n)\\), se puede obtener para \\(n \\geq n_0\\).
Esta notación se pronuncia como que \\(f(n)\\) es "big-Oh" de \\(g(n)\\), o \\(f(n)\\) "es el orden de" \\(g(n)\\),
o \\(f(n) \\ en O(g(n))\\); la función \\(f\\) es "menor o igual que" otra función \\(g\\)
hasta un factor constante y en el sentido asintótico, ya que \\(n\\) crece hacia el infinito.

La notación \\(O\\) se utiliza para caracterizar los tiempos de ejecución y los límites de espacio de los algoritmos
en términos de algún parámetro \\(n\\), que siempre se entiende como una medida del tamaño del problema.
Por ejemplo, si \\(n\\) denota el número de elementos de la secuencia ``data`` en el código anterior para ``find_max``,
diremos que el algoritmo tiene un tiempo de ejecución \\(O(n)\\). Esto ocurre porque la
asignación antes del bucle requiere un número constante de operaciones primitivas,
y lo mismo ocurre para cada iteración del bucle; finalmente, el bucle se ejecuta \(n\\) veces.
Dado que cada operación primitiva se ejecuta en tiempo constante
tenemos que el tiempo de ejecución del algoritmo sobre una entrada de tamaño \\(n\\) es como máximo una constante de \\(n\\),
es decir, \\(O(n)\\).

**La notación \\( \\Omega \\).**
Sean las funciones \\(f\\) y \\(g\\) que mapean números enteros positivos a números reales positivos.
Decimos que \\(f(n)\\) es \\( \\Omega (g(n)) \\), si \\(g(n)\\) es \\(O(f(n))\\), es decir, existe un real \\(c> 0\\)
y un número entero \\(n_0 \\geq 1\\)
tal que \\(f(n) \\geq cg(n)\\), para \\(n \\geq n_0\\).
Esta notación se pronuncia \\(f(n)\\) es "gran Omega" de \\(g(n)\\); la función \\(f\\) es "mayor que igual a"
la función \\(g\\), hasta un factor constante, y en el sentido asintótico.

**La notación \\( \\Theta \\).**
Sean las funciones \\(f\\) y \\(g\\) que mapean números enteros positivos a números reales positivos.
Decimos que \\(f(n)\\) es \\(\\Theta (g(n))\\) si \\(f(n)\\) es \\(O(g(n))\\) y \\(f(n)\\) es \\(\\Omega (g(n))\\),
es decir, que hay reales \\(c' > 0\\) y \\(c'' > 0\\), y un entero \\(n_0 \\geq 1\\) tal que
\\(c'g(n) \\leq f(n) \\leq c''g(n)\\), para \\(n \\geq n_0\\).
Esto denota que las dos funciones crecen a la misma velocidad.


**Análisis comparativo.**
La notación \\(O\\) se puede utilizar para ordenar clases de funciones por la tasa de crecimiento asintótica.
Las siete funciones introducidas anteriormente pueden ordenarse por tasa de crecimiento creciente en la siguiente
secuencia: \\(1\\), \\(\\log n\\), \\(n\\), \\(n\\log n\\), \\(n^k\\), \\(2^n\\).
Esto significa que si \\(f(n)\\) precede a \\(g(n)\\) en la secuencia, entonces \\(f(n)\\) es \\(O(g(n))\\).
Por ejemplo, si dos algoritmos resuelven el mismo problema en un tiempo de ejecución \\(O(n)\\) y \\(O(n^2)\\) respectivamente,
el primero es asintóticamente mejor que el segundo, hasta un cierto valor de \N(n\\N).

Es interesante comparar las tasas de crecimiento de la función siete, como aparece en la tabla siguiente.

======= ============= === ============== ========= ========== =============
\\(n\\) \\(\\log n\\) n   \\(n \log n\\) \\(n^2\\) \\(n^3\\)  \\(2^n\\)
======= ============= === ============== ========= ========== =============
8       3             8   24             64        512        256
16      4             16  64             256       4,096      65,536
32      5             32  160            1,024     32,768     4,294,967,296
64      6             64  384            4,096     262,144    1.84×10^19
128     7             128 896            16,384    2,097,152  3.40×10^38
256     8             256 2,048          65,536    16,777,216 1.15×10^77
======= ============= === ============== ========= ========== =============

La siguiente tabla explora el tamaño máximo permitido para una instancia de entrada que es procesada
procesado por un algoritmo en 1 segundo, 1 minuto y 1 hora, mostrando que un algoritmo asintóticamente lento es superado, a
a largo plazo, por un algoritmo asintóticamente más rápido,
incluso si el factor constante del algoritmo asintóticamente más rápido es mucho peor.

+------------------------------+-----------------------------------+
|                              | Maximum problem size ( \\(n\\) )  |
+------------------------------+------------+----------+-----------+
| Running time \\( \\mu\\)-sec | 1 second   | 1 minute | 1 hour    |
+==============================+============+==========+===========+
| \\(400n\\)                   | 2,500      | 150,000  | 9,000,000 |
+------------------------------+------------+----------+-----------+
| \\(2n^2\\)                   | 707        | 5,477    | 42,426    |
+------------------------------+------------+----------+-----------+
| \\(2^n\\)                    | 19         | 25       |    31     |
+------------------------------+------------+----------+-----------+

La siguiente tabla muestra el nuevo tamaño máximo del problema que se puede alcanzar para cualquier cantidad fija de tiempo,
suponiendo que los algoritmos con los tiempos de ejecución dados se ejecutan ahora en un ordenador 256 veces más rápido que el anterior.
Cada entrada es una función de ``m``, el tamaño máximo del problema anterior.
Incluso si utilizamos un hardware más rápido, seguimos sin poder superar la desventaja de un algoritmo asintóticamente lento.

+------------------------------+---------------------------+
| Running time \\( \\mu\\)-sec | New maximum problem size  |
+==============================+===========================+
| \\(400n\\)                   | \\(256m\\)                |
+------------------------------+---------------------------+
| \\(2n^2\\)                   | \\(16m\\)                 |
+------------------------------+---------------------------+
| \\(2^n\\)                    | \\(m+8\\)                 |
+------------------------------+---------------------------+

Tenga en cuenta que el uso de la notación \\(O\\) puede ser engañoso si los factores constantes que ocultan son muy grandes.
Considere una función como \\(10^{100} n\\), que es lineal, pero definitivamente inviable;
esto sucede porque el factor constante \\(10^{100}\\) es demasiado grande para ser manejado por cualquier ordenador.
Sin estirar nuestro ejemplo hasta este límite, debemos considerar que los factores constantes y los términos de orden inferior
siempre se esconden en la complejidad de un algoritmo.
Un algoritmo "rápido", en términos generales, es cualquier algoritmo que se ejecuta en un tiempo\\(O(n\log n)\\) con un factor constante razonable.
constante razonable); en algunos contextos, se considera aceptable una complejidad de tiempo \\(O(n^2)\\).
La línea que separa los algoritmos eficientes de los ineficientes debe trazarse entre los algoritmos que se ejecutan en
tiempo polinómico \\(O(n^k)\\) (con una baja \\(k\\)) y los que se ejecutan en tiempo exponencial \\(O(k^n)\\);
la distinción entre algoritmos de tiempo polinómico y de tiempo exponencial se considera una medida robusta de viabilidad.


Sección 5. Ejemplos de análisis de algoritmos
-----------------------------------------

En esta sección, proporcionamos algunos algoritmos simples, y mostramos el análisis de tiempo de ejecución relacionado
utilizando las notaciones introducidas anteriormente.
Utilizaremos una instancia de la clase lista de Python, ``data``, como representación de un array de valores.
Una llamada a la función ``len(data)`` se evalúa en tiempo constante, dado que
se mantiene una variable que registra la longitud de la lista para cada instancia de la lista;
Por lo tanto, un acceso directo a esta variable hará el trabajo en un tiempo de \\(O(1)\\).
Por otra parte, una clase de lista de Python permite acceder a un elemento arbitrario de la lista (usando ``data[j]``, para cada \\(j\\))
en tiempo constante. Esto ocurre porque las listas de Python se implementan como secuencias basadas en arrays,
y las referencias a los elementos de una lista se almacenan en un bloque de memoria consecutivo.
El elemento \\(j-\\) de la lista se puede encontrar utilizando el índice como un desplazamiento en la matriz subyacente.
Obsérvese que podríamos utilizar cualquier lenguaje de programación, o cualquier pseudo-lenguaje para describir el
algoritmo, y el análisis seguiría siendo el mismo.

Sección 5.1 Encontrar el máximo
================================

Dadas las consideraciones anteriores sobre ``data``, ``len`` y ``data[j]``, podemos
confirmar el análisis preliminar que hemos hecho sobre el algoritmo ``find_ max``,
con \\(n\\) la longitud de ``data``.
La inicialización de ``biggest`` a ``data[0]`` utiliza \\(O(1)\\) tiempo.
El bucle se ejecuta \\(n\\) veces, y dentro de cada iteración, se realiza una comparación y, posiblemente, una asignación.
Por último, la sentencia ``return`` en Python utiliza un tiempo de \\(O(1)\\).
Entonces tenemos que la función se ejecuta en tiempo \\(O(n)\\), como se esperaba.

Sección 5.2 Promedios de los prefijos
===========================

Sea \\(S\\) una secuencia de números \\(n\\); queremos calcular una secuencia \\(A\\),
tal que \\(A[j]\\) sea la media de los elementos \\(S[0], \\ldots ,S[j]\\), para \\(j = 0, \\ldots ,n-1\\).
Este problema se denomina *promedios de una secuencia*, y tiene varias aplicaciones.
Por ejemplo, dados los rendimientos año a año de un fondo de inversión,
un inversor puede querer ver la media de los rendimientos anuales del fondo para los
años más recientes.
Del mismo modo, teniendo en cuenta los registros de uso de la web, el gestor de un sitio web puede querer
de un sitio web puede querer seguir las tendencias de uso medio a lo largo de varios períodos de tiempo.

La primera implementación de un algoritmo para calcular los promedios de los prefijos se da en el siguiente código.
Cada elemento de \\(A\\) se calcula por separado, utilizando un bucle para calcular la suma parcial::

 1  def prefix_average(S):
 2    # Devuelve la lista A tal que A[j] es igual a la media de S[0], ..., S[j], para todo j
 3    n = len(S)
 4    A = [0]*n                # list of n zeros
 5    for j in range(n):
 6      total = 0
 7      for i in range(j + 1):
 8        total += S[i]
 9      A[j] = total / (j+1)    # the j-th average
 10   return A

La asignación ``n = len(S)`` se ejecuta en tiempo constante.
La sentencia ``A = [0]*n`` crea e inicializa una lista con longitud \\(n\\), con todas las entradas iguales a cero;
Esto utiliza un número constante de operaciones primitivas por entrada, y por lo tanto se ejecuta en tiempo \\(O(n)\\).
El cuerpo del bucle exterior está controlado por el contador \\(j\\), y se ejecuta \\(n\\) veces,
para \\(j = 0, \\ldots ,n-1\\).
Esto significa que las sentencias ``total = 0`` y ``A[j] = total / (j+1)`` se ejecutan \\(n\\) veces cada una.
Contribuyen con un número de operaciones primitivas proporcional a \\(n\\), es decir, tiempo \\(O(n)\\).
El cuerpo del bucle interior, que está controlado por el contador \\(i\\), se ejecuta \\(j+1\\) veces,
dependiendo del valor actual del contador del bucle exterior \\(j\\).
Así, la sentencia ``total += S[i]`` se ejecuta \\(1+2+3+ \\ldots +n\\) veces, lo que equivale a \\(n(n+1)/2\\);
esto significa tiempo \\(O(n^2)\\).
El tiempo de ejecución de esta implementación viene dado por la suma de tres términos:
el primer y el segundo término son \\(O(n)\\), y el tercer término es \\(O(n^2)\\).
El tiempo total de ejecución de ``prefix_average`` es \\(O(n^2)\\).

Un algoritmo de tiempo lineal para el mismo problema se da en el siguiente código::

 1  def prefix_average_linear(S):
 2    # Devuelve la lista A tal que A[j] es igual a la media de S[0], ..., S[j], para todo j
 3    n = len(S)
 4    A = [0]*n                # list of n zeros
 5    total = 0
 6    for j in range(n):
 7      total += S[j]
 8      A[j] = total / (j+1)    # average based on current sum
 9    return A

En el primer algoritmo, la suma de prefijos se calcula de nuevo para cada valor de \\(j\\), lo que lleva al comportamiento cuadrático.
En este nuevo algoritmo, mantenemos la suma de prefijos actual de forma dinámica, calculando efectivamente
calculando \\(S[0]+S[1]+ \\ldots +S[j]\\) como ``total += S[j]``, donde ``total`` es igual a la
suma \\(S[0]+S[1]+\\ldots +S[ j-1]\\) calculada por la pasada anterior del bucle sobre \\(j\\).
La inicialización de las variables requiere un tiempo de \\(O(1)\\).
La inicialización de la lista requiere un tiempo de \\(O(n)\\).
Hay un único bucle, controlado por el contador \\(j\\);
el mantenimiento de ese contador por parte del iterador de rango cuesta tiempo \\(O(n)\\).
El cuerpo del bucle se ejecuta \\(n\\) veces, para \\(j = 0, \\ldots ,n-1\\).
Así, las sentencias ``total += S[j]`` y ``A[j] = total / (j+1)`` se ejecutan \\(n\\) veces.
Dado que cada una de estas sentencias utiliza tiempo \\(O(1)\\) por iteración, su contribución global es tiempo \\(O(n)\\).
El tiempo total de ejecución del algoritmo ``prefix_average_linear`` es \\(O(n)\\), que es mucho mejor que
mejor que el tiempo cuadrático del primer algoritmo que hemos proporcionado.



Sección 5.3 Desunión de conjuntos de tres vías
======================================

Dadas tres secuencias de números, \\(A\\), \\(B\\), y \\(C\\), y dado que ninguna secuencia individual contiene
valores duplicados, el problema de la *división de conjuntos* consiste en decidir si la intersección de las tres secuencias está vacía.
A continuación se ofrece una implementación sencilla en Python::

 1  def disjoint1(A, B, C):
 2    # Devuelve True si no hay ningún elemento en común en las tres listas
 3    for a in A:
 4      for b in B:
 5        for c in C:
 6          if a == b == c:
 7            return False    # there is a common value
 8    return True             # sets are disjoint

Este algoritmo recorre cada posible triplete de valores de los tres conjuntos para comprobar si esos valores son iguales.
Si cada conjunto tiene un tamaño de \\(n\\), entonces la complejidad de tiempo en el peor de los casos es de \\(O(n^3)\\).
Obsérvese que si dos elementos seleccionados de \\(A\\) y \\(B\\) no coinciden entre sí
es inútil comprobar la coincidencia con los valores de C; por lo tanto, el siguiente código da una versión mejorada::

 1  def disjoint2(A, B, C):
 2    # Devuelve True si no hay ningún elemento en común en las tres listas
 3    for a in A:
 4      for b in B:
 5        if a == b:           # check C only if there is a match from A and B
 6          for c in C:
 7            if a == c        # here a == b == c
 8              return False   # there is a common value
 9     return True             # sets are disjoint

El bucle sobre \\(A\\) requiere un tiempo \\(O(n)\\).
El bucle sobre \\(B\\) supone un total de \\(O(n^2)\\) de tiempo, ya que ese bucle se ejecuta \\(n\\) diferentes veces.
A continuación, la prueba ``a == b`` se evalúa \ (O(n^2)\ ~ veces.
Ahora hay \\(n\\)x\\(n\\) pares \\((a,b)\\) que considerar pero, bajo la suposición anterior de que
ninguna secuencia individual contiene valores duplicados, puede haber a lo sumo \\(n\\) pares con a igual a b.
Por lo tanto, el bucle más interno, sobre \\(C\\), y los comandos dentro del cuerpo de ese bucle,
utilizan como máximo \\(O(n^2)\\) tiempo. El tiempo total empleado es de \\(O(n^2)\\).

Sección 5.4 Unicidad de los elementos
==============================

Dada una secuencia única \\(S\\) con \\(n\\) elementos, queremos decidir si todos los elementos de esa colección
son distintos entre sí.
La primera solución a este problema utiliza un algoritmo iterativo sencillo, que
comprueba si todos los pares de índices distintos \\( (j,k) \\), con \\(j < k\\), se refieren a elementos que son iguales::

 1  def unique1(S):
 2    # Devuelve True si no hay elementos duplicados en la secuencia S
 3    for j in range(len(S)):
 4      for k in range(j+1, len(S)):
 5       if S[j] == S[k]:
 6          return False        # hay un par duplicado
 7    return True               # todos los elementos son distintos

La primera iteración del bucle exterior provoca \\(n-1\\) iteraciones del bucle interior,
la segunda iteración del bucle exterior provoca \\(n-2\\) iteraciones del bucle interior, y así sucesivamente.
Así, el tiempo de ejecución en el peor de los casos de esta función es proporcional a
\\((n-1)+(n-2)+ \\ldots +2+1\\), es decir, \\(O(n^2)\\).
Obsérvese que si se ordena la secuencia de elementos, cualquier elemento duplicado se colocará junto a otro.
Por lo tanto, todo lo que tenemos que hacer es realizar un único barrido sobre la secuencia ordenada, en busca de duplicados consecutivos.
Una implementación en Python de este algoritmo es la siguiente::

 1  def unique2(S):
 2    # Devuelve True si no hay elementos duplicados en la secuencia S
 3    temp = sorted(S)            # S ordanda
 4    for j in range(1, len(temp)):
 5      if temp[j-1] == temp[j]:
 6        return False            # hay un par duplicado
 7    return True                 # todos los elementos son distintos

La función ``sorted`` devuelve una copia de la lista original, con elementos en orden creciente.
El tiempo medio de ejecución es de \\(O(n\\log n)\\).
El bucle subsiguiente se ejecuta en tiempo \\(O(n)\\) y, por tanto, todo el algoritmo se ejecuta en tiempo \\(O(n \\log n)\\).


Sección 6. Un enfoque matemático: invariantes de bucle
---------------------------------------------------

En las secciones anteriores hemos ofrecido algunos ejemplos de evaluación de la complejidad de los algoritmos.
El enfoque es básicamente siempre el mismo: se escribe un algoritmo (ya sea utilizando un
pseudocódigo o un lenguaje de programación), y la evaluación de las operaciones básicas (de tiempo constante)
y los bucles anidados que se producen en los algoritmos nos permiten establecer una relación entre el tamaño de la entrada y
el número de operaciones realizadas.
En otras palabras, somos capaces de encontrar un "acuerdo" sobre el consumo de tiempo del algoritmo,
pero esto no implica que hayamos dado una prueba formal de ese límite de tiempo.

Una de las técnicas utilizadas en el campo del análisis de la complejidad se llama *invariante de bucle*;
esta técnica es similar, si no igual, a la técnica matemática llamada *inducción*.
Imaginemos que queremos demostrar que una afirmación \\(L\\) sobre un bucle es correcta;
definimos \\(L\\) en términos de afirmaciones más pequeñas \\(L_0, L_1, \\ldots , L_k\\), con:

- la afirmación inicial, \\(L_0\\), es verdadera antes de comenzar el bucle;
- si \\(L_{j−1}\\) es verdadera antes de la iteración \\(j\\), entonces \\(L_j\\) será verdadera después de la iteración \\(j\\);
- la última sentencia, \\(L_k\\), implica que la sentencia \\(L\\) sea verdadera.

Dado el siguiente código para la función ``find``, que debe encontrar el índice más pequeño en
en el que aparece el elemento ``val`` en la secuencia ``Seq``, utilizamos la invariante de bucle para
demostrar su corrección::


 1  def find(Seq, val):
 2    # Devuelve el índice j tal que Seq[j] == val, -1 en caso contrario
 3    n = len(Seq)
 4    j = 0
 5    while j < n:
 6      if Seq[j] == val:
 7        return j
 8      j += 1
 9    return −1

Para demostrar que ``find`` es correcto, definimos una serie de sentencias \\(L_j\\),
una por cada iteración \\(j\\) del bucle "while":

**\\(L_j\\):** ``val`` es diferente a cualquiera de los primeros elementos \\(j\\) de ``Seq``.

Esta afirmación se comprueba que es cierta con \\(j=0\\), es decir, en la primera iteración del bucle,
porque no hay elementos entre los primeros 0 de ``Seq``.
En la iteración \\(j\\)-ésima, el elemento ``val`` se compara con ``Seq[j]``,
y se devuelve ``(j\\\\\N)`` si los dos elementos son iguales; si los dos elementos no son
iguales, entonces hay un elemento más que no es igual a ``val``, y el índice \\(j\\) se incrementa.
Por lo tanto, la afirmación \\(L_j\\) es verdadera para el nuevo valor de \\(j\\); por lo tanto, es
verdadera al principio de la siguiente iteración.
Si el bucle ``while`` termina sin devolver un índice en ``Seq``, tenemos que \\(j = n\\).
Esto implica que \\(L_n\\) es verdadera, porque no hay elementos de ``Seq`` igual a ``val``.
Por lo tanto, el algoritmo devuelve correctamente -1.
