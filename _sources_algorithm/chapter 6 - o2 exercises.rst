
Capítulo 6: Clasificación - ejercicios y desafíos
***********************************************

Sección 1. Revisión de la teoría
-------------------------------

Completa y ejecuta los trozos de código de las siguientes secciones (el código se encuentra en el capítulo seis).

**Ejercicio 1.1**

La *ordenación de burbujas* compara parejas de valores adyacentes en una lista, y las intercambia si no están en el orden correcto.
Realiza múltiples pases en la lista; en cada uno de ellos, el valor más grande se burbujea hasta su ubicación correcta.
El código Python de la ordenación por burbujas se da en el siguiente ActiveCode.

.. activecode:: bubble-sort
   :language: python
   :caption: Function for the bubble sort

   def bubbleSort(alist):
     for passnum in range(len(alist)-1,0,-1):
       for i in range(passnum):
         if alist[i] > alist[i+1]:
           temp = alist[i]
           alist[i] = alist[i+1]
           alist[i+1] = temp

        # INSERTE EL NUEVO CÓDIGO AQUÍ


Podemos escribir una *ordenación de burbuja corta*, que se detenga si encuentra que la lista ya ha sido ordenada.

.. activecode:: short-bubble-sort
   :language: python
   :caption: Función para la clasificación de burbujas cortas

   def shortbubbleSort(alist):
     exchanges = True
     passnum = len(alist)-1
     while passnum > 0 and exchanges:
       exchanges = False
       for i in range(passnum):
         if alist[i] > alist[i+1]:
           exchanges = True
           temp = alist[i]
           alist[i] = alist[i+1]
           alist[i+1] = temp
       passnum = passnum-1

        # INSERTE EL NUEVO CÓDIGO AQUÍ


**Ejercicio 1.2**

La *ordenación por selección* realiza múltiples pasadas sobre una lista
buscando el valor más grande, y moviéndolo a su ubicación correcta;
esto significa que sólo hay un intercambio por cada pase.
Después de la primera pasada, el elemento más grande está en el lugar correcto;
después de la segunda pasada, el siguiente más grande está en su lugar.
El código Python de la ordenación por selección se da en el siguiente ActiveCode.

.. activecode:: selection-sort
   :language: python
   :caption: Función para la ordenación de la selección

   def selectionSort(alist):
     for fillslot in range(len(alist)-1,0,-1):
       positionOfMax=0
       for location in range(1,fillslot+1):
         if alist[location] > alist[positionOfMax]:
           positionOfMax = location
       temp = alist[fillslot]
       alist[fillslot] = alist[positionOfMax]
       alist[positionOfMax] = temp

        # INSERTE EL NUEVO CÓDIGO AQUÍ


**Ejercicio 1.3**

La ordenación por inserción crea una sublista ordenada en las posiciones iniciales de la lista
e inserta en esta sublista un nuevo elemento, produciendo una nueva sublista ordenada.
Partiendo de una lista con el primer elemento solamente, cada elemento del segundo al último
se compara con los de la sublista ya ordenada;
los elementos mayores se desplazan hacia la derecha, y el elemento actual se inserta en la sublista
cuando se alcanza un elemento menor. El código Python para la ordenación por inserción está escrito en el siguiente ActiveCode.

.. activecode:: insertion-sort
   :language: python
   :caption: Función para la clasificación por inserción

   def insertionSort(alist):
     for index in range(1,len(alist)):
       currentvalue = alist[index]
       position = index
       while position > 0 and alist[position-1] > currentvalue:
         alist[position]=alist[position-1]
         position = position-1
       alist[position]=currentvalue

        # INSERTE EL NUEVO CÓDIGO AQUÍ


**Ejercicio 1.4**

La *ordenación por fusión* es un algoritmo recursivo.
Si la lista está vacía o tiene un elemento, se ordena; si la lista tiene más de un elemento
Si la lista tiene más de un elemento, se divide y se invoca recursivamente la ordenación por fusión en ambas mitades.
Una vez ordenadas las dos mitades, se realiza la fusión.
El código de Python para una secuencia basada en arrays se da en el siguiente ActiveCode.

.. activecode:: merge-sort
   :language: python
   :caption: Function for the merge sort

   def mergeSort(alist):
     print("Dividiendo ",alist)
     if len(alist) > 1:
       mid = len(alist)//2
       lefthalf = alist[:mid]
       righthalf = alist[mid:]

       mergeSort(lefthalf)
       mergeSort(righthalf)

       i=0
       j=0
       k=0
       while i < len(lefthalf) and j$< len(righthalf):
         if lefthalf[i] <= righthalf[j]:
           alist[k] = lefthalf[i]
           i=i+1
         else:
           alist[k] = righthalf[j]
           j=j+1
         k=k+1

       while i < len(lefthalf):
         alist[k]=lefthalf[i]
         i=i+1
         k=k+1

       while j <$len(righthalf):
         alist[k]=righthalf[j]
         j=j+1
         k=k+1
     print("Fusión ",alist)

        # INSERTE EL NUEVO CÓDIGO AQUÍ


**Ejercicio 1.5**

La *ordenación rápida* comienza seleccionando un valor, el *punto de separación*, que es el primer elemento de la lista, normalmente;
se encuentra su posición correcta en la lista, y esta posición (el *punto de división*) se utiliza en el proceso
de dividir la lista en dos sublistas. La ordenación rápida se aplicará recursivamente en estas sublistas.

Dos marcas, *marca izquierda* y *marca derecha*, se definen
como la primera y la última posición de los elementos restantes de la lista, respectivamente.
Leftmark se incrementa hasta que se encuentra un valor mayor que el pivote;
rightmark se decrementa hasta que se encuentra un valor menor que el pivote.
Los dos valores están fuera de lugar con respecto al punto de división, y se intercambian.

Este procedimiento continúa hasta que el valor de la marca derecha es menor que el de la marca izquierda; ahora se ha encontrado el punto de división.
El valor del pivote se intercambia con el contenido del punto de división, lo que significa que el pivote está en su posición correcta.
La función quickSort se invoca ahora recursivamente en las dos sublistas. La función quickSort se define como sigue.

.. activecode:: quick-sort
   :language: python
   :caption: Función para la clasificación rápida

   def quickSort(alist,first,last):
     if first < last:
       splitpoint = partition(alist,first,last)
       quickSort(alist,first,splitpoint-1)
       quickSort(alist,splitpoint+1,last)

   def partition(alist,first,last):
     pivotvalue = alist[first]
     leftmark = first+1
     rightmark = last

     done = False
     while not done:
       while leftmark <= rightmark and alist[leftmark] <= pivotvalue:
         leftmark = leftmark + 1
       while leftmark <= rightmark and alist[rightmark] >= pivotvalue:
         rightmark = rightmark -1

       if rightmark < leftmark:
         done = True
       else:
         temp = alist[leftmark]
         alist[leftmark] = alist[rightmark]
         alist[rightmark] = temp

     temp = alist[first]
     alist[first] = alist[rightmark]
     alist[rightmark] = temp
     return rightmark

        # INSERTE EL NUEVO CÓDIGO AQUÍ


**Ejercicio 1.6**

La selección es el problema de elegir el \\(k\\)-ésimo menor elemento de una colección no ordenada de \\(n\\) elementos.

Dada una secuencia no ordenada de \\(S\\) de \\(n\\) elementos comparables, y un número entero \\(k\\) en \\([1,n]\\) la selección
la *selección rápida aleatoria* escoge un elemento pivote de \\(S\\) al azar,
y lo utiliza para subdividir \\(S\\) en tres subsecuencias
\\(L\\), \\(E\\) y \\(G\\), que contienen los elementos de \\(S\\) menores, iguales y mayores que el pivote,
respectivamente.
A continuación, se encuentra cuál de los tres subconjuntos contiene el elemento deseado, basándose en el valor
de \\(k\\) y en los tamaños de esos subconjuntos. A continuación, se aplica la recursión sobre el subconjunto adecuado.
A continuación se muestra una implementación de la selección rápida aleatoria.

.. activecode:: quick-sort-2
   :language: python
   :caption: Función para la clasificación rápida

   def quickselect(S, k):
     if len(S) == 1:
       return S[0]
     pivot = random.choice(S)
     L = [x for x in S if x < pivot]     # elementos menos que el pivote
     E = [x for x in S if x == pivot]    # elementos iguales al pivote
     G = [x for x in S if pivot < x]     # elementos mayores que el pivote
     if k <= len(L):
       return quickselect(L, k)     # el k-ésimo más pequeño está en L
     elif k <= len(L) + len(E):
       return pivot                 # el k-ésimo más pequeño es igual al pivote
     else:
       j = k-len(L)-len(E)
       return quickselect(G, j)     # el k-ésimo más pequeño es el j-ésimo en G

        # INSERTE EL NUEVO CÓDIGO AQUÍ



Sección 2. Problemas de clasificación
------------------------------

En esta sección, mostraremos algunos algoritmos clásicos de ordenación; comprueba, completa y ejecuta el ActiveCode en cada subsección.

**Ejercicio 2.1

La *ordenación por casillero* es un algoritmo adecuado para ordenar secuencias de elementos en las que el número de elementos
y el número de valores clave posibles son aproximadamente los mismos.
Requiere \\( O(n+R) \\) tiempo, donde \\(n\\) es el número de elementos de la secuencia y \\(R\\), el *rango*,
es el número de valores posibles.

El algoritmo encuentra los valores mínimo y máximo de la secuencia (*min* y *max*, respectivamente),
y el rango \\( R=max-min+1 \\).
Se crea una nueva matriz de *casilleros*, inicialmente vacía; su tamaño es igual al rango \\( R \\).

Cada elemento \\(arr[i]\\) de la matriz inicial se coloca en el hueco del índice \\(arr[i] – min\\).
A continuación, los elementos del array palomero, los huecos no vacíos, se vuelven a colocar en el array original.
La ordenación por casilleros tiene un uso limitado, ya que los requisitos rara vez se cumplen.
Para matrices donde el rango es mucho mayor que \\(n\\), la ordenación por cubos es una generalización que es más eficiente en espacio y tiempo.
Una implementación de la ordenación por casillero se muestra en el siguiente ActiveCode.

.. activecode:: pigeonhole-sort
   :language: python
   :caption: Función para la clasificación de casillas

   def pigeonhole_sort(a):
     # construir los casilleros
     my_min = min(a)
     my_max = max(a)
     size = my_max - my_min + 1
     holes = [0] * size

     for x in a:
       holes[x - my_min] += 1

     i = 0
     for count in range(size):
       while holes[count] > 0:
         holes[count] -= 1
         a[i] = count + my_min
         i += 1

        # INSERTE EL NUEVO CÓDIGO AQUÍ


**Ejercicio 2.2**

*La ordenación en peine es una mejora de la ordenación en burbuja. La ordenación por burbujas compara valores adyacentes y elimina todas las inversiones.
La ordenación en peine utiliza huecos de tamaño superior a 1. El hueco comienza con un valor grande y se reduce en un factor de 1,3 en cada iteración,
hasta que alcanza el valor 1. Así, Comb Sort elimina más de un *contador de inversiones* con un solo intercambio.

El factor de encogimiento se ha encontrado empíricamente en 1,3; en el caso medio, este algoritmo funciona mejor que Bubble Sort.
La complejidad temporal del algoritmo en el caso medio es de \\( \\Omega(n^2/2^p) \\), donde \\(p\\) es el número de incrementos.
La complejidad en el peor de los casos de este algoritmo es \\(O(n^2)\\) y la complejidad en el mejor de los casos es \\(O(n \\log n)\\)
A continuación se muestra la implementación.

.. activecode:: comb-sort
   :language: python
   :caption: Función para la clasificación en peine

   # encontrar el siguiente hueco desde el actual
   def getNextGap(gap):
     gap = (gap * 10) // 13
     if gap < 1:
       return 1
     return gap

   # ordenar arr[] con Comb Sort
   def combSort(arr):
     n = len(arr)
     gap = n

     swapped = True
     while gap !=1 or swapped == 1:
       gap = getNextGap(gap)        # encontrar el siguiente hueco
       swapped = False               # inicializar swapped como false, para comprobar si se produce un intercambio
       for i in range(0, n-gap):
         if arr[i] > arr[i + gap]:
           arr[i], arr[i + gap]=arr[i + gap], arr[i]
           swapped = True

         # INSERTE EL NUEVO CÓDIGO AQUÍ



**Ejercicio 2.3**

*La búsqueda por saltos es un algoritmo de búsqueda para matrices ordenadas.
Comprueba menos elementos (que la búsqueda lineal) saltando adelante por pasos fijos o saltando algunos elementos
en lugar de buscar en todos los elementos.
Supongamos que tenemos un array \\(arr[]\\) de tamaño \\(n\\) y un bloque (a saltar) de tamaño \\(m\\).
Buscamos en los índices \\(arr[0] , arr[m] , arr[2m], \\ldots , arr[km] , \\ldots \\), y así sucesivamente.
Una vez que encontramos el intervalo \\( arr[km] < x < arr[(k+1)m] \\), realizamos una operación de búsqueda lineal a partir del índice
\\(km\\) para encontrar el elemento \\(x\\).

En el peor de los casos, tenemos que hacer \\(n/m\\) saltos, y si el último valor comprobado es mayor que
el elemento a buscar,
realizamos \\(m-1\\) comparaciones, para la búsqueda lineal.
Por lo tanto, el número total de comparaciones en el peor de los casos será de \\( ((n/m) + m-1) \\).
La función \( f(m)=(n/m) + m-1 \\N es mínima cuando \\( m = \\sqrt n \\); por tanto, el mejor tamaño de paso es
\\( m = \\sqrt n \\).
En lo que sigue, el código para implementar la búsqueda de saltos.

.. activecode:: jump-search
   :language: python
   :caption: Función para la búsqueda de saltos

   import math

   def jumpSearch(arr , x):
     n = len(arr)
     step = math.sqrt(n)

     # encontrar el bloque donde está el elemento
     prev = 0
     while arr[int(min(step, n)-1)] < x:
       prev = step
       step += math.sqrt(n)
       if prev >= n:
         return -1

     # encontrar el bloque donde está el elemento
     while arr[int(prev)] < x:
       prev += 1
       if prev == min(step, n):   # if we reach next block or end of array, the element is not present
         return -1

     # si se encuentra el elemento
     if arr[int(prev)] == x:
       return int(prev)
     return -1

        # INSERTE EL NUEVO CÓDIGO AQUÍ



**Ejercicio 2.4**

La *Búsqueda exponencial* en un array ordenado implica dos pasos::

 Encontrar el rango donde el elemento x está presente
 Hacer una búsqueda binaria en ese rango

Para encontrar el rango, empezamos con el subarray de tamaño 1, comparamos su último elemento con \\(x\\);
luego probamos con el tamaño 2, luego con el 4, y así sucesivamente hasta que el último elemento de una submatriz no sea mayor que \\(x\\).
Una vez que encontremos un índice \\(i\\) sabemos que el elemento debe estar presente entre \\(i/2\\) y \\(i\\).
La complejidad temporal es \\( O(\\log n) \\), y el espacio requerido por la búsqueda binaria \\( O(\\log n) \\) espacio.
Con la búsqueda binaria iterativa, sólo necesitaríamos un espacio \\( O(1)\\).

.. activecode:: jump-search-2
   :language: python
   :caption: Función para la búsqueda de saltos

   def binarySearch(arr, l, r, x):
   # función de búsqueda binaria que devuelve la ubicación de x en un array dado arr[l..r]
     if r >= l:
       mid = l + ( r-l ) // 2
       if arr[mid] == x:
         return mid
       if arr[mid] > x:
         return binarySearch(arr, l, mid - 1, x)
       return binarySearch(arr, mid + 1, r, x)
     return -1

   def exponentialSearch(arr, x):
   # devuelve la posición de la primera ocurrencia de x en el array
     n = len(arr)
     if arr[0] == x:
       return 0

     # encontrar el rango para la búsqueda binaria mediante la duplicación repetida
     i = 1
     while i < n and arr[i] <= x:
       i = i * 2

     # llamar a la búsqueda binaria del rango
     return binarySearch(arr, i // 2, min(i, n-1), x)

        # INSERTE EL NUEVO CÓDIGO AQUÍ


Sección 3. Ejercicios y autoevaluación
----------------------------------------

**Ejercicio 3.1**

Dada la siguiente lista de números para ordenar: [19, 1, 9, 7, 3, 10, 13, 15, 8, 12];
¿qué lista se obtiene después de tres pasadas completas de ordenación por burbujas?

**Ejercicio 3.2**

Dada la siguiente lista de números para ordenar: [11, 7, 12, 14, 19, 1, 6, 18, 8, 20];
¿qué lista se obtiene después de tres pases completos de la ordenación por selección?

**Ejercicio 3.3**

Dada la siguiente lista de números para ordenar: [15, 5, 4, 18, 12, 19, 14, 10, 8, 20];
¿qué lista se obtiene tras tres pases completos de ordenación por inserción?

**Ejercicio 3.4**

Dada la siguiente lista de números: [21, 1, 26, 45, 29, 28, 2, 9, 16, 49, 39, 27, 43, 34, 46, 40];
¿cuál es la lista que se ordenará después de tres llamadas recursivas de mergesort?

**Ejercicio 3.5**

Dada la siguiente lista de números: [21, 1, 26, 45, 29, 28, 2, 9, 16, 49, 39, 27, 43, 34, 46, 40];
¿cuáles son las dos primeras listas que se fusionan?

**Ejercicio 3.6**

Diga la siguiente lista de números [14, 17, 13, 15, 19, 10, 3, 16, 9, 12]:
¿cuál es la lista después de la segunda partición del quicksort?

**Ejercicio 3.7**

¿Qué ordenación de Shell, Quick, Merge o Insertion se garantiza que es \\(O(n \\log n)\\) en el peor de los casos?

**Ejercicio 3.8**

Un algoritmo que ordena las entradas de clave-valor por clave es *straggling*
si en algún momento dos entradas \\(e_i\\) y \\(e_j\\) tienen claves iguales, pero \\(e_i\\) aparece antes que \\(e_j\\) en la entrada,
entonces el algoritmo coloca a \\(e_i\\) después de \\(e_j\\) en la salida.
Describa un cambio en el algoritmo de ordenación por fusión para que sea rezagado.

**Ejercicio 3.9**

Supongamos que se nos dan dos secuencias ordenadas por \\(n\\) elementos \\(A\\) y \\(B\\) cada una con elementos distintos,
pero con algunos elementos en ambas secuencias.
Encontrar un método de tiempo \\(O(n)\\) para calcular una secuencia que represente la
unión de \\(A\\) y \\(B\\) (sin duplicados) como una secuencia ordenada.

**Ejercicio 3.10**

De las \\(n!\\) posibles entradas a un algoritmo de ordenación basado en la comparación,
¿cuál es el número máximo de entradas que pueden ser ordenadas correctamente con tan sólo \\(n\\) comparaciones?

**Ejercicio 3.11**

Supongamos que \\(S\\) es una secuencia de \\(n\\) valores, cada uno igual a 0 o 1.
¿Cuánto tiempo se tarda en ordenar \\(S\\) con el algoritmo merge-sort? ¿Y con el quick-sort?
