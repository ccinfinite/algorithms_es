

Capítulo 3: Secuencias basadas en matrices - ejercicios y desafíos
***************************************************************


Sección 1. Revisión de la teoría
-------------------------------

**Ejercicio 1.1**

Completa y ejecuta el siguiente fragmento de código (que se encuentra en el Capítulo Tres, Sección 3);
Compara los resultados en tu sistema con los reportados en el Capítulo Tres.
(Sugerencia: aunque la velocidad puede diferir, la asintótica debería ser similar).

Luego, modifique el código para demostrar que la clase lista de Python ocasionalmente reduce
el tamaño de su matriz subyacente cuando se extraen elementos de una lista.
(Sugerencia: haz que la lista sea lo suficientemente grande antes de empezar a eliminar entradas).

.. activecode:: size-of-data
   :language: python
   :caption: Imprime el tamaño de un array dinámico en relación con sus elementos

   import sys               # incluye la función getsizeof

   data = [ ] 			    # lista vacía
   for k in range(n):
     a = len(data) 			    # a es el número de elementos
     b = sys.getsizeof(data) 	# tamaño de los datos en bytes
     print('Longitud: {0:3d}; Tamaño en bytes: {1:4d}'.format(a, b))
     data.append(None) 		    # aumentar la longitud en uno

           # INSERTAR NUEVO CÓDIGO AQUÍ


**Ejercicio 1.2**

La implementación de ``insert`` para la clase ``DynamicArray`` ha sido introducida en el Capítulo Tres, Sección 4.2.
Cuando se produce un redimensionamiento, la operación de redimensionamiento tarda en copiar todos los elementos de
un array antiguo a uno nuevo, y luego el bucle posterior en el cuerpo de
``insert`` desplaza muchos de esos elementos. Modifica el siguiente trozo de código para que, en el caso de un redimensionamiento
los elementos sean desplazados a su posición final durante esa operación, evitando así el desplazamiento posterior.
(Sugerencia: utilice dos bucles no anidados)

.. activecode:: insert-for-dynamic-array
   :language: python
   :caption: Insertar un valor en la posición k de un array dinámico

   def insert(self, k, value):

     if self._n == self._capacity: 		# no hay suficiente espacio, redimensiona la matriz
       self._resize(2*self._capacity)
     for j in range(self._n, k, −1): 	# desplazamiento a la derecha, primero a la derecha
       self._A[j] = self._A[j−1]
     self._A[k] = value
     self. n += 1




Sección 2. Un problema: construir un juego Tic-Tac-Toe
-------------------------------------------------

En esta sección, construiremos un juego de tres en raya para dos jugadores, que podremos jugar en la línea de comandos.
Revisa el siguiente código; luego, completa y ejecuta el ActiveCode al final de la sección.

Inicialmente, construiremos un tablero de juego vacío \\(3 \\times 3\\) que está numerado como el teclado numérico.
Un jugador puede hacer su movimiento en el tablero de juego introduciendo el número del teclado numérico.
Para ello, utilizaremos un diccionario,
un tipo de datos primitivo en Python que almacena datos en formato "clave: valor".
Crearemos un diccionario de longitud 9, y cada clave representará un bloque del tablero;
su valor correspondiente representará el movimiento realizado por un jugador.
Crearemos una función ``printBoard()`` que podremos utilizar cada vez que queramos imprimir el tablero actualizado en el juego::

 theBoard = {'7': ' ' , '8': ' ' , '9': ' ' ,
             '4': ' ' , '5': ' ' , '6': ' ' ,
             '1': ' ' , '2': ' ' , '3': ' ' }

 def printBoard(board):
   print(board['7'] + '|' + board['8'] + '|' + board['9'])
   print('-+-+-')
   print(board['4'] + '|' + board['5'] + '|' + board['6'])
   print('-+-+-')
   print(board['1'] + '|' + board['2'] + '|' + board['3'])

Ahora, en la función principal, primero tomaremos la entrada del jugador y comprobaremos si la entrada es un movimiento válido o no.
Si es así, rellenaremos ese bloque; si no, pediremos al usuario que elija otro bloque::

 def game():

   turn = 'X'
   count = 0

   for i in range(10):
     printBoard(theBoard)
     print("Es tu turno," + turn + ".¿A que lugar mover?")

     move = input()
     if theBoard[move] == ' ':
       theBoard[move] = turn
       count += 1
     else:
       print("Ese lugar ya está ocupado. ¿A qué lugar te mueves?")
       continue

Comprobamos ahora un total de 8 condiciones, buscando la ganadora.
El jugador que haya hecho la última jugada, lo declararemos ganador;
de lo contrario, si el tablero se llena y nadie gana, declararemos el resultado como un empate::

 # comprobar si el jugador X u O ha ganado, para cada movimiento después de 5 movimientos.
 if count >= 5:
   if theBoard['7'] == theBoard['8'] == theBoard['9'] != ' ': # en la parte superior
     printBoard(theBoard)
     print("\nFin del juego.\n")
     print(" **** " +turn + " ganado. ****")
     break
   elif theBoard['4'] == theBoard['5'] == theBoard['6'] != ' ': # a través del centro
     printBoard(theBoard)
     print("\nFin del juego.\n")
     print(" **** " +turn + " ganado. ****")
     break
   elif theBoard['1'] == theBoard['2'] == theBoard['3'] != ' ': # a través de la parte inferior
     printBoard(theBoard)
     print("\nFin del juego.\n")
     print(" **** " +turn + " ganado. ****")
     break
   elif theBoard['1'] == theBoard['4'] == theBoard['7'] != ' ': # por el lado izquierdo
     printBoard(theBoard)
     print("\nFin del juego.\n")
     print(" **** " +turn + " ganado. ****")
     break
   elif theBoard['2'] == theBoard['5'] == theBoard['8'] != ' ': # por el centro
     printBoard(theBoard)
     print("\nFin del juego.\n")
     print(" **** " +turn + " ganado. ****")
     break
   elif theBoard['3'] == theBoard['6'] == theBoard['9'] != ' ': # por el lado derecho
     printBoard(theBoard)
     print("\nFin del juego.\n")
     print(" **** " +turn + " ganado. ****")
     break
   elif theBoard['7'] == theBoard['5'] == theBoard['3'] != ' ': # diagonal
     printBoard(theBoard)
     print("\nFin del juego.\n")
     print(" **** " +turn + " ganado. ****")
     break
   elif theBoard['1'] == theBoard['5'] == theBoard['9'] != ' ': # diagonal
     printBoard(theBoard)
     print("\nFin del juego.\n")
     print(" **** " +turn + " ganado. ****")
     break

 # Si no ganan ni X ni O y el tablero está lleno, declara el resultado como un empate.
 if count == 9:
   print("\nFin del juego.\n")
   print("Es un empate!!")

 # cambiar el jugador después de cada movimiento.
   if turn =='X':
     turn = 'O'
   else:
       turn = 'X'


Por último, preguntamos a los usuarios si quieren reiniciar el juego::

 board_keys = []
 for key in theBoard:
   board_keys.append(key)

 restart = input("¿Quiere volver a jugar?(y/n)")
 if restart == "y" or restart == "Y":
   for key in board_keys:
     theBoard[key] = " "
   game()


Comprueba y completa el siguiente ActiveCode y ejecuta el juego Tic-Tac-Toe.

.. activecode:: tic-tac-toe
   :language: python
   :caption: Play the game!

   theBoard = {'7': ' ' , '8': ' ' , '9': ' ' ,
              '4': ' ' , '5': ' ' , '6': ' ' ,
              '1': ' ' , '2': ' ' , '3': ' ' }

   board_keys = []

   for key in theBoard:
     board_keys.append(key)

   def printBoard(board):
     print(board['7'] + '|' + board['8'] + '|' + board['9'])
     print('-+-+-')
     print(board['4'] + '|' + board['5'] + '|' + board['6'])
     print('-+-+-')
     print(board['1'] + '|' + board['2'] + '|' + board['3'])

   def game():
     turn = 'X'
     count = 0
     for i in range(10):
       printBoard(theBoard)
       print("Es tu turno," + turn + ".¿A qué lugar mover?")

       move = input()

       if theBoard[move] == ' ':
         theBoard[move] = turn
         count += 1
       else:
         print("Ese lugar ya está ocupado.\n ¿A qué lugar mover?")
         continue

       if count >= 5:
         if theBoard['7'] == theBoard['8'] == theBoard['9'] != ' ': # en la parte superior
           printBoard(theBoard)
           print("\nFin del juego.\n")
           print(" **** " +turn + " ganado. ****")
           break
       elif theBoard['4'] == theBoard['5'] == theBoard['6'] != ' ': # a través del centro
         printBoard(theBoard)
         print("\nFin del juego.\n")
         print(" **** " +turn + " ganado. ****")
         break
       elif theBoard['1'] == theBoard['2'] == theBoard['3'] != ' ': # a través de la parte inferior
         printBoard(theBoard)
         print("\nFin del juego.\n")
         print(" **** " +turn + " ganado. ****")
         break
       elif theBoard['1'] == theBoard['4'] == theBoard['7'] != ' ': # por el lado izquierdo
         printBoard(theBoard)
         print("\nFin del juego.\n")
         print(" **** " +turn + " ganado. ****")
         break
       elif theBoard['2'] == theBoard['5'] == theBoard['8'] != ' ': # por el centro
         printBoard(theBoard)
         print("\nFin del juego.\n")
         print(" **** " +turn + " ganado. ****")
         break
       elif theBoard['3'] == theBoard['6'] == theBoard['9'] != ' ': # por el lado derecho
         printBoard(theBoard)
         print("\nFin del juego.\n")
         print(" **** " +turn + " ganado. ****")
         break
       elif theBoard['7'] == theBoard['5'] == theBoard['3'] != ' ': # diagonal
         printBoard(theBoard)
         print("\nFin del juego.\n")
         print(" **** " +turn + " ganado. ****")
         break
       elif theBoard['1'] == theBoard['5'] == theBoard['9'] != ' ': # diagonal
         printBoard(theBoard)
         print("\nFin del juego.\n")
         print(" **** " +turn + " ganado. ****")
         break

       if count == 9:
         print("\nFin del juego.\n")
         print("Es un empate!!")

       if turn =='X':
         turn = 'O'
       else:
         turn = 'X'

     restart = input("¿Quiere volver a jugar?(y/n)")
     if restart == "y" or restart == "Y":
       for key in board_keys:
         theBoard[key] = " "

       game()

   if __name__ == "__main__":
     game()





Sección 3. Ejercicios y autoevaluación
----------------------------------------

**Ejercicio 3.1**

Sea \\(A\\) un array de tamaño \\(n \\geq 2\\) que contiene enteros de 1 a \\(n−1\\),
con exactamente uno repetido. Describa un algoritmo rápido para encontrar el número entero que se repite en \\(A\\).
(Pista: No ordenar A)

**Ejercicio 3.2**

Calcule la suma de todos los números de un conjunto de datos \\(n \\times n\\), representado como una lista de listas,
utilizando estructuras de control estándar.
(Sugerencia: Use bucles anidados)

**Ejercicio 3.3**

Describa cómo se puede combinar la función incorporada ``sum`` con la sintaxis de comprensión de Python para calcular la suma de todos los números de un conjunto de datos \\( n \\times n\\)
representado como una lista de listas.
(Pista: Construya una lista de subtotales, uno por cada lista anidada)

**Ejercicio 3.4**

El método ``shuffle`` del módulo ``random``, toma una lista de Python y la reordena
aleatoriamente, lo que significa que cada ordenación posible es igualmente probable.
Construye tu propia versión de dicha función, utilizando la función ``randrange(n)`` del módulo ``random``,
que devuelve un número aleatorio entre 0 y \\(n−1\\).

**Ejercicio 3.5**

Considera una implementación de un array dinámico, pero en lugar de copiar
los elementos en un array del doble de tamaño (es decir, de \\(N\\) a \\(2N\\) ) cuando
cuando se alcanza su capacidad, copiamos los elementos en un array con \\(N/4\\) celdas adicionales,
pasando de la capacidad \\(N\\) a la capacidad \\(N+N/4\\).
Demostrar que la realización de una secuencia de \\(n\\) las operaciones de adición todavía se ejecuta en \\(O(n)\\) el tiempo.

**Ejercicio 3.6**

Dado el siguiente fragmento de código para la clase ``DynamicArray``,
implementa y añade un método ``pop`` que elimine el último elemento del array,
y que reduzca la capacidad, \\(N\\) del array a la mitad cada vez que el número de elementos del
matriz sea inferior a \\(N/4\\).

.. activecode:: insert-for-dynamic-array-2
   :language: python
   :caption: Insertar un valor en la posición k de un array dinámico

   import ctypes					# proporciona matrices de bajo nivel

   class DynamicArray:
     def  __init__ (self):
       self._n = 0 				    # número de elementos
       self._capacity = 1 			# capacidad inicial
       self._A = self._make_array(self._capacity) 			# definir matriz de bajo nivel

     def __len__ (self):
       return self._n

     def __getitem__ (self, k):
       if not 0 <= k < self._n:
         raise IndexError('índice inválido')
       return self._A[k]

     def append(self, obj):
       if self._n == self._capacity:			# conjunto completo
         self._resize(2*self._capacity) 		# doble capacidad
       self._A[self._n] = obj
       self._n += 1

     def _resize(self, c):
       B = self._make_array(c) 			  # nuevo conjunto
       for k in range(self._n):
         B[k] = self._A[k]
       self._A = B 						  # A es la nueva matriz
       self._capacity = c

     def _make_array(self, c):
       return (c*ctypes.py_object)( )


**Ejercicio 3.7**

Demostrar que cuando se utiliza un array dinámico que crece y se encoge como en el
ejercicio anterior, la siguiente serie de operaciones de \\(2n\\) toma \\(O(n)\\) tiempo:
Operaciones de adición \\(n\\) en un array inicialmente vacío, seguidas de operaciones de extracción \\(n\\).

**Ejercicio 3.8**

La sintaxis de Python ``data.remove(value)`` elimina la primera aparición del elemento ``value`` de la lista ``data``.
Implementa la función ``removeall(data, value)``, que elimina todas las apariciones
de ``valor`` de la lista dada, de forma que el tiempo de ejecución en el peor de los casos de la función sea \\(O(n)\\)
en una lista con \\(n\\) elementos.

**Ejercicio 3.9**

Sea \\(B\\) un array de tamaño \\(n \\geq 6\\) que contiene números enteros de 1 a \\(n−5\\), con exactamente cinco repetidos.
Describa un algoritmo para encontrar los cinco enteros que se repiten en \\(B\\).

**Ejercicio 3.10**

Describa una forma de utilizar la recursividad para sumar todos los números de un conjunto de datos \\(n \\times n\\),
representado como una lista de listas.

**Ejercicio 3.11**

Escriba un programa en Python para una clase matricial que pueda sumar y multiplicar matrices bidimensionales
matrices bidimensionales de números, asumiendo que las dimensiones coinciden adecuadamente.
